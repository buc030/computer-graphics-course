
uniform sampler2D renderedTexture;

uniform int horizontalBlur;
uniform int verticalBlur;
uniform int reduceColors;
uniform int numOfColors = 6;

in vec2 renderedTexCoord;

layout(location = 0) out vec4 fColor;

void main() 
{
	float blurSize = 1.0/512;
	vec4 sum = vec4(0.0);
	if(horizontalBlur == 1)
	{
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x - 4.0*blurSize, renderedTexCoord.y)) * 0.05;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x - 3.0*blurSize, renderedTexCoord.y)) * 0.09;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x - 2.0*blurSize, renderedTexCoord.y)) * 0.12;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x - blurSize, renderedTexCoord.y)) * 0.15;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y)) * 0.16;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x + blurSize, renderedTexCoord.y)) * 0.15;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x + 2.0*blurSize, renderedTexCoord.y)) * 0.12;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x + 3.0*blurSize, renderedTexCoord.y)) * 0.09;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x + 4.0*blurSize, renderedTexCoord.y)) * 0.05;
	}
	else if(verticalBlur == 1)
	{
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y - 4.0*blurSize)) * 0.05;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y - 3.0*blurSize)) * 0.09;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y - 2.0*blurSize)) * 0.12;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y - blurSize)) * 0.15;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y)) * 0.16;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y + blurSize)) * 0.15;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y + 2.0*blurSize)) * 0.12;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y + 3.0*blurSize)) * 0.09;
		sum += texture2D(renderedTexture, vec2(renderedTexCoord.x, renderedTexCoord.y + 4.0*blurSize)) * 0.05;
	}
	else if(reduceColors == 1)
	{
		vec4 prevColor = texture2D(renderedTexture, renderedTexCoord);
		sum = ceil(prevColor*numOfColors)/numOfColors;
	}

	fColor = sum;
} 

