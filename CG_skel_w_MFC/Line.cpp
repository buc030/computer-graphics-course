#include "stdafx.h"
#include "Line.h"
#include "Polygon.h"
#define _ZETA 0.0001

CLine::CLine(const vec2& p1, const vec2& p2) : m_p1(p1), m_p2(p2)
{
}


CLine::~CLine()
{
}

bool CLine::intersect(const CLine& other, double& t, double& u) const
{
	vec2 r = m_p2 - m_p1;
	vec2 s = other.m_p2 - other.m_p1;

	double crossRs = crossD(r, s);
	//t is for this
	if (abs(crossRs) > _ZETA)
	{
		t = crossD((other.m_p1 - m_p1), s) / crossRs;
		u = crossD((other.m_p1 - m_p1), r) / crossRs;

		return t <= 1 && t >= 0 && u <= 1 && u >= 0;
	}

	return false;
}


bool CLine::intersect(const CLine& other, vec2& res) const
{
	double t;
	double u;
	if (intersect(other, t, u))
	{
		res = vec2(m_p2.x*t + m_p1.x*(1 - t), m_p2.y*t + m_p1.y*(1 - t));
		return true;
	}
	return false;
}


bool CLine::clip(int left, int right, int bottom, int top)
{
	float dx = m_p2.x - m_p1.x;
	float dy = m_p2.y - m_p1.y;
	float p[4] = { -dx, dx, -dy, dy };
	float q[4] = { m_p1.x - left, right - m_p1.x, m_p1.y - bottom, top - m_p1.y };

	float u1 = 0.0f;
	float u2 = 1.0f;
	for (int i = 0; i < 4; i++)
	{

		if (p[i] == 0)
		{
			if (q[i] < 0)
			{
				return false;
			}

		}
		if (p[i] < 0)
		{
			float r = q[i] / p[i];
			if (r > u2)
			{
				return false;
			}
			else if (r > u1)
			{
				u1 = r;
			}
		}
		else if (p[i] > 0)
		{
			float r = q[i] / p[i];
			if (r < u1)
			{
				return false;
			}
			else if (r < u2)
			{
				u2 = r;
			}
		}
	}

	vec2 temp = m_p1;
	m_p1.x = m_p1.x + u1*dx;
	m_p1.y = m_p1.y + u1*dy;

	m_p2.x = temp.x + u2*dx;
	m_p2.y = temp.y + u2*dy;

	return true;
}


