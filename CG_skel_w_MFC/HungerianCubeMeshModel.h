#pragma once
#include "MeshModel.h"
#include <array>

struct vec3Int
{
	int x, y, z;
	vec3Int(const vec3& other) : x(round(other.x)), y(round(other.y)), z(round(other.z)) {}
	operator vec3() const
	{
		return vec3(x, y, z);
	}

	bool operator<(const vec3Int& other) const
	{
		return x != other.x ? x < other.x : y != other.y ? y < other.y : z < other.z;
	}
};

struct AnimationInfo;

class HungerianCubeMeshModel : public MeshModel
{
public:
	enum StickerSide
	{
		LEFT_SIDE = 0,
		RIGHT_SIDE,
		UP_SIDE,
		DOWN_SIDE,
		FRONT_SIDE,
		BACK_SIDE
	};
	enum AtomicAction
	{
		LEFT_FACE_ROTATE = 0,
		RIGHT_FACE_ROTATE,
		UP_FACE_ROTATE,
		DOWN_FACE_ROTATE,
		FRONT_FACE_ROTATE,
		BACK_FACE_ROTATE,
		NUM_OF_ACTIONS
	};


	friend struct AnimationInfo;
private:

	struct Sticker
	{
		vec3 m_pos; //position index
		vec3 m_normal; //normal index
		vec3 m_up; //vector pointing upward from the normal
		StickerSide m_origSide;
	};

	struct EdgeCubie
	{
		EdgeCubie(int idx1, int idx2) { m_idxes[0] = idx1; m_idxes[1] = idx2; };
		int m_idxes[2];
	};
	struct CornerCubie
	{
		CornerCubie(int idx1, int idx2, int idx3) { m_idxes[0] = idx1; m_idxes[1] = idx2; m_idxes[2] = idx3; };
		int m_idxes[3];
	};
	
	struct Action
	{
		Action() {}
		Action(AtomicAction action) : m_atomicActions(1, action) {}
		Action(const vector<AtomicAction>& atomicActions) : m_atomicActions(atomicActions) { }
		vector<AtomicAction> m_atomicActions;
	};

	//state of the cube
	struct HungerianCubeData
	{
		std::vector<Sticker> m_Stickers;
		std::vector<EdgeCubie> m_EdgesIdxes;
		std::vector<CornerCubie> m_CornersIdexes;

		void rotateFace(AtomicAction AtomicAction, int degrees);
		void applyAction(Action action);
		void applyActionStep(Action action, int degrees);
		void rotateRightFace(float theta);
		void rotateLeftFace(float theta);
		void rotateUpFace(float theta);
		void rotateDownFace(float theta);
		void rotateFrontFace(float theta);
		void rotateBackFace(float theta);
		void roundAfterRotations();

		bool isInPlace(const vec3Int& normal, StickerSide origSide) const;
		bool isInPlace(const Sticker& Sticker) const;
		bool isInPlace(const EdgeCubie& Sticker) const;

		bool operator<(const HungerianCubeData& other) const;
	};



	vector<Action> m_allowedActions;
	HungerianCubeData m_cubeData;
	bool m_isAnimating;

	
	void insretSticker(vec3 pos, vec3 normal, vec3 up, StickerSide side);
	void initStickers();
	void initEdgesAndCornersIdexes();
	void initAllowedActions();

	vector<Action> getEdgeRoute(const EdgeCubie& edge) const;
	int getStickerDistance(const Sticker& Sticker) const;
	void pushIntoVector(const vec3& v, std::vector<GLfloat>& vertexes);

	int getNumOfPlacedStickers();
	int getScore() const;
	int countBadEdges() const;
	


public:
	
	bool operator==(const HungerianCubeMeshModel& other) const;

	HungerianCubeMeshModel();
	virtual ~HungerianCubeMeshModel();

	void rotateFaceAndAnimate(Scene* scene, AtomicAction AtomicAction, bool clockwise);
	void applyActionsAndAnimate(Scene* scene, const vector<Action>& actions);

	void solve(Scene* scene);

	void draw(Renderer* renderer);

	//return a map from poly index to neibour poly indexes 
	virtual void getOpenglData(
		std::vector<GLfloat>& vertexes,
		std::vector<GLfloat>& normals,
		std::vector<GLfloat>& neigbour1Normals,
		std::vector<GLfloat>& neigbour2Normals);
	
	virtual bool getOpenglNormalTextureData(
		std::vector<GLfloat>& UVs,
		std::vector<GLfloat>& tangents,
		std::vector<GLfloat>& bitangents);
	virtual bool getOpenglTextureData(std::vector<GLfloat>& UVs);

	virtual const vector<IPolygon>& getPolygons() const;
	virtual int getPolygonsCount() const;
	virtual void getMeterialIndexes(std::vector<GLuint>& meterialIdxes);

	void mixCube(int n);
};

