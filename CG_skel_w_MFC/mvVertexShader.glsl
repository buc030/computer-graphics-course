
uniform mat4 modelview; //ok
uniform mat4 normalsModelview; //ok

in  vec3 vPosition; //model coordinates
in vec3 normal; //model coordinates
out vec3 transformedNormal;

void main()
{
	transformedNormal = (normalsModelview*vec4(normal, 0)).xyz;
	gl_Position = modelview*vec4(vPosition, 1);
}

