#pragma once

#include "RGBColor.h"
#include "vec.h"
#include "Model.h"

class ILight : public Model
{
	static void initSphere();
public:
	static MeshModel* m_sphere;
	CRGBColor m_diffuse;
	CRGBColor m_specular;
	CRGBColor m_ambient;
	bool m_isParalel;
	ILight(vec4 position, CRGBColor diffuse, CRGBColor specular, CRGBColor ambient, bool isParalel = false);
	ILight();
	~ILight();

	virtual std::string print() const;
	void draw(Renderer* renderer);

	void setLocationInWorldCoordinates(vec4 position);

};

