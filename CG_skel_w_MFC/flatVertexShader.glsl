


uniform mat4 modelview; //ok
uniform mat4 normalsModelview; //ok
uniform mat4 projection; //ok
uniform int time;
uniform int vertexAnimation;

in  vec3 vPosition; //model coordinates
in vec3 normal; //model coordinates
flat out vec3 vertexColor;
out vec3 transformedNormal;


float rand1(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(12.9898,78.233, 309.2169))) * 43758.5453);
}
float rand2(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(5645.5647,124.8789, 2654.6945))) * 183.9832);
}
float rand3(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(477.89, 25.68 ,6876.854))) * 54235.6467);
}

void main()
{
	vec3 animatedPosition = vPosition;
	if(vertexAnimation == 1)
	{
		//animation step is between -1 and 1
		float animStep = sin(rand1(vPosition.xyz)*time/10.0)*rand1(vPosition.xyz);
		animatedPosition += animStep*vec3(0,0.1,0);
	}
	transformedNormal = (normalsModelview*vec4(normal, 0)).xyz;
	vec4 v = modelview*vec4(animatedPosition, 1);
	
	vertexColor = calculateVertexColor(v, transformedNormal);
	gl_Position = projection*v;
	
}
