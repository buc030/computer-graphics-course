
uniform vec3 linesColor = vec3(0,0,0);
layout(location = 0) out vec4 fColor;

void main() 
{ 
	fColor = vec4(linesColor,1);
} 

