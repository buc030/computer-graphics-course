// LightsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "LightsDialog.h"
#include "afxdialogex.h"
#include <sstream>

// CLightsDialog dialog

IMPLEMENT_DYNAMIC(CLightsDialog, CDialogEx)

CLightsDialog::CLightsDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLightsDialog::IDD, pParent)
{

}

CLightsDialog::~CLightsDialog()
{
}

void CLightsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIGHTS_LIST, m_listBox);
}


BEGIN_MESSAGE_MAP(CLightsDialog, CDialogEx)
	ON_BN_CLICKED(IDC_ADD_LIGHT_BUTTON, &CLightsDialog::OnBnClickedAddLightButton)
	ON_LBN_DBLCLK(IDC_LIGHTS_LIST, &CLightsDialog::OnLbnDblclkLightsList)
	ON_BN_CLICKED(IDC_REMOVE_LIGHT_BUTTON, &CLightsDialog::OnBnClickedRemoveLightButton)
	ON_BN_CLICKED(IDC_SET_ACTIVE_LIGHT_BUTTON, &CLightsDialog::OnBnClickedSetActiveLightButton)
END_MESSAGE_MAP()


BOOL CLightsDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetWindowText("Lights managment");
	reloadListBox();
	return TRUE;
}
// CLightsDialog message handlers


void CLightsDialog::OnBnClickedAddLightButton()
{
	if (m_lightPropertyDlg.DoModal() == IDOK)
	{
		m_scene->addLight(
			m_lightPropertyDlg.m_xyz, 
			m_lightPropertyDlg.m_diffuse.GetColor(), 
			m_lightPropertyDlg.m_specular.GetColor(),
			m_lightPropertyDlg.m_ambient.GetColor());
		reloadListBox();
	}
}

void CLightsDialog::reloadListBox()
{
	m_listBox.ResetContent();
	for (int i = 0; i < m_scene->m_lights.size(); i++)
	{
		std::stringstream ss;
		ss << "#" << i << ":    " << m_scene->m_lights[i]->print();
		m_listBox.AddString(ss.str().c_str());
	}
}

void CLightsDialog::OnLbnDblclkLightsList()
{
	// TODO: Add your control notification handler code here
	int selected = m_listBox.GetCurSel();
	if (selected != -1)
	{
		m_lightPropertyDlg.m_xyz = m_scene->m_lights[selected]->getLocationInWorldCoordinates();
		m_lightPropertyDlg.m_ambient.SetColor(m_scene->m_lights[selected]->m_ambient);
		m_lightPropertyDlg.m_diffuse.SetColor(m_scene->m_lights[selected]->m_diffuse);
		m_lightPropertyDlg.m_specular.SetColor(m_scene->m_lights[selected]->m_specular);
		
		m_lightPropertyDlg.m_isParallel = m_scene->m_lights[selected]->m_isParalel;
		if (m_lightPropertyDlg.DoModal() == IDOK)
		{
			
			m_scene->m_lights[selected]->m_ambient = m_lightPropertyDlg.m_ambient.GetColor();
			m_scene->m_lights[selected]->m_diffuse = m_lightPropertyDlg.m_diffuse.GetColor();
			m_scene->m_lights[selected]->m_specular = m_lightPropertyDlg.m_specular.GetColor();
			m_scene->m_lights[selected]->m_isParalel = m_lightPropertyDlg.m_isParallel;
			m_scene->m_lights[selected]->setLocationInWorldCoordinates(m_lightPropertyDlg.m_xyz);

			reloadListBox();
		}
	}
}


void CLightsDialog::OnBnClickedRemoveLightButton()
{
	int selected = m_listBox.GetCurSel();
	if (selected != -1)
	{
		if (selected == m_scene->m_activeLight)
		{
			AfxMessageBox("Can't remove active light!", MB_OK | MB_ICONSTOP); 
			return;
		}
		if (selected < m_scene->m_activeLight)
		{
			m_scene->m_activeLight--;
		}
		m_scene->m_lights.erase(m_scene->m_lights.begin() + selected);
		reloadListBox();
	}
}


void CLightsDialog::OnBnClickedSetActiveLightButton()
{
	int selected = m_listBox.GetCurSel();
	if (selected != -1)
	{
		m_scene->m_activeLight = selected;
	}
	
}
