#define MAX_LIGHTS 8

uniform int numLights; //ok
uniform int modifiedPhong;
uniform struct Light 
{
   vec4 position; //camera coordinates
   vec3 ambient;
   vec3 diffuse; 
   vec3 specular; 
} lights[MAX_LIGHTS]; //ok

#define MAX_METERIALS 8
uniform int numMeterials; //ok
uniform struct Meterial {
   vec3 ambient;
   vec3 diffuse; 
   vec3 specular; 
   vec3 emissive;
   float shinines;
} allMeterials[MAX_METERIALS]; 


//n, v are in camera coordinates
vec3 applyLight(int lightIndex, Meterial meterial, vec3 NN, vec3 NL, vec3 NE) 
{
	vec3 ambient = meterial.ambient*lights[lightIndex].ambient;

	vec3 diffuse = vec3(0);
	vec3 specular = vec3(0);
	float d = dot(NN, NL);
	if (d > 0)
	{
		diffuse = d*(lights[lightIndex].diffuse*meterial.diffuse);
	}

	//modifiedPhong
	//vec3 H = normalize(NL + NE);
	//float s = dot(H, NN);
	vec3 R = reflect(-NL, NN); 
	float s = dot(R, NE);
	if (s > 0)
	{
		specular = meterial.specular*lights[lightIndex].specular*pow(s, meterial.shinines);
	}

	return ambient + diffuse + specular;
}

//n, v are in camera coordinates
vec3 calculateVertexColor(vec4 v, vec3 n)
{
	
	vec3 c = vec3(0);
	vec3 N = normalize(n);
	vec3 E = -normalize(v.xyz);
	for(int i = 0; i < MAX_LIGHTS && i < numLights; i++)
	{
		vec3 L = normalize(lights[i].position.xyz - v.xyz);
		if(lights[i].position.w == 0)
		{
			L = normalize(lights[i].position.xyz);
		}
		c += applyLight(i, allMeterials[gl_VertexID%(min(MAX_METERIALS, numMeterials))], N, L, E);
	}

	c += allMeterials[gl_VertexID%(min(MAX_METERIALS, numMeterials))].emissive;
	c.x = clamp(c.x, 0, 1);
	c.y = clamp(c.y, 0, 1);
	c.z = clamp(c.z, 0, 1);
	return c;
}


//n, v are in camera coordinates
vec3 calculateColor(Meterial meterial, vec3 N, vec3 _L[MAX_LIGHTS], vec3 E)
{

	vec3 NN = normalize(N);
	vec3 NE = normalize(E);
	vec3 c = vec3(0);
	for(int i = 0; i < MAX_LIGHTS && i < numLights; i++)
	{
		vec3 NL = normalize(_L[i]);
		c += applyLight(i, meterial, NN, NL, NE);
	}

	c += meterial.emissive;
	c.x = clamp(c.x, 0, 1);
	c.y = clamp(c.y, 0, 1);
	c.z = clamp(c.z, 0, 1);
	return c;

}



