
uniform mat4 projection; //ok
uniform mat4 modelview; //ok
uniform mat4 normalsModelview;
uniform mat4 cameraMatrix;

out vec3 reflected;
out vec2 renderedTexCoord;
in vec3 vPosition; //model coordinates
in vec3 normal;

void main()
{
	vec4 v = modelview*vec4(vPosition, 1);
	gl_Position = projection*v;
	vec4 ndc = gl_Position/gl_Position.w;
	renderedTexCoord = (ndc.xy + vec2(1,1))/2.0;

	reflected = reflect(normalize(v.xyz), (normalsModelview*vec4(normal, 0)).xyz);
	reflected = ((inverse(transpose(inverse(cameraMatrix)))*vec4(reflected,0))).xyz;
}





