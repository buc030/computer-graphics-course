layout(triangles) in;

layout(line_strip, max_vertices=6) out;

uniform float normal_length;
uniform mat4 projection;

in vec4 transformedNormal[];


out vec3 vertexColor;
out vec2 renderedTexCoord;


void main()
{
  int i;
  for(i=0; i<gl_in.length(); i++)
  {
    vec3 P = gl_in[i].gl_Position.xyz;
    vec3 N = transformedNormal[i].xyz;
    
    gl_Position = projection * vec4(P, 1.0);
    vertexColor = vec3(0,1,0);
	renderedTexCoord = (gl_Position.xy + vec2(1,1))/2.0;
    EmitVertex();
    
    gl_Position = projection * vec4(P + N * normal_length, 1.0);
    vertexColor = vec3(0,1,0);
	renderedTexCoord = (gl_Position.xy + vec2(1,1))/2.0;
    EmitVertex();
    
    EndPrimitive();
  }
}