#pragma once

#include "Transformation.h"
// CTransformationDialog dialog

class CTransformationDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CTransformationDialog)

public:
	CTransformationDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTransformationDialog();

// Dialog Data
	enum { IDD = IDD_TRANSFORM_DIALOG };

	Transformation::SelectedFrame m_selectedFrame;
	Transformation::SelectedTransformation m_selectedTransformation;
	float m_stepSize;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedTranslateRadio();
	afx_msg void OnBnClickedScaleRadio();
	afx_msg void OnBnClickedRotateRadio();
	afx_msg void OnBnClickedModelFrameRadio();
	afx_msg void OnBnClickedWorldFrameRadio();
	afx_msg void OnBnClickedXRadio();
	afx_msg void OnBnClickedYRadio();
	afx_msg void OnBnClickedZRadio();
};
