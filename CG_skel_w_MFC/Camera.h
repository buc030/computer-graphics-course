#pragma once

#include "Model.h"

typedef struct
{
	float m_fovy, m_aspect, m_zNear, m_zFar;
} PerspectiveParameters;

typedef struct
{
	float m_top, m_bottom, m_left, m_right, m_zNear, m_zFar;
} OrthogonalParameters;

class Camera : public Model
{
	static void initCoordinatesSystemMeshModel();
public:
	static MeshModel* m_coordinatesSystem;
	
	Camera(Renderer* renderer);
	bool m_isPerspective;
	PerspectiveParameters m_perParams;
	OrthogonalParameters m_orthParams;
	//float m_left, m_right, m_bottom, m_top;

	//camera to world
	//mat4 cTransformInverse;

	//world to camera
	//mat4 cTransform;

	mat4 getCameraTransform() const;
	mat4 getCameraTransformInverse() const;
	mat4 projection;

	void UpdateScreenWidthAndHeight(int w, int h);
	void virtual draw(Renderer* renderer);

	virtual void scale(GLfloat x, GLfloat y, GLfloat z, bool aroundWorld = false);
	virtual void resetTransformations();
	void zoomIn();
	void zoomOut();

	virtual string print() const;

	void LookAt(const vec4& eye, const vec4& at, const vec4& up);
	void LookAtModel(Model* model);
	void Ortho();
	mat4 Perspective();
	void Frustum(const float left, const float right,
		const float bottom, const float top,
		const float zNear, const float zFar);


};