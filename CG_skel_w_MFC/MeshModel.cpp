#include "StdAfx.h"
#include "MeshModel.h"
#include "vec.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>  
#include "Texture.h"
#include "Polygon.h"
#include "PrimMeshModel.h"
using namespace std;



vec3 vec3fFromStream(std::istream & aStream)
{
	float x, y, z;
	aStream >> x >> std::ws >> y >> std::ws >> z;
	return vec3(x, y, z);
}

vec2 vec2fFromStream(std::istream & aStream)
{
	float x, y;
	aStream >> x >> std::ws >> y;
	return vec2(x, y);
}



MeshModel::MeshModel(string fileName) : m_boundingBox(NULL), m_drawBoundingBox(false), m_enviormentMapping(false)
, m_silhoutteRendering(false), m_toonShading(false), m_colorAnimationHueChange(false), m_colorAnimationSaturationChange(false), m_vertexAnimation(false),
m_Meterials(1)
{
	loadFile(fileName);
	computeBoundingBox();
	generatePlanerTextureCoordinates();
	generateSphereTextureCoordinates();

	
	for(int i = 0; i < m_vertices.size(); i++)
	{
		m_meterialIndexes.push_back(i%m_Meterials.size());
	}
	
}

MeshModel::MeshModel() : m_boundingBox(NULL), m_drawBoundingBox(false),  m_enviormentMapping(false)
, m_silhoutteRendering(false), m_toonShading(false), m_colorAnimationHueChange(false), m_colorAnimationSaturationChange(false), m_vertexAnimation(false),
m_Meterials(1)
{
	generatePlanerTextureCoordinates();
	generateSphereTextureCoordinates();
}

MeshModel::~MeshModel(void)
{
	delete m_boundingBox;
}

void MeshModel::setMeterials(const vector<CMetirial>& meterials)
{
	m_Meterials = meterials;
}


vector<CMetirial>& MeshModel::getMeterials()
{
	return m_Meterials;
}

void MeshModel::loadFile(istream& ifile)
{
	
	if (!ifile.good())
	{
		return;
	}
	vector<FaceIdcs> faces;

	faces.reserve(1000000);
	// while not end of file
	while (!ifile.eof())
	{
		// get line
		string curLine;
		getline(ifile, curLine);

		// read type of the line
		istringstream issLine(curLine);
		string lineType;

		issLine >> std::ws >> lineType;
		if (lineType.empty() || lineType[0] == '#' || lineType == "g" || lineType == "s" || lineType == "o" || lineType == "mtllib" || lineType == "usemtl")
		{
			continue;
		}
		
		// based on the type parse data
		if (lineType == "v") 
			m_vertices.push_back(vec3fFromStream(issLine));
		else if (lineType == "vn")
			m_normals.push_back(vec3fFromStream(issLine));
		else if (lineType == "f")
			faces.push_back(issLine);
		else if (lineType == "vt")
			m_textureCoords["file"].push_back(vec2fFromStream(issLine));
		else
		{
			cout<< "Found unknown line Type \"" << lineType << "\"";
		}
	}
	//Vertex_positions is an array of vec3. Every three elements define a triangle in 3D.
	//If the face part of the obj is
	//f 1 2 3
	//f 1 3 4
	//Then vertex_positions should contain:
	//vertex_positions={v1,v2,v3,v1,v3,v4}
 
	// iterate through all stored faces and create triangles
	m_polygons.reserve(faces.size());
	for (int i = 0; i < faces.size();++i)
	{
		m_polygons.push_back(IPolygon(m_vertices, m_normals, m_textureCoords));
		FaceIdcs& currFace = faces[i];
		m_polygons[i].setVertexes(currFace.v);
		m_polygons[i].setVertexNormals(currFace.vn);
		m_polygons[i].setTextureCoordinates("file", currFace.vt);
	}
}

void MeshModel::loadFile(string fileName)
{
	try
	{
		m_name = fileName.substr(fileName.find_last_of('\\') + 1);
	}
	catch(...)
	{
		m_name = fileName;
	}
	ifstream ifile(fileName.c_str());
	loadFile(ifile);
}

void MeshModel::generatePlanerTextureCoordinates()
{
	for(int i = 0; i < m_vertices.size(); i++)
	{
		m_textureCoords["planner"].push_back(vec2(m_vertices[i].x, m_vertices[i].y));
	}

	for(int i = 0; i < m_polygons.size(); i++)
	{
		m_polygons[i].setTextureCoordinates("planner", m_polygons[i].getVertexesIndexes());
	}
}

void MeshModel::generateSphereTextureCoordinates()
{
	for (int i = 0; i < m_vertices.size(); i++)
	{
		vec3 v = normalize(m_vertices[i]);
		vec2 vt = vec2(
				0.5 + atan2(v.z, v.x) / 2 * M_PI, 
				0.5 - asin(v.y)
				);
		vt = vt*length(m_vertices[i]);
		m_textureCoords["sphere"].push_back(vt);
	}

	for (int i = 0; i < m_polygons.size(); i++)
	{
		m_polygons[i].setTextureCoordinates("sphere", m_polygons[i].getVertexesIndexes());
	}
}
bool MeshModel::useNormalTexture(const string& textname, const string& textCoordName)
{
	if(m_textures.find(textname) == m_textures.end() || m_textureCoords.find(textCoordName) == m_textureCoords.end())
	{
		return false;
	}

	m_activeTextureNormalName = textname;
	m_activeTextureNormalCoordName = textCoordName;
	return true;
}

bool MeshModel::useTexture(const string& textname, const string& textCoordName)
{

	if(m_textures.find(textname) == m_textures.end() || m_textureCoords.find(textCoordName) == m_textureCoords.end())
	{
		return false;
	}

	m_activeTextureName = textname;
	m_activeTextureCoordName = textCoordName;
	return true;
}


bool MeshModel::loadTexture(const string& filename)
{
	string textName = filename.substr(filename.find_last_of("/\\") + 1, std::string::npos);
	if(m_textures.find(textName) != m_textures.end())
	{
		return false;
	}

	m_textures[textName] = new Texture(filename);
	return true;
}


Texture* MeshModel::getActiveNormalTexture()
{
	if(m_activeTextureNormalName == "")
	{
		return NULL;
	}
	assert(m_textures.find(m_activeTextureNormalName) != m_textures.end());
	return m_textures[m_activeTextureNormalName];
}


Texture* MeshModel::getActiveTexture()
{
	if(m_activeTextureName == "")
	{
		return NULL;
	}
	assert(m_textures.find(m_activeTextureName) != m_textures.end());
	return m_textures[m_activeTextureName];
}

string MeshModel::printCreationCode() const
{
	stringstream ss;
	for (int i = 0; i < m_polygons.size(); i++)
	{
		ss << m_polygons[i].print();
	}
	return ss.str();
}
string MeshModel::print() const
{
	stringstream ss;
	ss << m_name << ",   location = " << vec3FromVec4(m_world_transform*m_model_transform*vec4(0, 0, 0, 1));
	ss << ", Env mapping is " << (m_enviormentMapping ? "On" : "Off");
	ss << ", Texture: " << m_activeTextureName;
	return ss.str();
}


void MeshModel::draw(Renderer* renderer)
{
	renderer->DrawMeshModel(this);
}


const vector<IPolygon>& MeshModel::getPolygons() const
{
	return m_polygons;
}

void updateIfNeeded(float& target_high, float& target_low, float other)
{
	if (other > target_high)
	{
		target_high = other;
	}
	if (other < target_low)
	{
		target_low = other;
	}
}

void MeshModel::computeBoundingBox(float& x_low, float& x_high, float& y_low, float& y_high, float& z_low, float& z_high)
{
#undef max
	x_low = y_low = z_low = std::numeric_limits<float>::max();
	x_high = y_high = z_high = std::numeric_limits<float>::lowest();
	for (int i = 1; i < m_polygons.size(); i++)
	{
		IPolygon& poly = m_polygons[i];
		for(int j = 0; j < 3; j++)
		{
			vec4 v = poly.getVertex(j);
			updateIfNeeded(x_high, x_low, v.x);
			updateIfNeeded(y_high, y_low, v.y);
			updateIfNeeded(z_high, z_low, v.z);
		}
	}
}

vec3 MeshModel::getCenterOfMassInWorldCoordinates() const
{
	const MeshModel* m = m_boundingBox ? m_boundingBox : this;
	mat4 trnasform = m_boundingBox ? m_world_transform*m_model_transform*m_boundingBox->getModelMatrix() : m_world_transform*m_model_transform;

	vec4 res(0, 0, 0, 0);
	for (int i = 0; i < m->m_vertices.size(); i++)
	{
		res += trnasform*vec4(m->m_vertices[i], 1);
	}
	//m_world_transform*m_model_transform
	res /= res.w;
	return vec3FromVec4(res);
}

void MeshModel::computeBoundingBox()
{
	if (m_boundingBox)
	{
		delete m_boundingBox;
		m_boundingBox = NULL;
	}
	float x_low, x_high, y_low, y_high, z_low, z_high;
	computeBoundingBox(x_low, x_high, y_low, y_high, z_low, z_high);

	m_boundingBox = new CubePrimMeshModel();

	m_boundingBox->m_world_transform = Translate((x_high + x_low) / 2, (y_high + y_low) / 2, (z_high + z_low) / 2)* 
		Scale((x_high - x_low) / 2, (y_high - y_low) / 2, (z_high - z_low) / 2);
	
	m_boundingBox->m_model_transform = mat4();
}

#define EPS_ FLT_EPSILON
struct EdgeComp
{
	bool operator()(vec3 v1, vec3 v2) const
	{
		if (abs(length(v1 - v2)) < EPS_)
		{
			return false;
		}

		if (v1.x < v2.x)
		{
			return false;
		}
		else if (v1.x > v2.x)
		{
			return true;
		}

		if (v1.y < v2.y)
		{
			return false;
		}
		else if (v1.y > v2.y)
		{
			return true;
		}

		if (v1.z < v2.z)
		{
			return false;
		}
		else if (v1.z > v2.z)
		{
			return true;
		}

		return false;
	}
};

bool MeshModel::getOpenglNormalTextureData(
	std::vector<GLfloat>& UVs,
	std::vector<GLfloat>& tangents,
	std::vector<GLfloat>& bitangents)
{
	if(!m_polygons[0].hasTextureCoordinates(m_activeTextureNormalCoordName))
	{
		return false;
	}
	std::vector<vec3> v3tangents;
	std::vector<vec3> v3bitangents;
	computeTangentBasis(v3tangents, v3bitangents);
	for (int i = 0; i < m_polygons.size(); i++)
	{
		IPolygon& currTri = m_polygons[i];
		for (int j = 0; j < 3; j++)
		{
			assert (currTri.hasTextureCoordinates(m_activeTextureNormalCoordName));
			
			UVs.push_back(currTri.getTextureCoord(m_activeTextureNormalCoordName, j).x);
			UVs.push_back(currTri.getTextureCoord(m_activeTextureNormalCoordName, j).y);

			assert(3 * i + j < v3bitangents.size() && 3 * i + j < v3tangents.size());
			tangents.push_back(v3tangents[3 * i + j].x);
			tangents.push_back(v3tangents[3 * i + j].y);
			tangents.push_back(v3tangents[3 * i + j].z);

			bitangents.push_back(v3bitangents[3 * i + j].x);
			bitangents.push_back(v3bitangents[3 * i + j].y);
			bitangents.push_back(v3bitangents[3 * i + j].z);

		}
	}
}

bool MeshModel::getOpenglTextureData(std::vector<GLfloat>& UVs)
{
	if(!m_polygons[0].hasTextureCoordinates(m_activeTextureCoordName))
	{
		return false;
	}
	std::vector<vec3> v3tangents;
	std::vector<vec3> v3bitangents;
	computeTangentBasis(v3tangents, v3bitangents);
	for (int i = 0; i < m_polygons.size(); i++)
	{
		IPolygon& currTri = m_polygons[i];
		for (int j = 0; j < 3; j++)
		{
			assert (currTri.hasTextureCoordinates(m_activeTextureCoordName));
			UVs.push_back(currTri.getTextureCoord(m_activeTextureCoordName, j).x);
			UVs.push_back(currTri.getTextureCoord(m_activeTextureCoordName, j).y);
		}
	}
}

void MeshModel::getMeterialIndexes(std::vector<GLuint>& meterialIdxes)
{
	return;
}

//return a map from poly index to neibour poly indexes 
void MeshModel::getOpenglData(
	std::vector<GLfloat>& vertexes, 
	std::vector<GLfloat>& normals,
	std::vector<GLfloat>& neigbour1Normals,
	std::vector<GLfloat>& neigbour2Normals)
{
	EdgeComp comp;
	map<std::pair<int, int>, vector<int>> m_edgeToPolygons;
	map<vec3, int, EdgeComp> vertexToFirstOcc;
	for (int i = 0; i < m_polygons.size(); i++)
	{
		IPolygon& currTri = m_polygons[i];

		for (int j = 0; j < 3; j++) 
		{
			if (vertexToFirstOcc.find(vec3FromVec4(currTri.getVertex(j))) == vertexToFirstOcc.end())
			{
				vertexToFirstOcc[vec3FromVec4(currTri.getVertex(j))] = i * 3 + j;
			}
			int first = vertexToFirstOcc[vec3FromVec4(currTri.getVertex(j))];

			if (vertexToFirstOcc.find(vec3FromVec4(currTri.getVertex((j + 1)%3))) == vertexToFirstOcc.end())
			{
				vertexToFirstOcc[vec3FromVec4(currTri.getVertex((j + 1)%3))] = i * 3 + (j + 1) % 3;
			}
			assert(vertexToFirstOcc.find(vec3FromVec4(currTri.getVertex((j + 1) % 3))) != vertexToFirstOcc.end());
			int second = vertexToFirstOcc[vec3FromVec4(currTri.getVertex((j + 1)%3))];

			std::pair<int, int> edge = std::make_pair(first, second);
			if (edge.first > edge.second)
			{
				edge = std::make_pair(edge.second, edge.first);
			}

			m_edgeToPolygons[edge].push_back(i);
			assert(m_edgeToPolygons.find(edge) != m_edgeToPolygons.end());
		}
	}

	std::vector<vec3> v3tangents;
	std::vector<vec3> v3bitangents;
	computeTangentBasis(v3tangents, v3bitangents);

	for (int i = 0; i < m_polygons.size(); i++)
	{
		IPolygon& currTri = m_polygons[i];
		for (int j = 0; j < 3; j++)
		{		
			vertexes.push_back(currTri.getVertex(j).x);
			vertexes.push_back(currTri.getVertex(j).y);
			vertexes.push_back(currTri.getVertex(j).z);

			if (currTri.getNormal(j) == vec3(0, 0, 0))
			{
				normals.push_back(currTri.getNormal().x);
				normals.push_back(currTri.getNormal().y);
				normals.push_back(currTri.getNormal().z);
			}
			else
			{
				normals.push_back(currTri.getNormal(j).x);
				normals.push_back(currTri.getNormal(j).y);
				normals.push_back(currTri.getNormal(j).z);
			}


			neigbour1Normals.push_back(currTri.getNormal().x);
			neigbour1Normals.push_back(currTri.getNormal().y);
			neigbour1Normals.push_back(currTri.getNormal().z);


			int first = vertexToFirstOcc[vec3FromVec4(currTri.getVertex(j))];
			int second = vertexToFirstOcc[vec3FromVec4(currTri.getVertex((j + 1)%3))];
			std::pair<int, int> edge = std::make_pair(first, second);
			if (edge.first > edge.second)
			{
				edge = std::make_pair(edge.second, edge.first);
			}

			assert(m_edgeToPolygons.find(edge) != m_edgeToPolygons.end());
			vector<int>& polys = m_edgeToPolygons[edge];

			if (polys.size() == 1)
			{
				neigbour2Normals.push_back(-currTri.getNormal().x);
				neigbour2Normals.push_back(-currTri.getNormal().y);
				neigbour2Normals.push_back(-currTri.getNormal().z);
				continue;
			}

			assert(polys.size() == 2);
			assert((polys[0] == i || polys[1] == i) && (polys[0] != polys[1]));
			if (polys[0] == i)
			{
				IPolygon& otherTri = m_polygons[polys[1]];
				neigbour2Normals.push_back(otherTri.getNormal().x);
				neigbour2Normals.push_back(otherTri.getNormal().y);
				neigbour2Normals.push_back(otherTri.getNormal().z);
			}
			else
			{
				IPolygon& otherTri = m_polygons[polys[0]];
				neigbour2Normals.push_back(otherTri.getNormal().x);
				neigbour2Normals.push_back(otherTri.getNormal().y);
				neigbour2Normals.push_back(otherTri.getNormal().z);
			}
		}
	}
}


void MeshModel::computeTangentBasis(std::vector<vec3> & tangents, std::vector<vec3> & bitangents)
{
	if (!m_polygons[0].hasTextureCoordinates(m_activeTextureNormalCoordName))
	{
		return;
	}
	
	for (int i = 0; i < m_polygons.size(); i++)
	{
		//m_polygons[i].getVertex(0)
		vec2 dUV1 = m_polygons[i].getTextureCoord(m_activeTextureNormalCoordName,1) - m_polygons[i].getTextureCoord(m_activeTextureNormalCoordName,0);
		vec2 dUV2 = m_polygons[i].getTextureCoord(m_activeTextureNormalCoordName,2) - m_polygons[i].getTextureCoord(m_activeTextureNormalCoordName,0);

		vec3 d1 = vec3FromVec4(m_polygons[i].getVertex(1) - m_polygons[i].getVertex(0));
		vec3 d2 = vec3FromVec4(m_polygons[i].getVertex(2) - m_polygons[i].getVertex(0));
		float r = 1.0f / (dUV1.x*dUV2.y - dUV1.y*dUV2.x);
		vec3 tangent = (d1*dUV2.y - d2*dUV1.y)*r;
		vec3 bitangent = (d2*dUV1.x - d1*dUV2.x)*r;

		tangents.push_back(tangent);
		tangents.push_back(tangent);
		tangents.push_back(tangent);

		bitangents.push_back(bitangent);
		bitangents.push_back(bitangent);
		bitangents.push_back(bitangent);
	}

	for (int i = 0; i < m_polygons.size(); i++)
	{
		for(int j = 0; j < 3; j++)
		{
			const vec3& normal = m_polygons[i].getNormal(j);
			vec3& tangent = tangents[3*i + j] ;
			vec3& bitangent = bitangents[3*i + j];
			
			tangent = normalize(tangent -normal*dot(normal, tangent));
			bitangent = cross(normal, tangent);
		}
	}
}