#pragma once

#include <vector>
#include "Scene.h"
#include "PerspectiveParamsDialog.h"
#include "OrthogonalParametersDialog.h"

// CCameraDialog dialog
class CCameraDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CCameraDialog)

public:
	CCameraDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCameraDialog();
	virtual BOOL OnInitDialog();

	void setCameras(std::vector<Camera*> cameras);
	void setActiveCamera(int id);
	int m_activeCamera;
	std::vector<Camera*> m_cameras;
	CPerspectiveParamsDialog m_perspectiveParamsDialog;
	COrthogonalParametersDialog m_orthoParamsDialog;
// Dialog Data
	enum { IDD = IDD_CAMERA_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_listBox; 
	afx_msg void OnBnClickedSetActiveCameraButton();
	afx_msg void OnBnClickedChangeParameters();
	afx_msg void OnBnClickedPerspectiveRadio();
	afx_msg void OnBnClickedOtrhoRadio();
};
