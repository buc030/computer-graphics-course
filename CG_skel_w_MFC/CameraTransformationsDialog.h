#pragma once

#include "Transformation.h"
// CameraTransformationsDialog dialog

class CameraTransformationsDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CameraTransformationsDialog)

public:
	CameraTransformationsDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CameraTransformationsDialog();

// Dialog Data
	enum { IDD = IDD_CAMERA_TRANSFORM_DIALOG };

	float m_stepSize;
	Transformation::SelectedFrame m_selectedFrame;
	Transformation::SelectedTransformation m_selectedTransformation;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCameraTranslateRadio();
	afx_msg void OnBnClickedCameraRotateRadio();
	afx_msg void OnBnClickedCameraViewFrameRadio();
	afx_msg void OnBnClickedCameraWorldFrameRadio();
};
