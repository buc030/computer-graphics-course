#include "stdafx.h"
#include "PrimMeshModel.h"
#include <sstream>

PrimMeshModel::PrimMeshModel() 
{
}


PrimMeshModel::~PrimMeshModel()
{
}


CubePrimMeshModel::~CubePrimMeshModel()
{

}

CubePrimMeshModel::CubePrimMeshModel()
{
	string objFile = 
		"# cube.obj\n"
		"#\n"
 
		"o cube\n"
		"mtllib cube.mtl\n"
 
		"v -1.0 -1.0 1.0\n"
		"v 1.0 -1.0 1.0\n"
		"v -1.0 1.0 1.0\n"
		"v 1.0 1.0 1.0\n"
		"v -1.0 1.0 -1.0\n"
		"v 1.0 1.0 -1.0\n"
		"v -1.0 -1.0 -1.0\n"
		"v 1.0 -1.0 -1.0\n"
 
		"vt 0.000000 0.000000\n"
		"vt 1.000000 0.000000\n"
		"vt 0.000000 1.000000\n"
		"vt 1.000000 1.000000\n"
 
		"vn 0.000000 0.000000 1.000000\n"
		"vn 0.000000 1.000000 0.000000\n"
		"vn 0.000000 0.000000 -1.000000\n"
		"vn 0.000000 -1.000000 0.000000\n"
		"vn 1.000000 0.000000 0.000000\n"
		"vn -1.000000 0.000000 0.000000\n"
 
		"g cube\n"
		"usemtl cube\n"
		"s 1\n"
		"f 1/1/1 2/2/1 3/3/1\n"
		"f 3/3/1 2/2/1 4/4/1\n"
		"s 2\n"
		"f 3/1/2 4/2/2 5/3/2\n"
		"f 5/3/2 4/2/2 6/4/2\n"
		"s 3\n"
		"f 5/4/3 6/3/3 7/2/3\n"
		"f 7/2/3 6/3/3 8/1/3\n"
		"s 4\n"
		"f 7/1/4 8/2/4 1/3/4\n"
		"f 1/3/4 8/2/4 2/4/4\n"
		"s 5\n"
		"f 2/1/5 8/2/5 4/3/5\n"
		"f 4/3/5 8/2/5 6/4/5\n"
		"s 6\n"
		"f 7/1/6 1/2/6 5/3/6\n"
		"f 5/3/6 1/2/6 3/4/6\n";

	std::istringstream ss(objFile);
	loadFile(ss);
	m_name = "Cube Primitive";

	generatePlanerTextureCoordinates();
	generateSphereTextureCoordinates();

	for(int i = 0; i < m_vertices.size(); i++)
	{
		m_meterialIndexes.push_back(i%m_Meterials.size());
	}

}
