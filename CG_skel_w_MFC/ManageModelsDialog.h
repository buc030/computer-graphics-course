#pragma once

#include "Scene.h"
#include "afxwin.h"
// CManageModelsDialog dialog

class CManageModelsDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CManageModelsDialog)
	void recalculateList();
public:
	CManageModelsDialog(CWnd* pParent = NULL);   // standard constructor

	virtual BOOL OnInitDialog();
	virtual ~CManageModelsDialog();

// Dialog Data
	enum { IDD = IDD_MANAGE_MODEL_DIALOG };

	Scene* m_scene;
protected:
	CListBox m_listBox; 
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedSetActiveModelButton();
	afx_msg void OnBnClickedSetMeterialButton();
	afx_msg void OnLbnDblclkLoadedModelsList();
	afx_msg void OnBnClickedSetNonUniformMeterialButton();
	afx_msg void OnBnClickedUnLoadModelButton();

	//int m_isEnvMapping;
	//int m_isSilhouette;
	CButton m_envMappingCheckbox;
	CButton m_silhouetteCheckbox;
	afx_msg void OnBnClickedEnvMappingCheck();
	afx_msg void OnBnClickedSilhouetteCheck();
	afx_msg void OnLbnSelchangeLoadedModelsList();
	CButton m_toonCheckbox;
	CButton m_colorAnimationHueChangeCheckbox;
	CButton m_vertexAnimationCheckbox;
	afx_msg void OnBnClickedToonShadingCheck();
	afx_msg void OnBnClickedColorAnimationHueChangeCheck();
	afx_msg void OnBnClickedVertexAnimationCheck();
	CButton m_animationChangeSaturationCheckbox;
	afx_msg void OnBnClickedModelTextureButton();
	afx_msg void OnBnClickedModelTexturesButton();
	afx_msg void OnBnClickedColorAnimationSaturationChangeCheck();
};
