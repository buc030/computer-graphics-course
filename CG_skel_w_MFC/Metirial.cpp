#include "stdafx.h"
#include "Metirial.h"
#include <sstream>

CMetirial::CMetirial(CRGBColor diffuse, CRGBColor specular, CRGBColor ambient, CRGBColor emissive, float shinines) :
m_diffuse(diffuse), m_specular(specular), m_ambient(ambient), m_emissive(emissive), m_shinines(shinines)
{
}

CMetirial::CMetirial() 
{
}

CMetirial::~CMetirial()
{
}

CMetirial CMetirial::operator / (float i) const
{
	return CMetirial(m_diffuse / i, m_specular / i, m_ambient / i, m_emissive / i, m_shinines);
}

CMetirial CMetirial::operator *(float i) const
{
	return CMetirial(m_diffuse*i, m_specular*i, m_ambient*i, m_emissive*i, m_shinines);
}

CMetirial CMetirial::operator +(const CMetirial& other) const
{
	return CMetirial(m_diffuse + other.m_diffuse, m_specular + other.m_specular, m_ambient + other.m_ambient, m_emissive + other.m_emissive, m_shinines);
}

std::string CMetirial::print() const
{
	std::stringstream ss;
	ss << "Meterial " << m_diffuse << ", " << m_specular << ", " << m_ambient << ", " << m_emissive;
	return ss.str();
}

CMetirial CMetirial::random()
{
	return CMetirial(CRGBColor::random(), CRGBColor::random(), CRGBColor::random(), CRGBColor::random(), rand()%1000);
}