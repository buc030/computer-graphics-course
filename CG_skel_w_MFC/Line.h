#pragma once
#include "vec.h"

class IPolygon;

class CLine
{

public:
	vec2 m_p1;
	vec2 m_p2;
	CLine(const vec2& p1, const vec2& p2);
	~CLine();

	bool intersect(const CLine& other, double& a, double& aOther) const;
	bool intersect(const CLine& other, vec2& res) const;
	bool clip(int left, int right, int bottom, int top);
};
