
uniform sampler2D normalmap; 
uniform int normalMapping;

uniform mat4 modelMatrix; //ok
uniform mat4 modelviewInverse; //ok
uniform mat4 modelview; //ok
uniform mat4 normalsModelview; //ok


in struct Meterial {
   vec3 ambient;
   vec3 diffuse; 
   vec3 specular; 
   vec3 emissive;
   float shinines;
} meterial;
in vec3 N; //model coordinates
in vec3 L[MAX_LIGHTS];
in vec3 E;
in vec2 texCoord;
in vec3 fT;
in vec3 fB;

layout(location = 0) out vec4 fColor;

void main() 
{ 		
	
	if(normalMapping == 1)
	{
		
		vec3 NN = normalize(N);
		vec3 tangent = normalize((normalsModelview*vec4(fT, 0)).xyz);
		tangent = normalize(tangent - NN*dot(NN, tangent));
		vec3 bitangent = normalize(cross(N, tangent));
		mat3 TBN = transpose(
			mat3(
				tangent,
				bitangent, 
				NN));
		
		vec3 _E = TBN * E;
		vec3 _L[MAX_LIGHTS];
		for(int i = 0; i < MAX_LIGHTS && i < numLights; i++)
		{
			_L[i] = TBN * L[i];
		}

		vec3 tangentNormal = 2*texture2D(normalmap, texCoord).rgb - vec3(1.0);
		tangentNormal.z = texture2D(normalmap, texCoord).b;
		tangentNormal = normalize(tangentNormal);
		fColor =  vec4(calculateColor(meterial, tangentNormal, _L, _E), 1);
	}
	else
	{
		fColor = vec4(calculateColor(meterial,N,L,E), 1);
	}

} 

