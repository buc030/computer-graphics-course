#pragma once


// CLightsDialog dialog
#include <vector>
#include "Scene.h"
#include "LightPropertiesDialog.h"
class CLightsDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CLightsDialog)

	void reloadListBox();
public:
	virtual BOOL OnInitDialog();
	CLightsDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLightsDialog();

// Dialog Data
	enum { IDD = IDD_LIGHTS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	Scene* m_scene;
	CLightPropertiesDialog m_lightPropertyDlg;
	CListBox m_listBox;
	afx_msg void OnBnClickedAddLightButton();
	afx_msg void OnLbnDblclkLightsList();
	afx_msg void OnBnClickedRemoveLightButton();
	afx_msg void OnBnClickedSetActiveLightButton();
};
