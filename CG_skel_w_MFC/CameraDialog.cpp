// CameraDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "CameraDialog.h"
#include "afxdialogex.h"
#include <sstream>
#include "Camera.h"
// CCameraDialog dialog

IMPLEMENT_DYNAMIC(CCameraDialog, CDialogEx)

CCameraDialog::CCameraDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCameraDialog::IDD, pParent)
{

}

CCameraDialog::~CCameraDialog()
{
}

void CCameraDialog::DoDataExchange(CDataExchange* pDX)
{
	//CDialogEx::DoDataExchange(pDX);
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAMERAS_LIST, m_listBox);
	
}


BEGIN_MESSAGE_MAP(CCameraDialog, CDialogEx)
	ON_BN_CLICKED(IDC_SET_ACTIVE_CAMERA_BUTTON, &CCameraDialog::OnBnClickedSetActiveCameraButton)
	ON_BN_CLICKED(IDC_CHANGE_PARAMETERS, &CCameraDialog::OnBnClickedChangeParameters)
	ON_BN_CLICKED(IDC_PERSPECTIVE_RADIO, &CCameraDialog::OnBnClickedPerspectiveRadio)
	ON_BN_CLICKED(IDC_OTRHO_RADIO, &CCameraDialog::OnBnClickedOtrhoRadio)
END_MESSAGE_MAP()


BOOL CCameraDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetWindowText("Camera managment");
	for (int i = 0; i < m_cameras.size(); i++)
	{
		stringstream ss;
		ss << "#" << i << ":    " << m_cameras[i]->print();
		m_listBox.AddString(ss.str().c_str());
	}
	return TRUE;
}
void CCameraDialog::setCameras(std::vector<Camera*> cameras)
{
	m_cameras = cameras;
}
void CCameraDialog::setActiveCamera(int id)
{
	m_activeCamera = id;
}
// CCameraDialog message handlers


void CCameraDialog::OnBnClickedSetActiveCameraButton()
{
	int selected = m_listBox.GetCurSel();
	if (selected != -1)
		m_activeCamera = selected;
}


void CCameraDialog::OnBnClickedChangeParameters()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		return;
	}
	if (m_cameras[selected]->m_isPerspective)
	{
		if (m_perspectiveParamsDialog.DoModal() == IDOK)
		{
			m_cameras[selected]->m_perParams.m_fovy = m_perspectiveParamsDialog.m_fovy;
			m_cameras[selected]->m_perParams.m_aspect = m_perspectiveParamsDialog.m_aspect;
			m_cameras[selected]->m_perParams.m_zNear = m_perspectiveParamsDialog.m_zNear;
			m_cameras[selected]->m_perParams.m_zFar = m_perspectiveParamsDialog.m_zFar;
			m_cameras[selected]->Perspective();
		}
	}
	else
	{
		if (m_orthoParamsDialog.DoModal() == IDOK)
		{
			m_cameras[selected]->m_orthParams.m_left = m_orthoParamsDialog.m_left;
			m_cameras[selected]->m_orthParams.m_right = m_orthoParamsDialog.m_right;
			m_cameras[selected]->m_orthParams.m_bottom = m_orthoParamsDialog.m_bottom;
			m_cameras[selected]->m_orthParams.m_top = m_orthoParamsDialog.m_top;
			m_cameras[selected]->m_orthParams.m_zNear = m_orthoParamsDialog.m_zNear;
			m_cameras[selected]->m_orthParams.m_zFar = m_orthoParamsDialog.m_zFar;
			m_cameras[selected]->Ortho();
		}
	}

	m_listBox.ResetContent();
	//redraw
	for (int i = 0; i < m_cameras.size(); i++)
	{
		stringstream ss;
		ss << "#" << i << ":    " << m_cameras[i]->print();
		m_listBox.AddString(ss.str().c_str());
	}
}


void CCameraDialog::OnBnClickedPerspectiveRadio()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		return;
	}
	m_listBox.ResetContent();
	m_cameras[selected]->Perspective();
	//redraw
	for (int i = 0; i < m_cameras.size(); i++)
	{
		stringstream ss;
		ss << "#" << i << ":    " << m_cameras[i]->print();
		m_listBox.AddString(ss.str().c_str());
	}
}


void CCameraDialog::OnBnClickedOtrhoRadio()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		return;
	}
	m_listBox.ResetContent();
	m_cameras[selected]->Ortho();
	//redraw
	for (int i = 0; i < m_cameras.size(); i++)
	{
		stringstream ss;
		ss << "#" << i << ":    " << m_cameras[i]->print();
		m_listBox.AddString(ss.str().c_str());
	}

}
