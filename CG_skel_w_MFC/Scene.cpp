#include "stdafx.h"
#include "Scene.h"
#include "MeshModel.h"
#include <string>
#include "Transformation.h"
#include "PrimMeshModel.h"
#include "HungerianCubeMeshModel.h"
#include <sstream>
#include "Camera.h"
using namespace std;

void Scene::loadPrimitiveModel(ModelPrimitiveType type)
{
	MeshModel *model = NULL;
	if (type == CUBE)
	{
		model = new CubePrimMeshModel();
	}
	else if (type == HUNGERIAN_CUBE)
	{
		model = new HungerianCubeMeshModel();
	}


	if (model)
	{
		models.push_back(model);
		m_activeModel = models.size() - 1;
	}
}

void Scene::loadOBJModel(string fileName)
{
	
	MeshModel *model = new MeshModel(fileName);

	models.push_back(model);
	m_activeModel = models.size() - 1;
}

void Scene::draw()
{
	m_renderer->SetScene(this);
	
	// 1. Send the renderer the current camera transform and the projection
	// 2. Tell all models to draw themselves

	//TODO: uncomment
	m_renderer->DrawGrid();

	for (auto iter = models.begin(); iter != models.end(); iter++)
	{
		Model* model = *iter;
		
		model->draw(m_renderer);
	}

	
	//draw camera's:
	if (m_drawCameras)
	{
		for (int i = 0; i < cameras.size(); i++)
		{
			if (i == m_activeCamera)
			{
				continue;
			}
			cameras[i]->draw(m_renderer);
		}
	}

	for (int i = 0; i < m_lights.size(); i++)
	{
		m_lights[i]->draw(m_renderer);
	}
}

void Scene::resetActiveModelTransformations()
{
	models[m_activeModel]->resetTransformations();
}



void increaseDistance(float& v1, float& v2)
{
	if (v1 > v2)
	{
		v1 = v1 + 1;
		v2 = v2 - 1;
	}
	else
	{
		v1 = v1 - 1;
		v2 = v2 + 1;
	}
}
void decreaseDistance(float& v1, float& v2)
{
	if (v1 > v2)
	{
		v1 = v1 - 1;
		v2 = v2 + 1;
	}
	else
	{
		v1 = v1 + 1;
		v2 = v2 - 1;
	}
}
void Camera::zoomIn()
{
	if (m_isPerspective)
	{
		m_perParams.m_fovy = m_perParams.m_fovy - 1;
		Perspective();
	}
	else
	{
		decreaseDistance(m_orthParams.m_bottom, m_orthParams.m_top);
		decreaseDistance(m_orthParams.m_left, m_orthParams.m_right);
		Ortho();
	}
}
void Camera::zoomOut()
{
	if (m_isPerspective)
	{
		m_perParams.m_fovy = m_perParams.m_fovy + 1;
		Perspective();
	}
	else
	{
		increaseDistance(m_orthParams.m_bottom, m_orthParams.m_top);
		increaseDistance(m_orthParams.m_left, m_orthParams.m_right);
		Ortho();
	}
}
void Scene::zoomActiveCamera(Direction dir)
{
	if (dir == IN_DIR)
		cameras[m_activeCamera]->zoomIn();
	else if (dir == OUT_DIR)
		cameras[m_activeCamera]->zoomOut();
}

void Scene::rotateXActiveModel(GLfloat theta)
{
	if (!models.empty())
		models[m_activeModel]->rotateX(theta);
}

void Scene::rotateYActiveModel(GLfloat theta)
{
	if (!models.empty())
		models[m_activeModel]->rotateY(theta);
}

void Scene::scaleActiveModel(GLfloat x, GLfloat y, GLfloat z)
{
	if (!models.empty())
		models[m_activeModel]->scale(x, y, z);
}


void Scene::translateActiveModel(Direction d, GLfloat amount)
{
	Model* activeModel = models[m_activeModel];
	if (d == UP_DIR)
		activeModel->translate(0, amount, 0);
	else if (d == DOWN_DIR)
		activeModel->translate(0, -amount, 0);
	else if (d == LEFT_DIR)
		activeModel->translate(-amount, 0, 0);
	else if (d == RIGHT_DIR)
		activeModel->translate(amount, 0, 0);
}


void Scene::drawDemo()
{
	assert(false);
}

int Scene::addCamera(vec3 xyz)
{
	cameras.push_back(new Camera(m_renderer));
	cameras.back()->LookAt(vec4(xyz, 1), vec4(0, 0, -1, 1), vec4(0, 1, 0, 1));
	return cameras.size() - 1;
}

void Scene::setActiveModelBoundingBox(bool drawBoundingBox)
{
	if (!models.empty())
		models[m_activeModel]->m_drawBoundingBox = drawBoundingBox;
}


void Scene::toggleActiveCameraProjection()
{
	if (!cameras[m_activeCamera]->m_isPerspective)
	{
		cameras[m_activeCamera]->Perspective();
	}
	else
	{
		cameras[m_activeCamera]->Ortho();
	}

}


