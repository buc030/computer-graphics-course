#include "stdafx.h"
#include "Transformation.h"
#include "Scene.h"
#include "HungerianCubeMeshModel.h"
#include <assert.h>


Transformation::Transformation() : m_selectedTransformation(NOTHING), m_model(NULL)
{

}
Transformation::Transformation(SelectedTransformation transType, SelectedFrame frame, float step) : 
	m_selectedTransformation(transType),
	m_selectedFrame(frame),
	m_step(step),
	m_model(NULL)
{
}


Transformation::~Transformation()
{
}



Transformation Transformation::getInverseTransformation()
{
	if (m_selectedTransformation != SCALE)
	{
		return Transformation(m_selectedTransformation, m_selectedFrame, -m_step);
	}
	else
	{
		return Transformation(m_selectedTransformation, m_selectedFrame, 1.0/m_step);
	}
}




void Transformation::applyTransformation(Scene::Direction dir) const
{
	assert(m_model);
	Model* model = m_model;
	float step = m_step;
	if (dir == Scene::DOWN_DIR || dir == Scene::LEFT_DIR || dir == Scene::OUT_DIR)
	{
		if (m_selectedTransformation != SCALE)
		{
			step = -m_step;
		}
		else
		{
			step = 1 / m_step;
		}
	}

	if (m_selectedTransformation == ROTATE)
	{
		if (dir == Scene::UP_DIR || dir == Scene::DOWN_DIR)
			model->rotateX(step, m_selectedFrame == WORLD_FRAME);
		else if (dir == Scene::LEFT_DIR || dir == Scene::RIGHT_DIR)
			model->rotateY(step, m_selectedFrame == WORLD_FRAME);
		if (dir == Scene::IN_DIR || dir == Scene::OUT_DIR)
			model->rotateZ(step, m_selectedFrame == WORLD_FRAME);
	}
	else if (m_selectedTransformation == TRANSLATION)
	{
		if (dir == Scene::LEFT_DIR || dir == Scene::RIGHT_DIR)
			model->translate(step, 0, 0, m_selectedFrame == WORLD_FRAME);
		else if (dir == Scene::UP_DIR || dir == Scene::DOWN_DIR)
			model->translate(0, step, 0, m_selectedFrame == WORLD_FRAME);
		if (dir == Scene::IN_DIR || dir == Scene::OUT_DIR)
			model->translate(0, 0, step, m_selectedFrame == WORLD_FRAME);
	}
	else if (m_selectedTransformation == SCALE)
	{
		if (dir == Scene::LEFT_DIR || dir == Scene::RIGHT_DIR)
			model->scale(step, 1, 1, m_selectedFrame == WORLD_FRAME);
		else if (dir == Scene::UP_DIR || dir == Scene::DOWN_DIR)
			model->scale(1, step, 1, m_selectedFrame == WORLD_FRAME);
		if (dir == Scene::IN_DIR || dir == Scene::OUT_DIR)
			model->scale(1, 1, step, m_selectedFrame == WORLD_FRAME);
	}
	else if (m_selectedTransformation == HUNGERIAN_LEFT_FACE)
	{
		assert(dynamic_cast<HungerianCubeMeshModel*>(m_model));
		HungerianCubeMeshModel* hungerianModel = static_cast<HungerianCubeMeshModel*>(m_model);
		//hungerianModel->rotateLeftFace(step);
	}
	else if (m_selectedTransformation == HUNGERIAN_RIGHT_FACE)
	{
		assert(dynamic_cast<HungerianCubeMeshModel*>(m_model));
		HungerianCubeMeshModel* hungerianModel = static_cast<HungerianCubeMeshModel*>(m_model);
		//hungerianModel->rotateRightFace(step);
	}
	else if (m_selectedTransformation == HUNGERIAN_UP_FACE)
	{
		assert(dynamic_cast<HungerianCubeMeshModel*>(m_model));
		HungerianCubeMeshModel* hungerianModel = static_cast<HungerianCubeMeshModel*>(m_model);
		//hungerianModel->rotateUpFace(step);
	}
	else if (m_selectedTransformation == HUNGERIAN_DOWN_FACE)
	{
		assert(dynamic_cast<HungerianCubeMeshModel*>(m_model));
		HungerianCubeMeshModel* hungerianModel = static_cast<HungerianCubeMeshModel*>(m_model);
		//hungerianModel->rotateDownFace(step);
	}
	else if (m_selectedTransformation == HUNGERIAN_FRONT_FACE)
	{
		assert(dynamic_cast<HungerianCubeMeshModel*>(m_model));
		HungerianCubeMeshModel* hungerianModel = static_cast<HungerianCubeMeshModel*>(m_model);
		//hungerianModel->rotateFrontFace(step);
	}
	else if (m_selectedTransformation == HUNGERIAN_BACK_FACE)
	{
		assert(dynamic_cast<HungerianCubeMeshModel*>(m_model));
		HungerianCubeMeshModel* hungerianModel = static_cast<HungerianCubeMeshModel*>(m_model);
		//hungerianModel->rotateBackFace(step);
	}
	else
	{
		assert(false);
	}
}
