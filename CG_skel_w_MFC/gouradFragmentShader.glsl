uniform sampler2D normalmap; 
uniform int normalMapping;


//for normal mapping (this is actually phong)
in vec3 L[MAX_LIGHTS];
in vec3 E;
in vec2 texCoord;
in Meterial meterial;
in vec3 vertexColor;

layout(location = 0) out vec4 fColor;

void main() 
{ 
	
	if(normalMapping == 1)
	{
		vec3 tangentNormal = normalize(2*texture2D(normalmap, texCoord).rgb - vec3(1.0));
		fColor = vec4(calculateColor(meterial, tangentNormal, L, E), 1);
	}
	else
	{
		fColor = vec4(vertexColor, 1);
	}
	
} 

