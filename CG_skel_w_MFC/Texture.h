#pragma once

#include <string>
#include <vector>

class Texture
{
public:
	std::vector<unsigned char> m_data;
	int m_width;
	int m_height;
	Texture(const std::string& filename);
	~Texture();
};

