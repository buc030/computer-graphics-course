#pragma once

#include "MeshModel.h"
#include "Polygon.h"
class PrimMeshModel : public MeshModel
{
public:
	PrimMeshModel();
	virtual ~PrimMeshModel();
};

class CubePrimMeshModel : public PrimMeshModel
{
public:
	CubePrimMeshModel();
	virtual ~CubePrimMeshModel();
};
