#include "StdAfx.h"
#include "Renderer.h"
#include "CG_skel_w_MFC.h"
#include "InitShader.h"
#include "GL\freeglut.h"
#include <assert.h>
#include "Polygon.h"
#include "Line.h"
#include <algorithm>
#include <limits>
#include "Interpolator.h"
#include <set>
#include "Scene.h"
#include "MeshModel.h"
#include <sstream>
#include "PrimMeshModel.h"
#include "Camera.h"
#include "Texture.h"
template<class T>
void Renderer::setUniform(string name, T value)
{
	assert(false);

}


template<>
void Renderer::setUniform(string name, mat4 value)
{
	GLuint id = glGetUniformLocation(m_programs[m_activeProgram], name.c_str());
	glUniformMatrix4fv(id, 1, GL_FALSE, &value[0][0]);
}

template<>
void Renderer::setUniform(string name, vec2 value)
{
	GLuint id = glGetUniformLocation(m_programs[m_activeProgram], name.c_str());
	glUniform2f(id, value.x, value.y);
}

template<>
void Renderer::setUniform(string name, vec3 value)
{
	GLuint id = glGetUniformLocation(m_programs[m_activeProgram], name.c_str());
	glUniform3f(id, value.x, value.y, value.z);
}

template<>
void Renderer::setUniform(string name, vec4 value)
{
	GLuint id = glGetUniformLocation(m_programs[m_activeProgram], name.c_str());
	glUniform4f(id, value.x, value.y, value.z, value.w);

}

template<>
void Renderer::setUniform(string name, unsigned int value)
{
	GLuint id = glGetUniformLocation(m_programs[m_activeProgram], name.c_str());
	glUniform1i(id, value);
}


template<>
void Renderer::setUniform(string name, int value)
{
	GLuint id = glGetUniformLocation(m_programs[m_activeProgram], name.c_str());
	glUniform1i(id, value);
}

template<>
void Renderer::setUniform(string name, float value)
{

	GLuint id = glGetUniformLocation(m_programs[m_activeProgram], name.c_str());
	glUniform1f(id, value);

}



template<>
void Renderer::setUniform(string name, CMetirial value)
{

	setUniform(name + ".ambient", (vec3)value.m_ambient);
	setUniform(name + ".diffuse", (vec3)value.m_diffuse);
	setUniform(name + ".specular", (vec3)value.m_specular);
	setUniform(name + ".emissive", (vec3)value.m_emissive);

	setUniform(name + ".shinines", value.m_shinines);
}


//name = light[i]
template<>
void Renderer::setUniform(string name, CMetirial* value)
{
	setUniform(name + ".emissive", (vec3)value->m_emissive);
	setUniform(name + ".ambient", (vec3)value->m_ambient);
	setUniform(name + ".diffuse", (vec3)value->m_diffuse);
	setUniform(name + ".specular", (vec3)value->m_specular);
	setUniform(name + ".shinines", (float)value->m_shinines);
}

template<>
void Renderer::setUniform(string name, vector<CMetirial> value)
{
	for (int i = 0; i < value.size(); i++)
	{
		stringstream ss;
		ss << name << "[" << i << "]";
		setUniform(ss.str(), value[i]);
	}
}


//name = light[i]
template<>
void Renderer::setUniform(string name, ILight* value)
{
	vec4 lightLocCameraCoord = m_currentCameraTransform*value->getLocationInWorldCoordinates();
	if (value->m_isParalel)
	{
		lightLocCameraCoord.w = 0;
	}
	setUniform(name + ".position", lightLocCameraCoord);
	setUniform(name + ".ambient", (vec3)value->m_ambient);
	setUniform(name + ".diffuse", (vec3)value->m_diffuse);
	setUniform(name + ".specular", (vec3)value->m_specular);
}

template<>
void Renderer::setUniform(string name, vector<ILight*> value)
{
	for (int i = 0; i < value.size(); i++)
	{
		stringstream ss;
		ss << name << "[" << i << "]";
		setUniform(ss.str(), value[i]);
	}
}


void Renderer::resizeWindow(int width, int height)
{
	m_width = width;
	m_height = height;
	glViewport(0, 0, m_width, m_height);
}

Renderer::Renderer() :m_width(512), m_height(512), 
	m_currentProjectioonMatrix(otrogonalProjectionMatrix()),
	m_drawPerVertexNormals(false),
	m_drawPerFaceNormals(false),
	m_shadingType(FLAT),
	m_reflectionModel(PHONG_REFLECTION),
	m_mappingRederingPassModel(NULL)
{
	m_renderingFrameBufferStack.push_back(0);
	InitOpenGLRendering();
	resizeWindow(m_width, m_height);
}

Renderer::Renderer(int width, int height) : m_width(width), m_height(height), 
	m_currentProjectioonMatrix(otrogonalProjectionMatrix()),
	m_drawPerVertexNormals(false),
	m_drawPerFaceNormals(false),
	m_shadingType(FLAT),
	m_reflectionModel(PHONG_REFLECTION),
	m_mappingRederingPassModel(NULL)
{
	m_renderingFrameBufferStack.push_back(0);
	InitOpenGLRendering();
	resizeWindow(m_width, m_height);
}

Renderer::~Renderer(void)
{
}



void Renderer::SetScene(Scene* scene)
{
	m_currentCameraTransform = scene->cameras[scene->m_activeCamera]->getCameraTransform();
	m_currentCameraTransformInv = scene->cameras[scene->m_activeCamera]->getCameraTransformInverse();
	m_currentProjectioonMatrix = scene->cameras[scene->m_activeCamera]->projection;
	m_scene = scene;
}

//set the active program, and set the scene depedent uniform parameters
void Renderer::setActiveProgram(ProgramType p)
{
	glUseProgram(m_programs[p]);
	m_activeProgram = p;

	switch (p)
	{
		case RENDER_FLAT_MODEL_PROGRAM:
		case RENDER_GOURAD_MODEL_PROGRAM:
		case RENDER_PHONG_MODEL_PROGRAM:
			setUniform("projection", transpose(m_currentProjectioonMatrix));
			setUniform("numLights", (int)m_scene->m_lights.size());
			setUniform("lights", m_scene->m_lights);
			break;
		case RENDER_NORMAL_PER_FACE_PROGRAM:
		case RENDER_NORMAL_PER_VERTEX_PROGRAM:
			setUniform("projection", transpose(m_currentProjectioonMatrix));
			setUniform("normal_length", 0.2f);
			break;
		case RENDER_GRID_PROGRAM:
			setUniform("projection", transpose(m_currentProjectioonMatrix));
			setUniform("cameraTransform", transpose(m_currentCameraTransform));
			break;
		case RENDER_TO_SCREEN_PROGRAM:
			setUniform("projection", transpose(m_currentProjectioonMatrix));
			break;
		case RENDER_ADD_TEXTURE_PROGRAM:
			setUniform("projection", transpose(m_currentProjectioonMatrix));
			break;
		case RENDER_ENVIORMENT_MAPPING:
			setUniform("projection", transpose(m_currentProjectioonMatrix));
			setUniform("cameraMatrix", transpose(m_currentCameraTransform));
			break;
		case RENDER_SILHOUETTE_RENDERING:
			setUniform("projection", transpose(m_currentProjectioonMatrix));
			break;
		case RENDER_COLOR_ANIMATION:
		case RENDER_TOON_SHADING:
			setUniform("projection", transpose(m_currentProjectioonMatrix));
			break;
		default:
			assert(false);
	}

}

void Renderer::InitOpenGLRendering()
{

	// Dark blue background
	//glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Create and compile our GLSL program from the shaders
	
	m_programs[RENDER_GRID_PROGRAM] = InitShader("gridVertexShader.glsl", NULL, "fshader.glsl", NULL);

	m_programs[RENDER_FLAT_MODEL_PROGRAM] = InitShader("flatVertexShader.glsl", NULL, "flatFragmentshader.glsl", "lights.glsl");
	m_programs[RENDER_GOURAD_MODEL_PROGRAM] = InitShader("gouradVertexShader.glsl", NULL, "gouradFragmentShader.glsl", "lights.glsl");
	m_programs[RENDER_PHONG_MODEL_PROGRAM] = InitShader("phongVertexShader.glsl", NULL, "phongFragmentShader.glsl", "lights.glsl");
	

	m_programs[RENDER_NORMAL_PER_VERTEX_PROGRAM] = InitShader("mvVertexShader.glsl", "geomertyNormalPerVertexShader.glsl", "additiveFragmentShader.glsl", NULL);
	m_programs[RENDER_NORMAL_PER_FACE_PROGRAM] = InitShader("mvVertexShader.glsl", "geomertyNormalPerFaceShader.glsl", "additiveFragmentShader.glsl", NULL);
	

	m_programs[RENDER_ADD_TEXTURE_PROGRAM] = InitShader("applyTextureVertex.glsl", NULL, "applyTextureFragment.glsl", NULL);
	
	m_programs[RENDER_ENVIORMENT_MAPPING] = InitShader("envMapping.v", NULL, "envMapping.f", NULL);

	m_programs[RENDER_SILHOUETTE_RENDERING] = InitShader("silhouette.v", "silhouette.g", "silhouette.f", NULL);

	m_programs[RENDER_TOON_SHADING] = InitShader("toon.v", NULL, "toon.f", NULL);
	m_programs[RENDER_COLOR_ANIMATION] = InitShader("colorAnimation.v", NULL, "colorAnimation.f", NULL);
	

	m_programs[RENDER_TO_SCREEN_PROGRAM] = InitShader("renderToScreenVertexShader.glsl", NULL, "renderToScreenFragmentShader.glsl", NULL);
	//RENDER_ADD_TEXTURE_PROGRAM

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);//less or equal

}



void Renderer::ClearBuffers()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::SwapBuffers()
{
	// Clear the screen
	// Swap buffers
	glutSwapBuffers();
}

#include "CImg.h"
#include <sstream>

using namespace cimg_library;
void convertToNonInterleaved(int w, int h, unsigned char* tangled, unsigned char* untangled) {
    //Take string in format R1 G1 B1 R2 G2 B2... and re-write it 
    //in the format format R1 R2 G1 G2 B1 B2... 
    //Assume 8 bit values for red, green and blue color channels.
    //Assume there are no other channels
    //tangled is a pointer to the input string and untangled 
    //is a pointer to the output string. This method assumes that 
    //memory has already been allocated for the output string.

    int numPixels = w*h;
    int numColors = 3;
    for(int i=0; i<numPixels; ++i) {
        int indexIntoInterleavedTuple = numColors*i;
        //Red
        untangled[i] = tangled[indexIntoInterleavedTuple];
        //Green
        untangled[numPixels+i] = tangled[indexIntoInterleavedTuple+1];
        //Blue
        untangled[2*numPixels+i] = tangled[indexIntoInterleavedTuple+2];
    }
}

void Renderer::debugPopupCurrentDraw(GLuint fb)
{
	glBindFramebuffer(GL_FRAMEBUFFER, fb);
	std::vector<unsigned char> data(m_width*m_height*3);

	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0, 0, m_width, m_height, GL_RGB, GL_UNSIGNED_BYTE, &data[0]);

	std::vector<unsigned char> data2(data.size());
	convertToNonInterleaved(m_width, m_height, &data[0], &data2[0]);

	CImg<unsigned char> image(&data2[0], m_width, m_height, 1, 3, false); //image(&data[0]);

	image.display();
	glBindFramebuffer(GL_FRAMEBUFFER, m_renderingFrameBufferStack[m_renderingFrameBufferStack.size() - 1]);
}
void Renderer::renderEnviorment(std::vector<unsigned char>& data, GLuint color_rb, bool debug)
{	
	
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);//less or equal

	m_scene->draw();

	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0,0,m_width,m_height,GL_RGB,GL_UNSIGNED_BYTE,&data[0]);

	static int i = 0; i++;
	if (debug)
	{
		std::vector<unsigned char> data2(data.size());
		convertToNonInterleaved(m_width, m_height ,&data[0],&data2[0]);
		//std::stringstream ss;
		//ss << "file" << i << ".bmp";
		CImg<unsigned char> image(&data2[0],m_width, m_height, 1, 3, false); //image(&data[0]);
		//CImg<unsigned char> image(data[0]);
		//image.permute_axes("yzcx");
		image.display();
	}

	//image.save(ss.str().c_str());
	//CImgDisplay main_disp(image,"Snapshot");
}

void Renderer::CreateFrameBuffers(GLuint& fb, GLuint& color_rb, GLuint& depth_rb, int width, int height)
{
	int a = glGetError();
	//RGBA8 RenderBuffer, 24 bit depth RenderBuffer, 512x512
	glGenFramebuffers(1, &fb);a = glGetError();
	glBindFramebuffer(GL_FRAMEBUFFER, fb);a = glGetError();
	//Create and attach a color buffer
	glGenRenderbuffers(1, &color_rb);a = glGetError();
	//We must bind color_rb before we call glRenderbufferStorageEXT
	glBindRenderbuffer(GL_RENDERBUFFER, color_rb);a = glGetError();
	//The storage format is RGBA8
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, width, height); a = glGetError();
	//Attach color buffer to FBO
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, color_rb);a = glGetError();
	//-------------------------
	glGenRenderbuffers(1, &depth_rb);a = glGetError();
	glBindRenderbuffer(GL_RENDERBUFFER, depth_rb);a = glGetError();
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height); a = glGetError();
	//-------------------------
	//Attach depth buffer to FBO
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_rb);a = glGetError();
	//-------------------------
	//Does the GPU support current FBO configuration?
	GLenum status;
	status = glCheckFramebufferStatus(GL_FRAMEBUFFER);a = glGetError();
	assert(GL_FRAMEBUFFER_COMPLETE == status);
	glBindFramebuffer(GL_FRAMEBUFFER, m_renderingFrameBufferStack.back());
}



void Renderer::CalculateEnviormentCubeMap(MeshModel* model)
{
	EnvTexture textureObject;
	if (m_modelEnvTextureObject.find(model) == m_modelEnvTextureObject.end())
	{
		glGenTextures(1, &textureObject.m_textureBuffer);

		textureObject.m_takenPos = vec3FromVec4(model->getLocationInWorldCoordinates());
		m_modelEnvTextureObject[model] = textureObject;

	}

	textureObject = m_modelEnvTextureObject[model];
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureObject.m_textureBuffer);
	glEnable(GL_TEXTURE_CUBE_MAP_EXT);

	m_mappingRederingPassModel = model;
	GLuint fb, color_rb, depth_rb;
	int oldWidth = m_width;
	int oldHeight = m_height;
	resizeWindow(128, 128);
	CreateFrameBuffers(fb, color_rb, depth_rb, m_width, m_height);
	m_renderingFrameBufferStack.push_back(fb);
	glBindFramebuffer(GL_FRAMEBUFFER, m_renderingFrameBufferStack.back());

	Camera oldCamera = *m_scene->cameras[m_scene->m_activeCamera];
	Camera& tempCamera = *m_scene->cameras[m_scene->m_activeCamera];

	std::vector<unsigned char> data[NUM_OF_PLANES];
	for (int i = 0; i < NUM_OF_PLANES; i++) 
	{
		data[i].resize(m_width*m_height*3);
	}
	

	//vec4 pos = model->getLocationInWorldCoordinates();
	vec4 pos = model->getCenterOfMassInWorldCoordinates();
	float x_low, x_high, y_low, y_high, z_low, z_high;
	model->computeBoundingBox(x_low, x_high, y_low, y_high, z_low, z_high);

	float factor = 3;
	bool debug = false;
	bool perspectiveReflection = true;
	if (!perspectiveReflection)
	{
		tempCamera.m_orthParams.m_left = (x_low - x_high);
		tempCamera.m_orthParams.m_right = -tempCamera.m_orthParams.m_left;
		tempCamera.m_orthParams.m_top = (y_high - y_low);
		tempCamera.m_orthParams.m_bottom = -tempCamera.m_orthParams.m_top;

		tempCamera.m_orthParams.m_zFar = 0;
		tempCamera.m_orthParams.m_zFar = 100;
		tempCamera.Ortho();
	}
	else
	{
		tempCamera.m_perParams.m_fovy = 90;
		tempCamera.m_perParams.m_zFar = 100;
		tempCamera.m_perParams.m_aspect = 1;
		tempCamera.m_perParams.m_zNear = min(
			abs(x_high - x_low) / 2, 
			min(
				abs(y_high - y_low) / 2, 
				abs(z_high - z_low) / 2
				)
			) - FLT_EPSILON;

		tempCamera.Perspective();
	}
	
	
	//x
	tempCamera.LookAt(pos, pos + vec4(x_high, 0, 0, 0) + vec4(2, 0, 0, 0), vec4(0, -1, 0, 0));
	renderEnviorment(data[X_PLANE], fb, debug);
	tempCamera.LookAt(pos, pos + vec4(x_low, 0, 0, 0) + vec4(-2, 0, 0, 0), vec4(0, -1, 0, 0));
	renderEnviorment(data[NEG_X_PLANE], fb, debug);

	//y
	tempCamera.LookAt(pos, pos + vec4(0, y_high, 0, 0)  + vec4(0, 2, 0, 0), vec4(0, 0, -1, 0));
	renderEnviorment(data[Y_PLANE], fb, debug);
	tempCamera.LookAt(pos, pos + vec4(0, y_low, 0, 0)  + vec4(0, -2, 0, 0), vec4(0, 0, -1, 0));
	renderEnviorment(data[NEG_Y_PLANE], fb, debug);

	//z
	tempCamera.LookAt(pos, pos + vec4(0, 0, z_high, 0) + vec4(0, 0, 2, 0), vec4(0, -1, 0, 0));
	renderEnviorment(data[Z_PLANE], fb, debug);
	tempCamera.LookAt(pos, pos + vec4(0, 0, z_low, 0) + vec4(0, 0, -2, 0), vec4(0, -1, 0, 0));
	renderEnviorment(data[NEG_Z_PLANE], fb, debug);

	for (int i = 0; i < NUM_OF_PLANES; i++) 
	{
	  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X_EXT + i, 
		0,                  //level 
		GL_RGB8,            //internal format 
		m_width,                  //width 
		m_height,                 //height 
		0,                  //border 
		GL_RGB,             //format 
		GL_UNSIGNED_BYTE,   //type 
		&data[i][0]); // pixel data 
	}

	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	*m_scene->cameras[m_scene->m_activeCamera] = oldCamera;
	SetScene(m_scene);
	resizeWindow(oldWidth, oldHeight);
	glDeleteFramebuffers(1, &fb);
	glDeleteRenderbuffers(1, &color_rb);
	glDeleteRenderbuffers(1, &depth_rb);
	m_renderingFrameBufferStack.pop_back();
	glBindFramebuffer(GL_FRAMEBUFFER, m_renderingFrameBufferStack.back());

	m_mappingRederingPassModel = NULL;
	
}

void Renderer::createTextureBuffer(Texture* texture)
{
	int a = glGetError();
	glEnable(GL_TEXTURE_2D);
	a = glGetError();
	GLuint textureObject;
	
	glGenTextures(1, &textureObject); a = glGetError();
	glBindTexture(GL_TEXTURE_2D, textureObject); a = glGetError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); a = glGetError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); a = glGetError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,
		0,
		GL_RGB,
		(GLsizei)texture->m_width,
		(GLsizei)texture->m_height,
		0,
		GL_RGB,
		GL_UNSIGNED_BYTE,
		&texture->m_data[0]); a = glGetError();
		//texture->m_image.data());
	glBindTexture(GL_TEXTURE_2D, 0); a = glGetError();
	m_textureToTextureGlObject[texture] = textureObject;
}

void Renderer::loadModelTextureCoordinates(MeshModel* model)
{
	std::vector<GLfloat> UVs;
	if(!model->getOpenglTextureData(UVs))
	{
		return;
	}

	int a;
	GLuint textCoordBuffer;
	if(!m_modelToVBOs[model].isAvailable(TEXTURE_COORD_VBO))
	{
		glGenBuffers(1, &textCoordBuffer); a = glGetError();
		m_modelToVBOs[model].setVbo(TEXTURE_COORD_VBO, textCoordBuffer);
	}
	textCoordBuffer = m_modelToVBOs[model].getVbo(TEXTURE_COORD_VBO);

	glBindBuffer(GL_ARRAY_BUFFER, textCoordBuffer); a = glGetError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*UVs.size(), &UVs[0], GL_STATIC_DRAW); a = glGetError();
	
}

void Renderer::loadModelNormalTextureCoordinates(MeshModel* model)
{
	std::vector<GLfloat> normalUVs;
	std::vector<GLfloat> tangents;
	std::vector<GLfloat> bitangents;
	if(!model->getOpenglNormalTextureData(normalUVs, tangents, bitangents))
	{
		return;
	}

	int a;
	GLuint textCoordBuffer;
	GLuint tangentCoordBuffer;
	GLuint bitangentCoordBuffer;
	if(!m_modelToVBOs[model].isAvailable(NORMAL_TEXTURE_COORD_VBO))
	{
		glGenBuffers(1, &textCoordBuffer); a = glGetError();
		glGenBuffers(1, &tangentCoordBuffer); a = glGetError();
		glGenBuffers(1, &bitangentCoordBuffer); a = glGetError();

		m_modelToVBOs[model].setVbo(NORMAL_TEXTURE_COORD_VBO, textCoordBuffer);
		m_modelToVBOs[model].setVbo(TANGENT_COORD_VBO, tangentCoordBuffer);
		m_modelToVBOs[model].setVbo(BITANGENT_COORD_VBO, bitangentCoordBuffer);
	}
	textCoordBuffer = m_modelToVBOs[model].getVbo(NORMAL_TEXTURE_COORD_VBO);
	tangentCoordBuffer = m_modelToVBOs[model].getVbo(TANGENT_COORD_VBO);
	bitangentCoordBuffer = m_modelToVBOs[model].getVbo(BITANGENT_COORD_VBO);


	glBindBuffer(GL_ARRAY_BUFFER, textCoordBuffer); a = glGetError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*normalUVs.size(), &normalUVs[0], GL_STATIC_DRAW); a = glGetError();
	m_modelToVBOs[model].setVbo(NORMAL_TEXTURE_COORD_VBO, textCoordBuffer);

	assert(!tangents.empty());
	//pass tangents coordinates:
	
	glBindBuffer(GL_ARRAY_BUFFER, tangentCoordBuffer); a = glGetError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*tangents.size(), &tangents[0], GL_STATIC_DRAW); a = glGetError();
	m_modelToVBOs[model].setVbo(TANGENT_COORD_VBO, tangentCoordBuffer);

	assert(!bitangents.empty());
	//pass bitangents coordinates:
	
	glBindBuffer(GL_ARRAY_BUFFER, bitangentCoordBuffer); a = glGetError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*bitangents.size(), &bitangents[0], GL_STATIC_DRAW); a = glGetError();
	m_modelToVBOs[model].setVbo(BITANGENT_COORD_VBO, bitangentCoordBuffer);
	
}

void Renderer::unloadModel(MeshModel* model)
{
	if (m_modelToVAO.find(model) == m_modelToVAO.end())
	{
		return;
	}
	
	GLuint vertexArrayObject = m_modelToVAO[model];
	glDeleteVertexArrays(1, &vertexArrayObject);

	VBOInfo vbo = m_modelToVBOs[model];
	for (int i = VERTEXES_VBO; i < NUM_OF_VBOS; i++)
	{
		if (vbo.isAvailable((VBOType)i))
		{
			GLuint temp = vbo.getVbo((VBOType)i);
			glDeleteBuffers(1, &temp);
		}
	}
	m_modelToVAO.erase(model);
	m_modelToVBOs.erase(model);
	
}
void Renderer::createModelVertexBuffer(MeshModel* model)
{
	//std::vector<std::vector<int> > adjList;
	std::vector<GLfloat> vertexes;
	std::vector<GLfloat> normals;

	std::vector<GLfloat> neigbour1Normals;
	std::vector<GLfloat> neigbour2Normals;

	model->getOpenglData(vertexes, normals, neigbour1Normals, neigbour2Normals);
	
	int polygonCount = model->getPolygonsCount();

	int a = glGetError();
	GLuint vertexArrayObject;
	//m_modelToVAO
	//create vertex array object for this model:
	glGenVertexArrays(1, &vertexArrayObject); a = glGetError(); assert(!a);
	glBindVertexArray(vertexArrayObject); a = glGetError(); assert(!a);
	m_modelToVAO[model] = vertexArrayObject;

	
	//pass vertexes:
	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer); a = glGetError(); assert(!a);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer); a = glGetError(); assert(!a);
	//copy the data to the graphic card memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*polygonCount * 3 * 3, &vertexes[0], GL_STATIC_DRAW); a = glGetError(); assert(!a);
	m_modelToVBOs[model].setVbo(VERTEXES_VBO, vertexbuffer);


	//pass normals:
	GLuint normalsBuffer;
	glGenBuffers(1, &normalsBuffer); a = glGetError();
	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer); a = glGetError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*polygonCount * 3 * 3, &normals[0], GL_STATIC_DRAW); a = glGetError();
	m_modelToVBOs[model].setVbo(NORMALS_VBO, normalsBuffer);

	if (!neigbour1Normals.empty())
	{
		//pass neigbour1Normals:
		GLuint neig1NormalsBuffer;
		glGenBuffers(1, &neig1NormalsBuffer); a = glGetError();
		glBindBuffer(GL_ARRAY_BUFFER, neig1NormalsBuffer); a = glGetError();
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*polygonCount * 3 * 3, &neigbour1Normals[0], GL_STATIC_DRAW); a = glGetError();
		m_modelToVBOs[model].setVbo(NEIGBOUR1_VBO, neig1NormalsBuffer);
	}


	if (!neigbour2Normals.empty())
	{
		//pass neigbour2Normals:
		GLuint neig2NormalsBuffer;
		glGenBuffers(1, &neig2NormalsBuffer); a = glGetError();
		glBindBuffer(GL_ARRAY_BUFFER, neig2NormalsBuffer); a = glGetError();
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*polygonCount * 3 * 3, &neigbour2Normals[0], GL_STATIC_DRAW); a = glGetError();
		m_modelToVBOs[model].setVbo(NEIGBOUR2_VBO, neig2NormalsBuffer);
	}

	vector<GLuint> meterialIdxesVec;
	model->getMeterialIndexes(meterialIdxesVec);
	int temp = polygonCount * 3;
	if (meterialIdxesVec.empty())
	{
		meterialIdxesVec.resize(polygonCount * 3, (GLint)0);
	}
	assert(meterialIdxesVec.size() > 0);
	GLfloat* meterialIdxes = new GLfloat[polygonCount * 3];
	for (int i = 0; i < polygonCount * 3; i++)
	{
		meterialIdxes[i] = (GLfloat)meterialIdxesVec[i];
	}
	//pass meterialIdxes:
	GLuint meterialIdxesBuffer;
	glGenBuffers(1, &meterialIdxesBuffer); a = glGetError();
	glBindBuffer(GL_ARRAY_BUFFER, meterialIdxesBuffer); a = glGetError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*polygonCount * 3, meterialIdxes, GL_STATIC_DRAW); a = glGetError();
	m_modelToVBOs[model].setVbo(METERIAL_IDXES_VBO, meterialIdxesBuffer);

	loadModelTextureCoordinates(model);
	loadModelNormalTextureCoordinates(model);

	glBindVertexArray(0);
};

void Renderer::unbindModelVertexBuffer(MeshModel* model)
{
	VBOInfo& vbos = m_modelToVBOs[model];
	int a;
	glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(VERTEXES_VBO)); a = glGetError();
	GLint loc = glGetAttribLocation(m_programs[m_activeProgram], "vPosition"); a = glGetError();
	glDisableVertexAttribArray(loc); a = glGetError();


	glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NORMALS_VBO)); a = glGetError();
	loc = glGetAttribLocation(m_programs[m_activeProgram], "normal"); a = glGetError();
	glDisableVertexAttribArray(loc); a = glGetError();

	if (vbos.isAvailable(NEIGBOUR1_VBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NEIGBOUR1_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "neig1Normal"); a = glGetError();
		glDisableVertexAttribArray(loc); a = glGetError();
	}


	if (vbos.isAvailable(NEIGBOUR2_VBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NEIGBOUR2_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "neig2Normal"); a = glGetError();
		glDisableVertexAttribArray(loc); a = glGetError();
	}


	if (vbos.isAvailable(TEXTURE_COORD_VBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(TEXTURE_COORD_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "vTextCoord"); a = glGetError();
		glDisableVertexAttribArray(loc); a = glGetError();
		glBindVertexArray(0);

	}

	if (vbos.isAvailable(NORMAL_TEXTURE_COORD_VBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NORMAL_TEXTURE_COORD_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "vNormalTextCoord"); a = glGetError();
		glDisableVertexAttribArray(loc); a = glGetError();
		

		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(TANGENT_COORD_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "vT"); a = glGetError();
		glDisableVertexAttribArray(loc); a = glGetError();


		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(BITANGENT_COORD_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "vB"); a = glGetError();
		glDisableVertexAttribArray(loc); a = glGetError();
		glBindVertexArray(0);
	}

	assert(vbos.isAvailable(METERIAL_IDXES_VBO));
	
	glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(METERIAL_IDXES_VBO)); a = glGetError();
	loc = glGetAttribLocation(m_programs[m_activeProgram], "meterialIdx"); a = glGetError();
	glDisableVertexAttribArray(loc); a = glGetError();
	
}

void Renderer::bindModelVertexBuffer(MeshModel* model)
{
	GLuint vertexArrayObject = m_modelToVAO[model];
	glBindVertexArray(vertexArrayObject);

	VBOInfo& vbos = m_modelToVBOs[model];
	int a;
	glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(VERTEXES_VBO)); a = glGetError();
	GLint loc = glGetAttribLocation(m_programs[m_activeProgram], "vPosition"); a = glGetError();
	glEnableVertexAttribArray(loc); a = glGetError();
	glVertexAttribPointer(loc, 3 /*num of floats in normal*/, GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();

	glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NORMALS_VBO)); a = glGetError();
	loc = glGetAttribLocation(m_programs[m_activeProgram], "normal"); a = glGetError();
	glEnableVertexAttribArray(loc); a = glGetError();
	glVertexAttribPointer(loc, 3 /*num of floats in normal*/, GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();

	if (vbos.isAvailable(NEIGBOUR1_VBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NEIGBOUR1_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "neig1Normal"); a = glGetError();
		glEnableVertexAttribArray(loc); a = glGetError();
		glVertexAttribPointer(loc, 3 /*num of floats in normal*/, GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();
	}

	if (vbos.isAvailable(NEIGBOUR2_VBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NEIGBOUR2_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "neig2Normal"); a = glGetError();
		glEnableVertexAttribArray(loc); a = glGetError();
		glVertexAttribPointer(loc, 3 /*num of floats in normal*/, GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();
	}

	if (vbos.isAvailable(TEXTURE_COORD_VBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(TEXTURE_COORD_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "vTextCoord"); a = glGetError();
		glEnableVertexAttribArray(loc); a = glGetError();
		glVertexAttribPointer(loc, 2 /*num of floats in normal*/, GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();
	}

	if (vbos.isAvailable(NORMAL_TEXTURE_COORD_VBO))
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NORMAL_TEXTURE_COORD_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "vNormalTextCoord"); a = glGetError();
		glEnableVertexAttribArray(loc); a = glGetError();
		glVertexAttribPointer(loc, 2 /*num of floats in normal*/, GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();

		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(TANGENT_COORD_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "vT"); a = glGetError();
		glEnableVertexAttribArray(loc); a = glGetError();
		glVertexAttribPointer(loc, 3 /*num of floats in normal*/, GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();

		glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(BITANGENT_COORD_VBO)); a = glGetError();
		loc = glGetAttribLocation(m_programs[m_activeProgram], "vB"); a = glGetError();
		glEnableVertexAttribArray(loc); a = glGetError();
		glVertexAttribPointer(loc, 3 /*num of floats in normal*/, GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();
	}
	
	assert(vbos.isAvailable(METERIAL_IDXES_VBO));
	
	glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(METERIAL_IDXES_VBO)); a = glGetError();
	loc = glGetAttribLocation(m_programs[m_activeProgram], "meterialIdx"); a = glGetError();
	glEnableVertexAttribArray(loc); a = glGetError();
	glVertexAttribPointer(loc, 1 , GL_FLOAT, GL_FALSE, 0, 0); a = glGetError();
	
}



void Renderer::DrawGrid(int width, int height)
{
	static GLuint vertexArrayObject;
	if(m_modelToVAO.find(NULL) == m_modelToVAO.end())
	{
		glGenVertexArrays(1, &vertexArrayObject); 
		m_modelToVAO[NULL] = vertexArrayObject;
	}

	glBindVertexArray(vertexArrayObject);

	setActiveProgram(RENDER_GRID_PROGRAM);
	setUniform("dim", vec2(width, height));
	glDrawArrays(GL_TRIANGLES, 0, width * height * 2 * 3);
	glBindVertexArray(0);
}

void Renderer::SetWireframeOnly(bool wireFrameOnly)
{
	glPolygonMode(GL_FRONT_AND_BACK, (wireFrameOnly ? GL_LINE : GL_FILL));
}

Renderer::ProgramType Renderer::getRequestedShadingProgram()
{
	switch (m_shadingType)
	{
	case FLAT:
		return RENDER_FLAT_MODEL_PROGRAM;
	case GOURAUND:
		return RENDER_GOURAD_MODEL_PROGRAM;
	case PHONG:
		return RENDER_PHONG_MODEL_PROGRAM;
	}
}


void Renderer::DrawCamera(Camera* camera)
{
	if (m_modelToVAO.find(Camera::m_coordinatesSystem) == m_modelToVAO.end())
	{
		createModelVertexBuffer(Camera::m_coordinatesSystem);
	}

	setActiveProgram(RENDER_GOURAD_MODEL_PROGRAM);
	bindModelVertexBuffer(Camera::m_coordinatesSystem);
	setUniform("modelview", transpose(m_currentCameraTransform*camera->getModelMatrix()*Camera::m_coordinatesSystem->getModelMatrix()));
	setUniform("normalsModelview", camera->getModelInverseMatrix()*m_currentCameraTransformInv);
	setUniform("allMeterials", Camera::m_coordinatesSystem->getMeterials());
	setUniform("numMeterials", Camera::m_coordinatesSystem->getMeterials().size());
	glDrawArrays(GL_TRIANGLES, 0, 3 * Camera::m_coordinatesSystem->getPolygonsCount());
	unbindModelVertexBuffer(Camera::m_coordinatesSystem);
}

void Renderer::DrawLight(ILight* light)
{
	if (m_modelToVAO.find(ILight::m_sphere) == m_modelToVAO.end())
	{
		createModelVertexBuffer(ILight::m_sphere);
	}

	setActiveProgram(RENDER_FLAT_MODEL_PROGRAM);
	bindModelVertexBuffer(ILight::m_sphere);
	setUniform("modelview", transpose(m_currentCameraTransform*light->getModelMatrix()*ILight::m_sphere->getModelMatrix()));
	setUniform("normalsModelview", light->getModelInverseMatrix()*m_currentCameraTransformInv);
	setUniform("allMeterials", ILight::m_sphere->getMeterials());
	setUniform("numMeterials", ILight::m_sphere->getMeterials().size());
	glDrawArrays(GL_TRIANGLES, 0, 3 * ILight::m_sphere->getPolygonsCount());
	unbindModelVertexBuffer(ILight::m_sphere);

}


void Renderer::DrawMeshModelMultiPass(MeshModel* model, GLuint colorTex, GLuint depthTex)
{
	//if this is the first time, create a buffer:
	if (m_modelToVAO.find(model) == m_modelToVAO.end())
	{
		createModelVertexBuffer(model);
	}


	unbindModelVertexBuffer(model);
	setActiveProgram(getRequestedShadingProgram());
	bindModelVertexBuffer(model);

	if (model->getActiveNormalTexture())
	{
		if (m_textureToTextureGlObject.find(model->getActiveNormalTexture()) == m_textureToTextureGlObject.end())
		{
			createTextureBuffer(model->getActiveNormalTexture());
		}

		GLuint textureObject = m_textureToTextureGlObject[model->getActiveNormalTexture()];
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textureObject);
		setUniform("normalMapping", 1);
		setUniform("normalmap", 1);
	}
	else
	{
		setUniform("normalMapping", 0);
		VBOInfo& vbos = m_modelToVBOs[model];
		if (vbos.isAvailable(NORMAL_TEXTURE_COORD_VBO))
		{
			glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(TANGENT_COORD_VBO));
			GLuint loc = glGetAttribLocation(m_programs[m_activeProgram], "vT");
			glDisableVertexAttribArray(loc);

			glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(BITANGENT_COORD_VBO));
			loc = glGetAttribLocation(m_programs[m_activeProgram], "vB");
			glDisableVertexAttribArray(loc);

			glBindBuffer(GL_ARRAY_BUFFER, vbos.getVbo(NORMAL_TEXTURE_COORD_VBO));
			loc = glGetAttribLocation(m_programs[m_activeProgram], "vNormalTextCoord");
			glDisableVertexAttribArray(loc);
		}
	}

	if (model->m_vertexAnimation)
	{
		if (m_meshModelTime.find(model) == m_meshModelTime.end())
		{
			m_meshModelTime[model] = 0;
		}
		
		setUniform("time", m_meshModelTime[model]);
		setUniform("vertexAnimation", 1);
	}
	else
	{
		setUniform("vertexAnimation", 0);
	}


	
	setUniform("modelMatrix", transpose(model->getModelMatrix()));
	setUniform("cameraMatrix", transpose(m_currentCameraTransform));
	setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
	setUniform("modelviewInverse", transpose(model->getModelInverseMatrix()*m_currentCameraTransformInv));
	setUniform("normalsModelview", model->getModelInverseMatrix()*m_currentCameraTransformInv);
	setUniform("enviormentMapping", (model->m_enviormentMapping ? 1 : 0));
	setUniform("allMeterials", model->getMeterials());
	setUniform("numMeterials", model->getMeterials().size());

	glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());

	if (m_drawPerFaceNormals)
	{
		unbindModelVertexBuffer(model);
		setActiveProgram(RENDER_NORMAL_PER_FACE_PROGRAM);
		bindModelVertexBuffer(model);
		setUniform("normalsModelview", model->getModelInverseMatrix()*m_currentCameraTransformInv);
		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
		glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());
	}

	if (m_drawPerVertexNormals)
	{
		unbindModelVertexBuffer(model);
		setActiveProgram(RENDER_NORMAL_PER_VERTEX_PROGRAM);
		bindModelVertexBuffer(model);
		setUniform("normalsModelview", model->getModelInverseMatrix()*m_currentCameraTransformInv);
		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
		glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());
	}

	if (model->getActiveTexture())
	{
		unbindModelVertexBuffer(model);
		setActiveProgram(RENDER_ADD_TEXTURE_PROGRAM);
		bindModelVertexBuffer(model);
		if (m_textureToTextureGlObject.find(model->getActiveTexture()) == m_textureToTextureGlObject.end())
		{
			createTextureBuffer(model->getActiveTexture());
		}
		GLuint textureObject = m_textureToTextureGlObject[model->getActiveTexture()];
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorTex);
		setUniform("renderedTexture", 0);

		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textureObject);
		setUniform("textureToAdd", 1);
		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
		glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());
	}

	if (model->m_enviormentMapping && !m_mappingRederingPassModel)
	{
		
		CalculateEnviormentCubeMap(model);
		unbindModelVertexBuffer(model);
		setActiveProgram(RENDER_ENVIORMENT_MAPPING);
		bindModelVertexBuffer(model);

		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, colorTex);
		setUniform("renderedTexture", 0);

		EnvTexture textureObject = m_modelEnvTextureObject[model];
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_CUBE_MAP);
		glBindTexture(GL_TEXTURE_CUBE_MAP, textureObject.m_textureBuffer);
		setUniform("cubeTexture", 1);

		setUniform("cameraMatrix", transpose(m_currentCameraTransform));

		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
		setUniform("normalsModelview", model->getModelInverseMatrix()*m_currentCameraTransformInv);
		glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());
		
	}

	if (model->m_silhoutteRendering)
	{
		unbindModelVertexBuffer(model);
		setActiveProgram(RENDER_SILHOUETTE_RENDERING);
		bindModelVertexBuffer(model);

		setUniform("linesColor", vec3(1,0,0));
		setUniform("thickness", 0.1f);
		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
		setUniform("normalsModelview", model->getModelInverseMatrix()*m_currentCameraTransformInv);
		glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());

	}

	if(model->m_toonShading)
	{
		unbindModelVertexBuffer(model);
		setActiveProgram(RENDER_SILHOUETTE_RENDERING);
		bindModelVertexBuffer(model);
		
		setUniform("linesColor", vec3(0.1,0.1,0.1));
		setUniform("thickness", 0.1f);
		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
		setUniform("normalsModelview", model->getModelInverseMatrix()*m_currentCameraTransformInv);
		glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());


		unbindModelVertexBuffer(model);
		setActiveProgram(RENDER_TOON_SHADING);
		bindModelVertexBuffer(model);
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, colorTex);
		setUniform("renderedTexture", 0);


		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));

		setUniform("numOfColors", 4);
		setUniform("horizontalBlur", 0);setUniform("verticalBlur", 0);setUniform("reduceColors", 1);
		glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());
	}

	if (model->m_colorAnimationHueChange || model->m_colorAnimationSaturationChange)
	{
		if (m_meshModelTime.find(model) == m_meshModelTime.end())
		{
			m_meshModelTime[model] = 0;
		}
		unbindModelVertexBuffer(model);
		setActiveProgram(RENDER_COLOR_ANIMATION);
		bindModelVertexBuffer(model);
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, colorTex);
		setUniform("renderedTexture", 0);
		setUniform("time", m_meshModelTime[model]);
		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
		if (model->m_colorAnimationHueChange)
		{
			setUniform("hueChange", 1);
			glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());
		}
		
		if (model->m_colorAnimationSaturationChange)
		{
			setUniform("hueChange", 0);
			glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());
		}
	}
}


void Renderer::DrawMeshModel(MeshModel* model)
{
	if (m_mappingRederingPassModel == model)
	{
		return;
	}

	//GLuint fb, color_rb, depth_rb;
	//CreateFrameBuffers(fb, color_rb, depth_rb, m_width, m_height);
	GLuint fb;
	glGenFramebuffers(1, &fb);
	m_renderingFrameBufferStack.push_back(fb);
	glBindFramebuffer(GL_FRAMEBUFFER, m_renderingFrameBufferStack.back());

	//create texture to holod intermediate results
	glActiveTexture(GL_TEXTURE0);
	GLuint colorTex, depthTex, fbo;
	glGenTextures(1, &colorTex);
	glBindTexture(GL_TEXTURE_2D, colorTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenTextures(1, &depthTex);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	int a;
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, m_width, m_height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	a = glGetError();
	glBindFramebuffer(GL_FRAMEBUFFER, m_renderingFrameBufferStack.back());
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorTex, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTex, 0);

	GLenum drawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);

	GLenum e = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	assert(e == GL_FRAMEBUFFER_COMPLETE);

	
	glBindFramebuffer(GL_FRAMEBUFFER, m_renderingFrameBufferStack.back());

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);//less or equal

	//render all the effects chosen by user
	DrawMeshModelMultiPass(model, colorTex, depthTex);

	m_renderingFrameBufferStack.pop_back();
	//render texture to screen:
	glBindFramebuffer(GL_FRAMEBUFFER, m_renderingFrameBufferStack.back());
	unbindModelVertexBuffer(model);
	setActiveProgram(RENDER_TO_SCREEN_PROGRAM);
	bindModelVertexBuffer(model);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, colorTex);
	// Set our "renderedTexture" sampler to user Texture Unit 0
	setUniform("renderedTexture", 0);
	setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()));
	if (model->m_vertexAnimation)
	{
		assert(m_meshModelTime.find(model) != m_meshModelTime.end());
		setUniform("time", m_meshModelTime[model]);
		setUniform("vertexAnimation", 1);
	}
	else
	{
		setUniform("vertexAnimation", 0);
	}

	glDrawArrays(GL_TRIANGLES, 0, 3 * model->getPolygonsCount());

	glDeleteTextures(1, &colorTex);
	glDeleteTextures(1, &depthTex);
	glDeleteFramebuffers(1, &fb);


	unbindModelVertexBuffer(model);


	if (model->m_drawBoundingBox)
	{
		GLint* oldWireframe = new GLint[2];
		glGetIntegerv(GL_POLYGON_MODE, oldWireframe);
		setActiveProgram(RENDER_FLAT_MODEL_PROGRAM);
		unbindModelVertexBuffer(model);
		
		SetWireframeOnly(true);
		MeshModel* boundingBox = model->getBoundingBox();
		if (m_modelToVAO.find(boundingBox) == m_modelToVAO.end())
		{
			createModelVertexBuffer(boundingBox);
		}
		
		bindModelVertexBuffer(boundingBox);
		vector<CMetirial> meterials(1, CMetirial(CRGBColor(), CRGBColor(), CRGBColor(), CRGBColor(1, 0, 0), 1));
		setUniform("allMeterials", meterials);
		setUniform("modelview", transpose(m_currentCameraTransform*model->getModelMatrix()*boundingBox->getModelMatrix()));
		glDrawArrays(GL_TRIANGLES, 0, 3 * boundingBox->getPolygonsCount());
		unbindModelVertexBuffer(boundingBox);
		bindModelVertexBuffer(model);
		SetWireframeOnly(oldWireframe[0] == GL_LINE);
		
		delete [] oldWireframe;
	}

	if (model->m_colorAnimationHueChange || model->m_colorAnimationSaturationChange || model->m_vertexAnimation)
	{
		m_meshModelTime[model]++;
	}
}
