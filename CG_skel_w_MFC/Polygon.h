#pragma once

#include "vec.h"
#include <vector>
#include <string>
#include <map>
#include "mat.h"
#include <assert.h>
#include "Metirial.h"


struct FaceIdcs
{
	std::vector<int> v;
	std::vector<int> vn;
	std::vector<int> vt;
	FaceIdcs();
	FaceIdcs(std::istream & aStream);
};


class IPolygon
{
	const std::vector<vec3>& m_vertexNormals;
	const std::vector<vec3>& m_vertexes;
	const std::map<std::string, std::vector<vec2> >& m_textureCoordinates;

	std::vector<int> m_vertexIdxes;
	std::vector<int> m_normalIdxes;
	std::map<std::string,std::vector<int> > m_textureIdxes;
public:

	IPolygon& operator=(const IPolygon& other);
	IPolygon(const std::vector<vec3>& vertices, const std::vector<vec3>& vertexNormals, const std::map<std::string, std::vector<vec2> >& textureCoordinates);
	~IPolygon();

	void setVertexes(const std::vector<int>& vertexIdxes);
	void setVertexNormals(const std::vector<int>& normalIdxes);
	void setTextureCoordinates(const std::string& texName, const std::vector<int>& textCoordIdexes);
	bool hasTextureCoordinates(const std::string& texName) const;

	vec4 getVertex(int idx) const;
	const vec3& getNormal(int idx) const;
	const vec2& getTextureCoord(const std::string& texName, int idx) const;

	const std::vector<int>& getVertexesIndexes() const;
	std::string print() const;
	vec3 getNormal() const;
};
