#pragma once

#include "Transformation.h";


class CRotationDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CRotationDialog)

public:
	CRotationDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRotationDialog();

// Dialog Data
	enum { IDD = IDD_ROTATION_DIALOG };



	RotationTransformation m_transformation;


protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedXRadio();
	afx_msg void OnBnClickedYRadio();
	afx_msg void OnBnClickedZRadio();
	afx_msg void OnBnClickedWorldFrameRadio();
	afx_msg void OnBnClickedModelFrameRadio();
};
