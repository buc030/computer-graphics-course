#include "stdafx.h"
#include "HungerianCubeMeshModel.h"
#include <set>
#include <map>
#include <list>
#include <unordered_set>
#include <queue>
#include <algorithm>
#include "GL/freeglut.h"
#include "GL/freeglut_ext.h"

//UTILS
void roundVec3(vec3& v)
{
	v.x = round(v.x);
	v.y = round(v.y);
	v.z = round(v.z);
}
vec3 roundVec3(const vec3& v)
{
	vec3 res;
	res.x = round(v.x);
	res.y = round(v.y);
	res.z = round(v.z);
	return res;
}


//INIT
HungerianCubeMeshModel::~HungerianCubeMeshModel()
{
}
void HungerianCubeMeshModel::insretSticker(vec3 pos, vec3 normal, vec3 up, StickerSide side)
{
	Sticker c;
	c.m_pos = pos;
	c.m_normal = normal;
	c.m_up = up;
	c.m_origSide = side;
	m_cubeData.m_Stickers.push_back(c);
}
void HungerianCubeMeshModel::initStickers()
{
	int k = 1;
	assert(k % 2 == 1);

	//create right face:
	for (int y = -k; y <= k; y++)
	{
		for (int z = -k; z <= k; z++)
		{
			insretSticker(vec3(1, y, z), vec3(1, 0, 0), vec3(0, 1, 0), RIGHT_SIDE);
		}
	}

	//create left face:
	for (int y = -k; y <= k; y++)
	{
		for (int z = -k; z <= k; z++)
		{
			insretSticker(vec3(-1, y, z), vec3(-1, 0, 0), vec3(0, -1, 0), LEFT_SIDE);
		}
	}

	//create up face:
	for (int x = -k; x <= k; x++)
	{
		for (int z = -k; z <= k; z++)
		{
			insretSticker(vec3(x, 1, z), vec3(0, 1, 0), vec3(0, 0, 1), UP_SIDE);
		}
	}

	//create down face:
	for (int x = -k; x <= k; x++)
	{
		for (int z = -k; z <= k; z++)
		{
			insretSticker(vec3(x, -1, z), vec3(0, -1, 0), vec3(0, 0, -1), DOWN_SIDE);
		}
	}

	//create front face:
	for (int x = -k; x <= k; x++)
	{
		for (int y = -k; y <= k; y++)
		{
			insretSticker(vec3(x, y, 1), vec3(0, 0, 1), vec3(0, -1, 0), FRONT_SIDE);
		}
	}

	//create back face:
	for (int x = -k; x <= k; x++)
	{
		for (int y = -k; y <= k; y++)
		{
			insretSticker(vec3(x, y, -1), vec3(0, 0, -1), vec3(0, 1, 0), BACK_SIDE);
		}
	}
}
void HungerianCubeMeshModel::initEdgesAndCornersIdexes()
{
	vector<bool> visited(m_cubeData.m_Stickers.size(), false);
	vector<vector<int>> groups;
	for (int i = 0; i < m_cubeData.m_Stickers.size(); i++)
	{
		if (visited[i])
		{
			continue;
		}

		visited[i] = true;

		//create group for i
		groups.resize(groups.size() + 1);
		
		for (int j = 0; j < m_cubeData.m_Stickers.size(); j++)
		{
			if (length(m_cubeData.m_Stickers[i].m_pos - m_cubeData.m_Stickers[j].m_pos) < 1) 
			{
				groups.back().push_back(j);
				visited[j] = true;
			}
		}
	}

	for (int i = 0; i < groups.size(); i++)
	{
		if (groups[i].size() == 2)
		{
			m_cubeData.m_EdgesIdxes.push_back(EdgeCubie(groups[i][0], groups[i][1]));
		}
		else if (groups[i].size() == 3)
		{
			m_cubeData.m_CornersIdexes.push_back(CornerCubie(groups[i][0], groups[i][1], groups[i][2]));
		}
		else
		{
			assert(groups[i].size() == 1);
		}
	}
}
void HungerianCubeMeshModel::initAllowedActions()
{
	for (int i = 0; i < NUM_OF_ACTIONS; i++)
	{
		m_allowedActions.push_back((AtomicAction)i);
	}
	
	
	for (int i = 0; i < NUM_OF_ACTIONS; i++)
	{
		Action a({ (AtomicAction)i, (AtomicAction)i });
		m_allowedActions.push_back(a);
	}
	for (int i = 0; i < NUM_OF_ACTIONS; i++)
	{
		Action a({ (AtomicAction)i, (AtomicAction)i, (AtomicAction)i });
		m_allowedActions.push_back(a);
	}
	
}
HungerianCubeMeshModel::HungerianCubeMeshModel() 
{
	m_isAnimating = false;
	m_Meterials.clear();
	m_Meterials.push_back(CMetirial(CRGBColor::RED(), CRGBColor::WHITE(), CRGBColor::RED(), CRGBColor::BLACK(), 1.0f));
	m_Meterials.push_back(CMetirial(CRGBColor::GREEN(), CRGBColor::WHITE(), CRGBColor::GREEN(), CRGBColor::BLACK(), 1.0f));
	m_Meterials.push_back(CMetirial(CRGBColor::BLUE(), CRGBColor::WHITE(), CRGBColor::BLUE(), CRGBColor::BLACK(), 1.0f));
	m_Meterials.push_back(CMetirial(CRGBColor::YELLOW(), CRGBColor::WHITE(), CRGBColor::YELLOW(), CRGBColor::BLACK(), 1.0f));
	m_Meterials.push_back(CMetirial(CRGBColor::ORANGE(), CRGBColor::WHITE(), CRGBColor::ORANGE(), CRGBColor::BLACK(), 1.0f));
	m_Meterials.push_back(CMetirial(CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::BLACK(), 1.0f));
	m_Meterials.push_back(CMetirial(CRGBColor::PINK(), CRGBColor::WHITE(), CRGBColor::PINK(), CRGBColor::BLACK(), 1.0f));

	initStickers();
	initEdgesAndCornersIdexes();
	initAllowedActions();

	int b4 = getScore();
	mixCube(1000);
	int after = getScore();
}



//ROTATION
void HungerianCubeMeshModel::HungerianCubeData::rotateRightFace(float theta)
{
	mat4 Rx = RotateX(theta);
	for (int i = 0; i < m_Stickers.size(); i++)
	{
		vec3& StickerVertex = m_Stickers[i].m_pos;
		vec3& StickerNormal = m_Stickers[i].m_normal;
		if (StickerVertex.x == 1)
		{
			StickerVertex = vec3FromVec4(Rx*vec4(StickerVertex, 1));
			m_Stickers[i].m_normal = vec3FromVec4(Rx*vec4(m_Stickers[i].m_normal, 1));
			m_Stickers[i].m_up = vec3FromVec4(Rx*vec4(m_Stickers[i].m_up, 1));
		}
	}
}
void HungerianCubeMeshModel::HungerianCubeData::rotateLeftFace(float theta)
{
	mat4 Rx = RotateX(theta);
	for (int i = 0; i < m_Stickers.size(); i++)
	{
		vec3& StickerVertex = m_Stickers[i].m_pos;
		vec3& StickerNormal = m_Stickers[i].m_normal;
		if (StickerVertex.x == -1)
		{
			StickerVertex = vec3FromVec4(Rx*vec4(StickerVertex, 1));
			StickerNormal = vec3FromVec4(Rx*vec4(StickerNormal, 1));
			m_Stickers[i].m_up = vec3FromVec4(Rx*vec4(m_Stickers[i].m_up, 1));
		}

	}
}
void HungerianCubeMeshModel::HungerianCubeData::rotateUpFace(float theta)
{
	mat4 Ry = RotateY(theta);
	for (int i = 0; i < m_Stickers.size(); i++)
	{
		vec3& StickerVertex = m_Stickers[i].m_pos;
		vec3& StickerNormal = m_Stickers[i].m_normal;
		if (StickerVertex.y == 1)
		{
			StickerVertex = vec3FromVec4(Ry*vec4(StickerVertex, 1));
			StickerNormal = vec3FromVec4(Ry*vec4(StickerNormal, 1));
			m_Stickers[i].m_up = vec3FromVec4(Ry*vec4(m_Stickers[i].m_up, 1));
		}
	}
}
void HungerianCubeMeshModel::HungerianCubeData::rotateDownFace(float theta)
{
	mat4 Ry = RotateY(theta);
	for (int i = 0; i < m_Stickers.size(); i++)
	{
		vec3& StickerVertex = m_Stickers[i].m_pos;
		vec3& StickerNormal = m_Stickers[i].m_normal;
		if (StickerVertex.y == -1)
		{
			StickerVertex = vec3FromVec4(Ry*vec4(StickerVertex, 1));
			StickerNormal = vec3FromVec4(Ry*vec4(StickerNormal, 1));
			m_Stickers[i].m_up = vec3FromVec4(Ry*vec4(m_Stickers[i].m_up, 1));
		}
	}
}
void HungerianCubeMeshModel::HungerianCubeData::rotateFrontFace(float theta)
{
	mat4 Rz = RotateZ(theta);
	for (int i = 0; i < m_Stickers.size(); i++)
	{
		vec3& StickerVertex = m_Stickers[i].m_pos;
		vec3& StickerNormal = m_Stickers[i].m_normal;
		if (StickerVertex.z == 1)
		{
			StickerVertex = vec3FromVec4(Rz*vec4(StickerVertex, 1));
			StickerNormal = vec3FromVec4(Rz*vec4(StickerNormal, 1));
			m_Stickers[i].m_up = vec3FromVec4(Rz*vec4(m_Stickers[i].m_up, 1));
		}
	}
}
void HungerianCubeMeshModel::HungerianCubeData::rotateBackFace(float theta)
{
	mat4 Rz = RotateZ(theta);
	for (int i = 0; i < m_Stickers.size(); i++)
	{
		vec3& StickerVertex = m_Stickers[i].m_pos;
		vec3& StickerNormal = m_Stickers[i].m_normal;
		if (StickerVertex.z == -1)
		{
			StickerVertex = vec3FromVec4(Rz*vec4(StickerVertex, 1));
			StickerNormal = vec3FromVec4(Rz*vec4(StickerNormal, 1));
			m_Stickers[i].m_up = vec3FromVec4(Rz*vec4(m_Stickers[i].m_up, 1));
		}
	}
}
void HungerianCubeMeshModel::HungerianCubeData::rotateFace(AtomicAction atomicAction, int degrees)
{
	switch (atomicAction)
	{
	case LEFT_FACE_ROTATE:
		rotateLeftFace(degrees); break;
	case RIGHT_FACE_ROTATE:
		rotateRightFace(degrees); break;
	case UP_FACE_ROTATE:
		rotateUpFace(degrees); break;
	case DOWN_FACE_ROTATE:
		rotateDownFace(degrees); break;
	case FRONT_FACE_ROTATE:
		rotateFrontFace(degrees); break;
	case BACK_FACE_ROTATE:
		rotateBackFace(degrees); break;
	default:
		assert(false);
	}
}
void HungerianCubeMeshModel::HungerianCubeData::applyAction(Action action)
{
	for (int i = 0; i < action.m_atomicActions.size(); i++)
	{
		rotateFace(action.m_atomicActions[i], 90);
		roundAfterRotations();
	}
}
void HungerianCubeMeshModel::HungerianCubeData::applyActionStep(Action action, int degrees)
{
	for (int i = 0; i < action.m_atomicActions.size(); i++)
	{
		rotateFace(action.m_atomicActions[i], degrees);
	}
}
void HungerianCubeMeshModel::HungerianCubeData::roundAfterRotations()
{
	for (int i = 0; i < m_Stickers.size(); i++)
	{
		roundVec3(m_Stickers[i].m_pos);
		roundVec3(m_Stickers[i].m_normal);
		roundVec3(m_Stickers[i].m_up);
	}
}
void HungerianCubeMeshModel::mixCube(int n)
{
	for (int i = 0; i < n; i++)
	{
		//NUM_OF_ACTIONS
		//int r = std::rand() % m_allowedActions.size();
		int r = std::rand() % NUM_OF_ACTIONS;
		m_cubeData.applyAction(m_allowedActions[r]);
	}
}

//ANIMATION
struct AnimationInfo
{
	HungerianCubeMeshModel* m_cube;
	Scene* m_scene;
	vector<HungerianCubeMeshModel::Action> m_actions;
	int m_stepsCounter;
	int m_numOfSteps;
	int m_currAction;
	void animate(int step)
	{
		m_stepsCounter++;
		m_cube->m_cubeData.applyActionStep(m_actions[m_currAction], step);
		if (m_stepsCounter == m_numOfSteps)
		{
			m_cube->m_cubeData.roundAfterRotations();
			if (m_currAction < m_actions.size() - 1) //done with this action, but not the last action
			{
				m_stepsCounter = 0;
				m_currAction++;
			}
		}

		m_scene->draw();
	}
	bool isDone()
	{
		return (m_stepsCounter == m_numOfSteps && m_currAction == m_actions.size() - 1) ||
			m_actions.size() == 0;
	}
	void finish()
	{
		m_cube->m_isAnimating = false;
	}
};
void startHungerianTransormation(int value)
{
	AnimationInfo* info = (AnimationInfo*)value;
	if (info->isDone())
	{
		assert(info->m_currAction == info->m_actions.size() - 1 || info->m_actions.size() == 0);
		info->finish();
		delete info;
		return;
	}

	glutPostRedisplay();
	glutTimerFunc(10, startHungerianTransormation, value);

	info->animate(9);
}
void HungerianCubeMeshModel::rotateFaceAndAnimate(Scene* scene, AtomicAction atomicAction, bool clockwise)
{
	applyActionsAndAnimate(scene, vector<Action>(1, atomicAction));
}
void HungerianCubeMeshModel::applyActionsAndAnimate(Scene* scene, const vector<Action>& actions)
{
	if (m_isAnimating)
	{
		return;
	}
	m_isAnimating = true;
	AnimationInfo* info = new AnimationInfo();
	info->m_cube = this;
	info->m_scene = scene;
	info->m_actions = actions;
	info->m_stepsCounter = 0;
	info->m_numOfSteps = 10;
	info->m_currAction = 0;
	startHungerianTransormation((int)info);

}



//DRAWING
void HungerianCubeMeshModel::pushIntoVector(const vec3& v, std::vector<GLfloat>& vertexes)
{
	vertexes.push_back(v.x);
	vertexes.push_back(v.y);
	vertexes.push_back(v.z);
}
//return a map from poly index to neibour poly indexes 
void HungerianCubeMeshModel::getOpenglData(
	std::vector<GLfloat>& vertexes,
	std::vector<GLfloat>& normals,
	std::vector<GLfloat>& neigbour1Normals,
	std::vector<GLfloat>& neigbour2Normals)
{
	float shiftAmount = 1.0 / 2.0 - 0.01;
	for (int i = 0; i < m_cubeData.m_Stickers.size(); i++)
	{
		vec3 v[4];
		bool bOnceAssert = false;
		vec3 StickerVertex = m_cubeData.m_Stickers[i].m_pos;
		vec3 StickerNormal = m_cubeData.m_Stickers[i].m_normal;
		vec3 u = normalize(m_cubeData.m_Stickers[i].m_up)*shiftAmount;
		vec3 r = normalize(cross(u, StickerNormal))*shiftAmount;
		v[0] = StickerVertex + u + r + StickerNormal*shiftAmount;
		v[1] = StickerVertex + u - r + StickerNormal*shiftAmount;
		v[2] = StickerVertex - u - r + StickerNormal*shiftAmount;
		v[3] = StickerVertex - u + r + StickerNormal*shiftAmount;
	

		pushIntoVector(v[0], vertexes); pushIntoVector(v[1], vertexes); pushIntoVector(v[2], vertexes);
		pushIntoVector(StickerNormal, normals); pushIntoVector(StickerNormal, normals); pushIntoVector(StickerNormal, normals);

		pushIntoVector(v[0], vertexes); pushIntoVector(v[2], vertexes); pushIntoVector(v[3], vertexes);
		pushIntoVector(StickerNormal, normals); pushIntoVector(StickerNormal, normals); pushIntoVector(StickerNormal, normals);
	}
}
void HungerianCubeMeshModel::getMeterialIndexes(std::vector<GLuint>& meterialIdxes)
{
	for (int i = 0; i < m_cubeData.m_Stickers.size(); i++)
	{
		for (int j = 0; j < 6; j++)
		{
			meterialIdxes.push_back(m_cubeData.m_Stickers[i].m_origSide);
		}
	}

	/*
	for (int i = 0; i < m_cubeData.m_CornersIdexes.size(); i++)
	{
		for (int j = 0; j < 6; j++)
		{
			meterialIdxes[m_cubeData.m_CornersIdexes[i][0] * 6 + j] = NUM_OF_ACTIONS;
			meterialIdxes[m_cubeData.m_CornersIdexes[i][1] * 6 + j] = NUM_OF_ACTIONS;
			meterialIdxes[m_cubeData.m_CornersIdexes[i][2] * 6 + j] = NUM_OF_ACTIONS;
		}
		
	}
	*/
}
bool HungerianCubeMeshModel::getOpenglNormalTextureData(
	std::vector<GLfloat>& UVs,
	std::vector<GLfloat>& tangents,
	std::vector<GLfloat>& bitangents)
{
	return false;
}
bool HungerianCubeMeshModel::getOpenglTextureData(std::vector<GLfloat>& UVs)
{
	return false;
}
void HungerianCubeMeshModel::draw(Renderer* renderer)
{
	renderer->unloadModel(this);
	renderer->DrawMeshModel(this);
}
const vector<IPolygon>& HungerianCubeMeshModel::getPolygons() const
{
	assert(false);
	return m_polygons;
}
int HungerianCubeMeshModel::getPolygonsCount() const
{
	return m_cubeData.m_Stickers.size() * 2;
}


//SOLVING
bool HungerianCubeMeshModel::HungerianCubeData::operator<(const HungerianCubeData& other) const
{
	/*
	for (int i = 0; i < m_EdgesIdxes.size(); i += 1)
	{
		vec3Int v = m_Stickers[m_EdgesIdxes[i].m_idxes[0]].m_pos;
		vec3Int vOther = other.m_Stickers[m_EdgesIdxes[i].m_idxes[0]].m_pos;
		int a = ((v.x + 1) << 4 | (v.y + 1) << 2 | (v.z + 1));
		int b = ((vOther.x + 1) << 4 | (vOther.y + 1) << 2 | (vOther.z + 1));
		if (a != b)
		{
			return a < b;
		}
	}

	for (int i = 0; i < m_CornersIdexes.size(); i += 1)
	{
		vec3Int v = m_Stickers[m_CornersIdexes[i].m_idxes[0]].m_pos;
		vec3Int vOther = other.m_Stickers[m_CornersIdexes[i].m_idxes[0]].m_pos;
		int a = ((v.x + 1) << 4 | (v.y + 1) << 2 | (v.z + 1));
		int b = ((vOther.x + 1) << 4 | (vOther.y + 1) << 2 | (vOther.z + 1));
		if (a != b)
		{
			return a < b;
		}
	}
	*/
	
	unsigned int size = m_Stickers.size();
	for (int i = 0; i < size; i += 1)
	{
		vec3Int v = m_Stickers[i].m_pos;
		vec3Int vOther = other.m_Stickers[i].m_pos;
		if (v.x != vOther.x)
		{
			return v.x < vOther.x;
		}
		if (v.y != vOther.y)
		{
			return v.y < vOther.y;
		}
		if (v.z != vOther.z)
		{
			return v.z < vOther.z;
		}
	}
	
	return false;
}
bool HungerianCubeMeshModel::HungerianCubeData::isInPlace(const EdgeCubie& edge) const
{
	return isInPlace(m_Stickers[edge.m_idxes[0]]) && isInPlace(m_Stickers[edge.m_idxes[1]]);
}
bool HungerianCubeMeshModel::HungerianCubeData::isInPlace(const Sticker& Sticker) const
{
	return isInPlace(Sticker.m_normal, Sticker.m_origSide);
}
bool HungerianCubeMeshModel::HungerianCubeData::isInPlace(const vec3Int& normal, StickerSide origSide) const
{
	//cout << "normal = " << normal.x << ", " << normal.y << ", " << normal.z << ", " << endl;
	if (normal.x == 1)
	{
		return origSide == RIGHT_SIDE;
	}
	if (normal.x == -1)
	{
		return origSide == LEFT_SIDE;
	}



	if (normal.y == 1)
	{
		return origSide == UP_SIDE;
	}
	if (normal.y == -1)
	{
		return origSide == DOWN_SIDE;
	}



	if (normal.z == 1)
	{
		return origSide == FRONT_SIDE;
	}
	if (normal.z == -1)
	{
		return origSide == BACK_SIDE;
	}

	assert(false);
	return false;
}
vector<HungerianCubeMeshModel::Action> HungerianCubeMeshModel::getEdgeRoute(const EdgeCubie& edge) const
{
	struct HungerianCubeDataFactory
	{
		list<HungerianCubeData> buffer;
		map<HungerianCubeData, HungerianCubeData*> factory;
		HungerianCubeData* getInstance(const HungerianCubeData& data)
		{
			auto iter = factory.find(data);
			if (iter != factory.end())
			{
				return iter->second;
			}
			buffer.push_back(data);
			factory[data] = &buffer.back();
			return &buffer.back();
		}
	};
	HungerianCubeDataFactory factory;
	vector<Action> route;
	if (m_cubeData.isInPlace(edge))
	{
		return route;
	}
	
	queue<HungerianCubeData*> que;
	set<HungerianCubeData*> visited;

	map<HungerianCubeData*, pair<Action, HungerianCubeData*>> father;
	map<HungerianCubeData*, int> depth;
	HungerianCubeData* target = NULL;
	que.push(factory.getInstance(m_cubeData));
	depth[factory.getInstance(m_cubeData)] = 0;

	while (!que.empty() && !target)
	{
		HungerianCubeData* curr = que.front();
		que.pop();
		if (visited.find(curr) != visited.end())
		{
			continue;
		}
		visited.insert(curr);
		assert(visited.find(curr) != visited.end());
		for (int i = 0; i < m_allowedActions.size(); i++)
		{
			HungerianCubeData copy = *curr;
			copy.applyAction(m_allowedActions[i]);

			HungerianCubeData* next = factory.getInstance(copy);
			if (visited.find(next) != visited.end())
			{
				continue;
			}
			
			father[next] = make_pair(m_allowedActions[i], curr);
			depth[next] = depth[curr] + 1;
			if (next->isInPlace(edge))
			{
				target = next;
				break;
			}
			que.push(next);
		}
	}

	int d = depth[target];
	assert(father.find(target) != father.end() || target == factory.getInstance(m_cubeData));
	while (father.find(target) != father.end())
	{
		route.push_back(father[target].first);
		target = father[target].second;
	}
	
	assert(d == route.size());
	return route;
}


#include<chrono>
int HungerianCubeMeshModel::countBadEdges() const
{
	int sum = 0;
	for (int i = 0; i < m_cubeData.m_EdgesIdxes.size(); i++)
	{
		vector<Action> route = getEdgeRoute(m_cubeData.m_EdgesIdxes[i]);
		cout << "d = " << route.size() << endl;
		int numOfQuarterTurns = 0;
		for (int j = 0; j < route.size(); j++)
		{
			if (route[j].m_atomicActions.size() != 2)
			{
				assert(route[j].m_atomicActions.size() == 1 || route[j].m_atomicActions.size() == 3);
				if (route[j].m_atomicActions[0] == UP_FACE_ROTATE || route[j].m_atomicActions[0] == DOWN_FACE_ROTATE)
				{
					numOfQuarterTurns++;
				}
			}
		}
		if (numOfQuarterTurns % 2 == 1)
		{
			sum++;
		}
	}
	
	return sum;
}
void HungerianCubeMeshModel::solve(Scene* scene)
{

	int badEdges =countBadEdges();
	cout << "badEdges = " << badEdges << endl;

	/*
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	for (int i = 0; i < m_cubeData.m_EdgesIdxes.size(); i++)
	{
		vector<Action> route = getEdgeRoute(m_cubeData.m_EdgesIdxes[i]);
		applyActionsAndAnimate(scene, route);
		cout << "d = " << route.size() << endl;
	}
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	std::cout << "Printing took "
		<< std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
		<< "us.\n";
	*/

}
int HungerianCubeMeshModel::getNumOfPlacedStickers()
{
	int sum = 0;
	for (int i = 0; i < m_cubeData.m_Stickers.size(); i++)
	{
		if (m_cubeData.isInPlace(m_cubeData.m_Stickers[i]))
		{
			sum++;
		}
	}
	return sum;

}
int HungerianCubeMeshModel::getScore() const
{
	int sum = 0;
	for (int i = 0; i < m_cubeData.m_Stickers.size(); i++)
	{
		
		if (!m_cubeData.isInPlace(m_cubeData.m_Stickers[i]))
		{
			sum++;
		}
		/*
		int distance = getStickerDistance(m_cubeData.m_Stickers[i]);
		//assert(10 - distance > 0);
		sum += (distance);
		*/
	}
	return sum;
}
int HungerianCubeMeshModel::getStickerDistance(const Sticker& Sticker) const
{
	vec3Int normal = Sticker.m_normal;
	mat4 Rx = RotateX(90);
	mat4 Ry = RotateY(90);
	mat4 Rz = RotateZ(90);

	std::set<vec3Int> visited;
	list <pair<vec3Int, int>> buffer;
	queue<pair<vec3Int, int>, std::list<pair<vec3Int, int> > > que(buffer);
	que.push(make_pair(normal, 0));

	while (!que.empty())
	{
		vec3Int currNormal = que.front().first;
		int currDistance = que.front().second;
		que.pop();
		if (m_cubeData.isInPlace(currNormal, Sticker.m_origSide))
		{
			return currDistance;
		}

		if (visited.find(currNormal) != visited.end())
		{
			continue;
		}
		visited.insert(currNormal);
		que.push(make_pair(roundVec3(vec3FromVec4(Rx*vec4(currNormal, 1))), currDistance + 1));
		que.push(make_pair(roundVec3(vec3FromVec4(Ry*vec4(currNormal, 1))), currDistance + 1));
		que.push(make_pair(roundVec3(vec3FromVec4(Rz*vec4(currNormal, 1))), currDistance + 1));
	}
	assert(false);
	return -1;
}

