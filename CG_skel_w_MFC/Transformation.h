#pragma once

#include "Scene.h"
class Model;
class Transformation
{
protected:
	
public:
	float m_step;
	Model* m_model;
	typedef enum
	{
		MODEL_FRAME,
		WORLD_FRAME,
		VIEW_FRAME,
		UNDEFINED_FRAME
	} SelectedFrame;

	typedef enum
	{
		TRANSLATION,
		SCALE,
		ROTATE,
		HUNGERIAN_LEFT_FACE,
		HUNGERIAN_RIGHT_FACE,
		HUNGERIAN_UP_FACE,
		HUNGERIAN_DOWN_FACE,
		HUNGERIAN_FRONT_FACE,
		HUNGERIAN_BACK_FACE,
		NOTHING
	} SelectedTransformation;

	SelectedFrame m_selectedFrame;
	SelectedTransformation m_selectedTransformation;
	Transformation();
	Transformation(SelectedTransformation transType, SelectedFrame frame, float step = 1);
	~Transformation();
	virtual void applyTransformation(Scene::Direction dir) const;
	Transformation getInverseTransformation();

};
