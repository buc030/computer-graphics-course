// RotationDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "RotationDialog.h"
#include "afxdialogex.h"
#include "Transformation.h"

// CRotationDialog dialog

IMPLEMENT_DYNAMIC(CRotationDialog, CDialogEx)

CRotationDialog::CRotationDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRotationDialog::IDD, pParent)
{

}

CRotationDialog::~CRotationDialog()
{
}

void CRotationDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CRotationDialog, CDialogEx)
	
	ON_BN_CLICKED(IDC_X_RADIO, &CRotationDialog::OnBnClickedXRadio)
	ON_BN_CLICKED(IDC_Y_RADIO, &CRotationDialog::OnBnClickedYRadio)
	ON_BN_CLICKED(IDC_Z_RADIO, &CRotationDialog::OnBnClickedZRadio)
	ON_BN_CLICKED(IDC_WORLD_FRAME_RADIO, &CRotationDialog::OnBnClickedWorldFrameRadio)
	ON_BN_CLICKED(IDC_MODEL_FRAME_RADIO, &CRotationDialog::OnBnClickedModelFrameRadio)
END_MESSAGE_MAP()


// CRotationDialog message handlers




void CRotationDialog::OnBnClickedXRadio()
{
	m_transformation.m_selectedAxis = Transformation::X;
}


void CRotationDialog::OnBnClickedYRadio()
{
	m_transformation.m_selectedAxis = Transformation::Y;
}


void CRotationDialog::OnBnClickedZRadio()
{
	m_transformation.m_selectedAxis = Transformation::Z;
}


void CRotationDialog::OnBnClickedWorldFrameRadio()
{
	m_transformation.m_selectedFrame = Transformation::WORLD_FRAME;
}


void CRotationDialog::OnBnClickedModelFrameRadio()
{
	m_transformation.m_selectedFrame = Transformation::MODEL_FRAME;
}
