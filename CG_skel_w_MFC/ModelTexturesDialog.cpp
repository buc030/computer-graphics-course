// ModelTexturesDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "ModelTexturesDialog.h"
#include "afxdialogex.h"

#include "Renderer.h"

// CModelTexturesDialog dialog

IMPLEMENT_DYNAMIC(CModelTexturesDialog, CDialogEx)

CModelTexturesDialog::CModelTexturesDialog(MeshModel* model, Renderer* renderer, CWnd* pParent /*=NULL*/)
	: CDialogEx(CModelTexturesDialog::IDD, pParent), m_model(model), m_renderer(renderer)
{

}

CModelTexturesDialog::~CModelTexturesDialog()
{
}

BOOL CModelTexturesDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	refreshLists();
	return TRUE;
}

void CModelTexturesDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MODEL_TEXTURE_LIST, m_texturesListbox);
	DDX_Control(pDX, IDC_TEXTURE_COORDINATE_LIST, m_textureCoordListbox);
}


BEGIN_MESSAGE_MAP(CModelTexturesDialog, CDialogEx)
	ON_BN_CLICKED(IDC_LOAD_NEW_TEXTURE_BUTTON, &CModelTexturesDialog::OnBnClickedLoadNewTextureButton)
	ON_BN_CLICKED(IDC_SET_ACTIVE_TEXTURE_BUTTON, &CModelTexturesDialog::OnBnClickedSetActiveTextureButton)
	ON_BN_CLICKED(IDC_SET_ACTIVE_NORMAL_TEXTURE_BUTTON, &CModelTexturesDialog::OnBnClickedSetActiveNormalTextureButton)
END_MESSAGE_MAP()


// CModelTexturesDialog message handlers

void CModelTexturesDialog::refreshLists()
{
	m_texturesListbox.ResetContent();
	m_textureCoordListbox.ResetContent();
	for(auto iter = m_model->m_textures.begin(); iter != m_model->m_textures.end(); iter++)
	{
		m_texturesListbox.AddString(iter->first.c_str());
	}

	for(auto iter = m_model->m_textureCoords.begin(); iter != m_model->m_textureCoords.end(); iter++)
	{
		m_textureCoordListbox.AddString(iter->first.c_str());
	}

}

void CModelTexturesDialog::OnBnClickedLoadNewTextureButton()
{
	CFileDialog dlg(TRUE,_T(".bmp"),NULL,NULL,_T("*.bmp|*.*"));
	if(dlg.DoModal()==IDOK)
	{
		std::string s((LPCTSTR)dlg.GetPathName());
		if(!m_model->loadTexture(s))
		{
			AfxMessageBox("Texture already exists!", MB_OK | MB_ICONSTOP);
		}
		refreshLists();
	}
}


void CModelTexturesDialog::OnBnClickedSetActiveTextureButton()
{
	
	int selectedTexture = m_texturesListbox.GetCurSel();
	if (selectedTexture == -1)
	{
		AfxMessageBox("Choose texture first!", MB_OK | MB_ICONSTOP);
		return;
	}

	int selectedCoord = m_textureCoordListbox.GetCurSel();
	if (selectedCoord == -1)
	{
		AfxMessageBox("Choose coordinates first!", MB_OK | MB_ICONSTOP);
		return;
	}

	CString s1;
	m_texturesListbox.GetText(selectedTexture, s1);
	CString s2;
	m_textureCoordListbox.GetText(selectedCoord, s2);
	m_model->useTexture(string(s1), string(s2));

	m_renderer->loadModelTextureCoordinates(m_model);

}


void CModelTexturesDialog::OnBnClickedSetActiveNormalTextureButton()
{
	int selectedTexture = m_texturesListbox.GetCurSel();
	if (selectedTexture == -1)
	{
		AfxMessageBox("Choose texture first!", MB_OK | MB_ICONSTOP);
		return;
	}

	int selectedCoord = m_textureCoordListbox.GetCurSel();
	if (selectedCoord == -1)
	{
		AfxMessageBox("Choose coordinates first!", MB_OK | MB_ICONSTOP);
		return;
	}

	CString s1;
	m_texturesListbox.GetText(selectedTexture, s1);
	CString s2;
	m_textureCoordListbox.GetText(selectedCoord, s2);
	m_model->useNormalTexture(string(s1), string(s2));

	m_renderer->loadModelNormalTextureCoordinates(m_model);
}
