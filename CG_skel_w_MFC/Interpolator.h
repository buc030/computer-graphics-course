#pragma once

#include "vec.h"
class Interpolator
{
public:
	Interpolator();
	~Interpolator();

	static double getTforInterpulate(const vec2& p1, const vec2& p2, const vec2& p)
	{
		vec2 v1 = p - p1;
		vec2 v2 = p2 - p1;
		return sqrt((double)dot(v1, v1) / (double)dot(v2, v2));
	}

	static double getTforInterpulate(const vec3& p1, const vec3& p2, const vec3& p)
	{

		vec3 v1 = p - p1;
		vec3 v2 = p2 - p1;
		return sqrt((double)dot(v1, v1) / (double)dot(v2, v2));
	}

	template<class FuncType>
	static FuncType interpulate(const FuncType& f1, const FuncType& f2, double t)
	{
		return f2*t + f1*(1 - t);
	}

	template<class FuncType>
	static FuncType interpulate(const vec2& p1, const FuncType& f1, const vec2& p2, const FuncType& f2, const vec2& p)
	{
		double t = getTforInterpulate(p1, p2, p);
		return f2*t + f1*(1 - t);
	}

	template<class FuncType>
	static FuncType interpulate(const vec3& p1, const FuncType& f1, const vec3& p2, const FuncType& f2, const vec3& p)
	{
		double t = getTforInterpulate(p1, p2, p);
		return f2*t + f1*(1 - t);
	}

};

