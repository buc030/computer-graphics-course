
uniform mat4 projection; //ok
uniform mat4 modelview; //ok

out vec2 renderedTexCoord;
in vec3 vPosition; //model coordinates

void main()
{
	gl_Position = projection*modelview*vec4(vPosition, 1);
	vec4 ndc = gl_Position/gl_Position.w;
	renderedTexCoord = (ndc.xy + vec2(1,1))/2.0;
}





