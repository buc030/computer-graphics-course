#pragma once
#include "scene.h"
#include "vec.h"
#include "mat.h"
#include <string>
#include "Polygon.h"

class Texture;
using namespace std;
class MeshModel : public Model
{
	friend class CModelTexturesDialog;
protected :
	MeshModel();
	
	vector<vec3> m_vertices;
	vector<vec3> m_normals;
	map<string, vector<vec2> > m_textureCoords;
	
	//if the size of m_Meterials is 1, it is uniform meterial,
	//else it is non uniform
	vector<CMetirial> m_Meterials;
	//meterial index for each vertex
	vector<int> m_meterialIndexes;

	vector<IPolygon> m_polygons;

	string m_name;
	MeshModel* m_boundingBox;
	std::map<std::string, Texture*> m_textures;


	std::string m_activeTextureName;
	std::string m_activeTextureCoordName;

	std::string m_activeTextureNormalName;
	std::string m_activeTextureNormalCoordName;


	void computeBoundingBox();
	void loadFile(istream&);
public:
	
	
	bool m_drawBoundingBox;
	bool m_enviormentMapping;
	bool m_silhoutteRendering;
	bool m_toonShading;
	bool m_colorAnimationHueChange;
	bool m_colorAnimationSaturationChange;
	bool m_vertexAnimation;


	
	
	Texture* getActiveNormalTexture();
	Texture* getActiveTexture();
	
	virtual int getPolygonsCount() const
	{
		return m_polygons.size();
	}

	void generatePlanerTextureCoordinates();
	void generateSphereTextureCoordinates();
	bool loadTexture(const string& filename);
	bool useTexture(const string& textname, const string& textCoordName);
	bool useNormalTexture(const string& textname, const string& textCoordName);

	void setMeterials(const vector<CMetirial>& meterials);
	vector<CMetirial>& getMeterials();


	void computeBoundingBox(float& x_low, float& x_high, float& y_low, float& y_high, float& z_low, float& z_high);
	void computeTangentBasis(std::vector<vec3> & tangents, std::vector<vec3> & bitangents);

	virtual bool getOpenglNormalTextureData(
		std::vector<GLfloat>& UVs,
		std::vector<GLfloat>& tangents,
		std::vector<GLfloat>& bitangents);
	virtual bool getOpenglTextureData(std::vector<GLfloat>& UVs);

	//return a map from poly index to neibour poly indexes 
	virtual void getOpenglData(
		std::vector<GLfloat>& vertexes,
		std::vector<GLfloat>& normals,
		std::vector<GLfloat>& neigbour1Normals,
		std::vector<GLfloat>& neigbour2Normals);
	virtual void getMeterialIndexes(std::vector<GLuint>& meterialIdxes);

	MeshModel(string fileName);
	~MeshModel(void);
	void loadFile(string fileName);
	void draw(Renderer* renderer);



	virtual string print() const;
	string printCreationCode() const;

	vec3 getCenterOfMassInWorldCoordinates() const;
	MeshModel* getBoundingBox() const
	{
		return m_boundingBox;
	}

	virtual const vector<IPolygon>& getPolygons() const;
};
