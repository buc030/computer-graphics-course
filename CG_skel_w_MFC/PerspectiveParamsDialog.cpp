// PerspectiveParamsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "PerspectiveParamsDialog.h"
#include "afxdialogex.h"


// CPerspectiveParamsDialog dialog

IMPLEMENT_DYNAMIC(CPerspectiveParamsDialog, CDialogEx)

CPerspectiveParamsDialog::CPerspectiveParamsDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPerspectiveParamsDialog::IDD, pParent)
{

}

CPerspectiveParamsDialog::~CPerspectiveParamsDialog()
{
}

void CPerspectiveParamsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PER_PARAM_ASPECT_EDIT, m_aspect);
	DDX_Text(pDX, IDC_PER_PARAM_FOVY_EDIT, m_fovy);
	DDX_Text(pDX, IDC_PER_PARAM_Z_far_EDIT, m_zFar);
	DDX_Text(pDX, IDC_PER_PARAM_Z_NEAR_EDIT, m_zNear);
}


BEGIN_MESSAGE_MAP(CPerspectiveParamsDialog, CDialogEx)

	ON_BN_CLICKED(IDOK, &CPerspectiveParamsDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// CPerspectiveParamsDialog message handlers






void CPerspectiveParamsDialog::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}
