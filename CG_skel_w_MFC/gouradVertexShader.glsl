uniform int normalMapping;

uniform mat4 modelview; //ok
uniform mat4 normalsModelview; //ok
uniform mat4 projection; //ok
uniform int time;
uniform int vertexAnimation;

in vec3 vPosition; //model coordinates
in vec3 normal;
in vec3 neig1Normal; //the triangle normal
in vec3 vT;
in vec3 vB;
in vec2 vNormalTextCoord;


//for normal mapping (this is actually phong)
out vec3 L[MAX_LIGHTS];
out vec3 E;
out vec2 texCoord;
out Meterial meterial;
out vec3 vertexColor;

float rand1(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(12.9898,78.233, 309.2169))) * 43758.5453);
}
float rand2(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(5645.5647,124.8789, 2654.6945))) * 183.9832);
}
float rand3(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(477.89, 25.68 ,6876.854))) * 54235.6467);
}

void main()
{
	vec3 animatedPosition = vPosition;
	if(vertexAnimation == 1)
	{
		//animation step is between -1 and 1
		float animStep = sin(rand1(vPosition.xyz)*time/10.0)*rand1(vPosition.xyz);
		animatedPosition += animStep*vec3(0,0.1,0);
	}
	vec3 transformedNormal = (normalsModelview*vec4(normal, 0)).xyz;
	vec4 v = modelview*vec4(animatedPosition, 1);
	gl_Position = projection*v;
	texCoord = vNormalTextCoord;

	meterial = allMeterials[gl_VertexID%(min(MAX_METERIALS, numMeterials))];
	E = ( -v.xyz);
	for(int i = 0; i < MAX_LIGHTS && i < numLights; i++)
	{
		if(lights[i].position.w == 0.0)
		{
			L[i] = lights[i].position.xyz;
		}
		else
		{
			L[i] = lights[i].position.xyz - v.xyz;
		}
	}

	if(normalMapping == 1)
	{
		mat3 TBN = transpose(
			mat3(
				((normalsModelview*vec4(vT, 0)).xyz),
				((normalsModelview*vec4(vB, 0)).xyz), 
				((normalsModelview*vec4(neig1Normal, 0)).xyz)));

		E = TBN * E;
		for(int i = 0; i < MAX_LIGHTS && i < numLights; i++)
		{
			L[i] = TBN * L[i];
		}
	}
	else
	{
		vertexColor = calculateColor(meterial, transformedNormal, L, E);
	}	
}
