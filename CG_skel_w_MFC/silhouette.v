

uniform mat4 modelview; //ok
uniform mat4 normalsModelview; //ok
uniform mat4 projection; //ok

in  vec3 vPosition; //model coordinates
in vec3 normal; //model coordinates
in vec3 neig1Normal;
in vec3 neig2Normal;

out vec3 E;
out vec3 gNeig1Normal;
out vec3 gNeig2Normal;
void main()
{
	vec4 v = modelview*vec4(vPosition, 1);
	E = -v.xyz;
	gNeig1Normal = normalize((normalsModelview*vec4(neig1Normal, 0)).xyz);
	gNeig2Normal = normalize((normalsModelview*vec4(neig2Normal, 0)).xyz);

	gl_Position = projection*v;
}
