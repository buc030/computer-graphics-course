#pragma once

#include "gl/glew.h"
#include <vector>
#include <string>
#include "Renderer.h"
#include "Light.h"
#include "Model.h"

using namespace std;

class Transformation;
class Camera;

class Scene {
	

public:
	Renderer *m_renderer;
	CRGBColor m_ambient;
	vector<ILight*> m_lights;
	vector<Camera*> cameras;
	vector<MeshModel*> models;
	typedef enum
	{
		UP_DIR,
		DOWN_DIR,
		LEFT_DIR,
		RIGHT_DIR,
		IN_DIR,
		OUT_DIR
	} Direction;

	typedef enum
	{
		CUBE,
		HUNGERIAN_CUBE
	} ModelPrimitiveType;
	Scene(Renderer *renderer) : m_renderer(renderer), m_activeModel(0), m_activeCamera(0), m_drawCameras(false), m_ambient(0.0f, 0.0f, 0.0f)
	{
		addCamera();
		toggleActiveCameraProjection();
		toggleActiveCameraProjection();
	}
	void loadOBJModel(string fileName);
	void loadPrimitiveModel(ModelPrimitiveType type);
	void draw();
	void drawDemo();

	void toggleActiveCameraProjection();

	void setActiveModelBoundingBox(bool drawBoundingBox);

	void applyTransformationOmActiveModel(Transformation tran, Direction dir);
	void applyTransformationOmActiveCamera(Transformation tran, Direction dir);

	void resetActiveModelTransformations();

	void translateActiveModel(Direction d, GLfloat amount);
	void rotateXActiveModel(GLfloat theta);
	void rotateYActiveModel(GLfloat theta);
	void scaleActiveModel(GLfloat x, GLfloat y, GLfloat z);
	void zoomActiveCamera(Direction dir);

	//add a camera
	int addCamera(vec3 xyz = vec3(0,0,0));
	vector<Camera*> getAllCameras()
	{
		return cameras;
	}

	void setCameras(const vector<Camera*>& cams)
	{
		//we do not allow removing camera's
		assert(cameras.size() <= cams.size());
		cameras = cams;
	}

	void addLight(vec4 position, CRGBColor diffuse, CRGBColor specular, CRGBColor ambient)
	{
		m_lights.push_back(new ILight(position, diffuse, specular, ambient));
		m_activeLight = m_lights.size() - 1;
	}
	bool m_drawCameras;
	int m_activeModel;
	int m_activeLight;
	int m_activeCamera;
	
};