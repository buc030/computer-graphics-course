#pragma once

#include "RGBColor.h"
class CMetirial
{
public:
	CRGBColor m_diffuse;
	CRGBColor m_specular;
	CRGBColor m_emissive;
	CRGBColor m_ambient;
	float m_shinines;
	CMetirial(CRGBColor diffuse, CRGBColor specular, CRGBColor ambient, CRGBColor emissive, float shinines);
	CMetirial();
	~CMetirial();

	CMetirial operator / (float i) const;
	CMetirial operator *(float i) const;
	CMetirial operator +(const CMetirial& other) const;

	static CMetirial random();
	std::string print() const;
};

