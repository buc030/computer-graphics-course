#pragma once

#include "gl/glew.h"
#include "Renderer.h"
#include "Metirial.h"

class Model {
protected:
	mat4 m_world_transform;
	mat4 m_world_transform_inverse;
	mat4 m_model_transform;
	mat4 m_model_transform_inverse;

public:
	//set default (black) uniform meterial
	Model() { }
	void virtual draw(Renderer* renderer) = 0;

	virtual void translate(GLfloat x, GLfloat y, GLfloat z, bool aroundWorld = false);
	virtual void rotateX(GLfloat thetha, bool aroundWorld = false);
	virtual void rotateY(GLfloat thetha, bool aroundWorld = false);
	virtual void rotateZ(GLfloat thetha, bool aroundWorld = false);
	virtual void scale(GLfloat x, GLfloat y, GLfloat z, bool aroundWorld = false);
	virtual void resetTransformations();
	virtual vec4 getLocationInWorldCoordinates() const;

	mat4 getModelMatrix() const
	{
		return m_world_transform*m_model_transform;
	}

	mat4 getModelInverseMatrix() const
	{
		return m_model_transform_inverse*m_world_transform_inverse;
	}

	
	virtual string print() const = 0;
	virtual ~Model() {}

};