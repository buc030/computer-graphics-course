#pragma once
#include "afxwin.h"
#include "MeshModel.h"
class Renderer;
// CModelTexturesDialog dialog

class CModelTexturesDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CModelTexturesDialog)
	virtual BOOL OnInitDialog();
	void refreshLists();
	MeshModel* m_model;
	Renderer* m_renderer;
public:
	CModelTexturesDialog(MeshModel* model, Renderer* renderer, CWnd* pParent = NULL);   // standard constructor
	virtual ~CModelTexturesDialog();

// Dialog Data
	enum { IDD = IDD_MODEL_TEXTURES_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_texturesListbox;
	CListBox m_textureCoordListbox;
	afx_msg void OnBnClickedLoadNewTextureButton();
	afx_msg void OnBnClickedSetActiveTextureButton();
	afx_msg void OnBnClickedSetActiveNormalTextureButton();
};
