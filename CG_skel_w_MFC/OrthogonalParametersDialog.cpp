// OrthogonalParametersDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "OrthogonalParametersDialog.h"
#include "afxdialogex.h"


// COrthogonalParametersDialog dialog

IMPLEMENT_DYNAMIC(COrthogonalParametersDialog, CDialogEx)

COrthogonalParametersDialog::COrthogonalParametersDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(COrthogonalParametersDialog::IDD, pParent)
{

}

COrthogonalParametersDialog::~COrthogonalParametersDialog()
{
}

void COrthogonalParametersDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ORTH_PARAM_BOTTOM_EDIT, m_bottom);
	DDX_Text(pDX, IDC_ORTH_PARAM_LEFT_EDIT, m_left);
	DDX_Text(pDX, IDC_ORTH_PARAM_RIGHT_EDIT, m_right);
	DDX_Text(pDX, IDC_ORTH_PARAM_TOP_EDIT, m_top);
	DDX_Text(pDX, IDC_ORTH_PARAM_Z_FAR_EDIT, m_zFar);
	DDX_Text(pDX, IDC_ORTH_PARAM_Z_NEAR_EDIT, m_zNear);
}


BEGIN_MESSAGE_MAP(COrthogonalParametersDialog, CDialogEx)
	ON_BN_CLICKED(IDOK, &COrthogonalParametersDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// COrthogonalParametersDialog message handlers


void COrthogonalParametersDialog::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}
