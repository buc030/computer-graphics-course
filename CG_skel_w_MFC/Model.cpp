#include "StdAfx.h"
#include "Model.h"


void Model::translate(GLfloat x, GLfloat y, GLfloat z, bool aroundWorld)
{
	vec3 origin = vec3FromVec4(m_world_transform*m_model_transform*vec4(0, 0, 0, 1));
	cout << "model origin (in world) before translate = " << origin << endl;
	if (!aroundWorld)
	{
		vec4 xyz = m_world_transform*m_model_transform*vec4(x, y, z, 1);
		vec4 t = xyz - origin;
		m_world_transform = m_world_transform*Translate(t.x, t.y, t.z);
		m_world_transform_inverse = Translate(-t.x, -t.y, -t.z)*m_world_transform_inverse;
	}
	else
	{
		m_world_transform = Translate(x, y, z)*m_world_transform;
		m_world_transform_inverse = m_world_transform_inverse*Translate(-x, -y, -z);
	}
	origin = vec3FromVec4(m_world_transform*m_model_transform*vec4(0, 0, 0, 1));
	cout << "model origin (in world) after translate = " << origin << endl;
}

void Model::rotateX(GLfloat thetha, bool aroundWorld)
{
	if (!aroundWorld)
	{
		m_model_transform_inverse = RotateX(-thetha)*m_model_transform_inverse;
		m_model_transform = m_model_transform*RotateX(thetha);
	}
	else
	{
		m_world_transform_inverse = m_world_transform_inverse*RotateX(-thetha);
		m_world_transform = RotateX(thetha)*m_world_transform;
	}

}

void Model::rotateY(GLfloat thetha, bool aroundWorld)
{
	if (!aroundWorld)
	{
		m_model_transform_inverse = RotateY(-thetha)*m_model_transform_inverse;
		m_model_transform = m_model_transform*RotateY(thetha);
	}
	else
	{
		m_world_transform_inverse = m_world_transform_inverse*RotateY(-thetha);
		m_world_transform = RotateY(thetha)*m_world_transform;
	}
}

void Model::rotateZ(GLfloat thetha, bool aroundWorld)
{
	if (!aroundWorld)
	{
		m_model_transform_inverse = RotateZ(-thetha)*m_model_transform_inverse;
		m_model_transform = m_model_transform*RotateZ(thetha);
	}
	else
	{
		m_world_transform_inverse = m_world_transform_inverse*RotateZ(-thetha);
		m_world_transform = RotateZ(thetha)*m_world_transform;
	}

}

void Model::scale(GLfloat x, GLfloat y, GLfloat z, bool aroundWorld)
{
	if (!aroundWorld)
	{
		m_model_transform_inverse = Scale(1 / x, 1 / y, 1 / z)*m_model_transform_inverse;
		m_model_transform = m_model_transform*Scale(x, y, z);
	}
	else
	{
		m_world_transform_inverse = m_world_transform_inverse*Scale(1 / x, 1 / y, 1 / z);
		m_world_transform = Scale(x, y, z)*m_world_transform;
	}

}

void Model::resetTransformations()
{
	m_world_transform = mat4();
	m_model_transform = mat4();
	m_world_transform_inverse = mat4();
	m_model_transform_inverse = mat4();
}


vec4 Model::getLocationInWorldCoordinates() const
{
	return m_world_transform*m_model_transform*vec4(0, 0, 0, 1);
}

