#pragma once


// COrthogonalParametersDialog dialog

class COrthogonalParametersDialog : public CDialogEx
{
	DECLARE_DYNAMIC(COrthogonalParametersDialog)

public:
	COrthogonalParametersDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~COrthogonalParametersDialog();
	float m_bottom;
	float m_left;
	float m_right;
	float m_top;
	float m_zFar;
	float m_zNear;
// Dialog Data
	enum { IDD = IDD_OTRHOGONAL_PARAMETERS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
