

uniform sampler2D renderedTexture;

in vec2 renderedTexCoord;
out vec4 fColor;


void main() 
{ 
	fColor = texture(renderedTexture, renderedTexCoord);
} 

