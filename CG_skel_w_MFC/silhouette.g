
uniform float thickness = 0.1;
layout(triangles) in;
layout(triangle_strip, max_vertices=36) out;

in vec3 E[];
in vec3 gNeig1Normal[];
in vec3 gNeig2Normal[];


#define EPSILON 0.0
#define Z_EPSILON 0.01

void EmitThickLine(int StartIndex, int EndIndex)
{
	vec4 startV = gl_in[StartIndex].gl_Position/gl_in[StartIndex].gl_Position.w;
	vec4 endV = gl_in[EndIndex].gl_Position/gl_in[EndIndex].gl_Position.w;

	vec2 p0 = startV.xy;
	vec2 p1 = endV.xy;

	vec2 line = p1 - p0;
	vec2 normal = vec2(-line.y, line.x);

	vec2 a = p0 + thickness * normal;
	vec2 b = p0 - thickness * normal;
	vec2 c = p1 + thickness * normal;
	vec2 d = p1 - thickness * normal;

	gl_Position = vec4(p0, startV.z /(1.0+Z_EPSILON), startV.w);
    EmitVertex();

    gl_Position = vec4(a, startV.z /(1.0+Z_EPSILON), startV.w);
    EmitVertex();

	gl_Position = vec4(p1, endV.z /(1.0+Z_EPSILON), endV.w);
    EmitVertex();
	EndPrimitive();




	gl_Position = vec4(a, startV.z /(1.0+Z_EPSILON), startV.w);
    EmitVertex();

	gl_Position = vec4(c, endV.z /(1.0+Z_EPSILON), endV.w);
    EmitVertex();

	gl_Position = vec4(p1, endV.z /(1.0+Z_EPSILON), endV.w);
    EmitVertex();
    EndPrimitive();




	gl_Position = vec4(b, startV.z /(1.0+Z_EPSILON), startV.w);
    EmitVertex();

	gl_Position = vec4(p0, startV.z /(1.0+Z_EPSILON), startV.w);
    EmitVertex();

	gl_Position = vec4(p1, endV.z /(1.0+Z_EPSILON), endV.w);
    EmitVertex();
    EndPrimitive();




	gl_Position = vec4(b, startV.z /(1.0+Z_EPSILON), startV.w);
    EmitVertex();

	gl_Position = vec4(d, endV.z /(1.0+Z_EPSILON), endV.w);
    EmitVertex();

	gl_Position = vec4(p1, endV.z /(1.0+Z_EPSILON), endV.w);
    EmitVertex();
    EndPrimitive();
}


void main()
{
	for(int i = 0; i < 3; i++)
	{
	    if (dot(gNeig1Normal[i], E[i])*dot(gNeig2Normal[i], E[i]) <= EPSILON) 
		{
			EmitThickLine(i, (i + 1)%3);
		}
	}
}
