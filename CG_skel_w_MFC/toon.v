
uniform mat4 projection; //ok
uniform mat4 modelview; //ok

in vec3 normal;
in vec3 vPosition; //model coordinates

out vec2 renderedTexCoord;
out vec3 N;
out vec3 L;

void main()
{
	gl_Position = projection*modelview*vec4(vPosition, 1);
	vec4 ndc = gl_Position/gl_Position.w;
	renderedTexCoord = (ndc.xy + vec2(1,1))/2.0;
}
