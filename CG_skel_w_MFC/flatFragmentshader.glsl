
flat in vec3 vertexColor;

layout(location = 0) out vec4 fColor;

void main() 
{ 
	fColor = vec4(vertexColor, 1);
} 

