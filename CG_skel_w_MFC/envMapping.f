
uniform samplerCube cubeTexture;
uniform sampler2D renderedTexture;

in vec3 reflected;
in vec2 renderedTexCoord;
layout(location = 0) out vec4 fColor;

void main() 
{ 
	vec4 enviormentColor = texture(cubeTexture, normalize(reflected));
	vec4 prevColor = texture(renderedTexture, renderedTexCoord);
	fColor = prevColor + enviormentColor;
} 

