#include "stdafx.h"
#include "Light.h"
#include <sstream>
#include "Metirial.h"
#include "PrimMeshModel.h"
#include "MeshModel.h"
MeshModel* ILight::m_sphere = NULL;

void ILight::initSphere()
{
	if (ILight::m_sphere == NULL)
	{
		ILight::m_sphere = new MeshModel("lightSphere.obj");
		ILight::m_sphere->setMeterials(vector<CMetirial>(1, CMetirial(CRGBColor(0, 0, 0), CRGBColor(0, 0, 0), CRGBColor(0, 0, 0), CRGBColor(1, 1, 0), 1.0f)));
		ILight::m_sphere->scale(0.5, 0.5, 0.5);
	}
}
ILight::ILight(vec4 position, CRGBColor diffuse, CRGBColor specular, CRGBColor ambient, bool isParalel) :
	m_diffuse(diffuse), m_specular(specular), m_ambient(ambient), m_isParalel(isParalel)
{
	setLocationInWorldCoordinates(position);
	initSphere();
}

ILight::ILight() : m_isParalel(false)
{
	initSphere();
}

ILight::~ILight()
{
}

std::string ILight::print() const
{
	std::stringstream ss;
	ss << (!m_isParalel ? "Point light source" : "Paralel light source") << ", Position: " << getLocationInWorldCoordinates();
	return ss.str();
}

void ILight::setLocationInWorldCoordinates(vec4 position)
{
	m_model_transform = mat4();
	m_model_transform_inverse = mat4();
	m_world_transform = mat4();
	m_world_transform_inverse = mat4();
	translate(position.x, position.y, position.z);
}


void ILight::draw(Renderer* renderer)
{
	renderer->DrawLight(this);
}

