#include "stdafx.h"
#include "GL/glew.h"
#include "GL/freeglut.h"
#include "GL/freeglut_ext.h"
#include "InitShader.h"
#include <iostream>
#include <string>

using namespace std;

// Create a NULL-terminated string by reading the provided file
static string readShaderSource(const char* shaderFile, bool &res)
{
    FILE* fp = fopen(shaderFile, "r");

    if ( fp == NULL ) 
	{
		res = false;
		return ""; 
	}

    fseek(fp, 0L, SEEK_END);
    long size = ftell(fp);

    fseek(fp, 0L, SEEK_SET);
    char* buf = new char[size + 1];
    fread(buf, 1, size, fp);

    buf[size] = '\0';
    fclose(fp);

	res = true;
    return string(buf);
}


// Create a GLSL program object from vertex and fragment shader files
GLuint InitShader(const char* vShaderFile, const char* gShaderFile, const char* fShaderFile, const char* includeShaderFile)
{
    struct Shader {
		const char*  filename;
		GLenum       type;
		string      source;
    }  shaders[3] = 
	{
		{ vShaderFile, GL_VERTEX_SHADER, "" },
		{ gShaderFile, GL_GEOMETRY_SHADER, "" },
		{ fShaderFile, GL_FRAGMENT_SHADER, "" }
    };

	string includeSource;
	
	if(includeShaderFile)
	{
		bool noError;
		includeSource = readShaderSource( includeShaderFile, noError);
		if (!noError) 
		{
			std::cerr << "Failed to read " << includeShaderFile << std::endl;

			while (1);
			exit( EXIT_FAILURE );
		}
	}


    GLuint program = glCreateProgram();
    for ( int i = 0; i < 3; ++i ) 
	{
		Shader& s = shaders[i];
		if (s.filename == NULL)
		{
			continue;
		}

		bool noError;
		s.source = readShaderSource(s.filename, noError);
		if (!noError) 
		{
			std::cerr << "Failed to read " << s.filename << std::endl;

			while (1);
			exit( EXIT_FAILURE );
		}
	
		GLuint shader = glCreateShader( s.type );

		if(!includeShaderFile)
		{
			string totalSourceStr = "#version 330\n" + s.source;
			const GLchar* totalCode[] = {(const GLchar*)totalSourceStr.c_str()};
			glShaderSource(shader, 1, totalCode, NULL );
		}
		else
		{
			string includeString = "#version 330\n" + includeSource;
			const GLchar* totalCode[] = {(const GLchar*)includeString.c_str(), (const GLchar*)s.source.c_str()};
			glShaderSource(shader, 2, totalCode, NULL );
		}


		glCompileShader( shader );

		GLint  compiled;
		glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) 
		{
			std::cerr << s.filename << " failed to compile:" << std::endl;
			GLint  logSize;
			glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logSize );
			char* logMsg = new char[logSize];
			glGetShaderInfoLog( shader, logSize, NULL, logMsg );
			std::cerr << logMsg << std::endl;
			delete [] logMsg;

			while (1);
			exit( EXIT_FAILURE );
		}

		glAttachShader( program, shader );
    }

    /* link  and error check */
    glLinkProgram(program);

    GLint  linked;
    glGetProgramiv( program, GL_LINK_STATUS, &linked );
	if ( !linked ) 
	{
		std::cerr << "Shader program failed to link" << std::endl;
		GLint  logSize;
		glGetProgramiv( program, GL_INFO_LOG_LENGTH, &logSize);
		char* logMsg = new char[logSize];
		glGetProgramInfoLog( program, logSize, NULL, logMsg );
		std::cerr << logMsg << std::endl;
		delete [] logMsg;

		while (1);
			exit( EXIT_FAILURE );
    }

    /* use program object */
    glUseProgram(program);

    return program;
}
