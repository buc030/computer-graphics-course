layout(triangles) in;

layout(line_strip, max_vertices=2) out;

uniform float normal_length;
uniform mat4 projection;

out vec2 renderedTexCoord;
out vec3 vertexColor;

void main()
{
  vec3 p0 = gl_in[0].gl_Position.xyz;
  vec3 p1 = gl_in[1].gl_Position.xyz;
  vec3 p2 = gl_in[2].gl_Position.xyz;

  vec3 N = normalize(cross(p2 - p1, p0 - p1));

  vec3 centerOfMass = (p0 + p1 + p2) / 3.0;

  gl_Position = projection*vec4(centerOfMass, 1.0);
  vertexColor = vec3(1, 0, 0);
  renderedTexCoord = (gl_Position.xy + vec2(1,1))/2.0;
  EmitVertex();
  gl_Position = projection * vec4(centerOfMass + N * normal_length, 1.0);
  vertexColor = vec3(1, 0, 0);
  renderedTexCoord = (gl_Position.xy + vec2(1,1))/2.0;
  EmitVertex();
  EndPrimitive();
}