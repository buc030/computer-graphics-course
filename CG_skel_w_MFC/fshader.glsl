
in  vec2 texCoord;
in vec3 vertexColor;

out vec4 fColor;

void main() 
{ 
	fColor = vec4(vertexColor, 1);
 
} 

