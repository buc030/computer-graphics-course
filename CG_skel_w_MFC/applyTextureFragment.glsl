
uniform sampler2D textureToAdd;
uniform sampler2D renderedTexture;

in vec2 texCoord;
in vec2 renderedTexCoord;
layout(location = 0) out vec4 fColor;

void main() 
{ 
	fColor = texture(textureToAdd, texCoord) + texture(renderedTexture, renderedTexCoord);
	//fColor = texture2D(textureToAdd, texCoord);
} 

