// CameraTransformationsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "CameraTransformationsDialog.h"
#include "afxdialogex.h"


// CameraTransformationsDialog dialog

IMPLEMENT_DYNAMIC(CameraTransformationsDialog, CDialogEx)

CameraTransformationsDialog::CameraTransformationsDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CameraTransformationsDialog::IDD, pParent),
	m_selectedFrame(Transformation::UNDEFINED_FRAME)
{

}

CameraTransformationsDialog::~CameraTransformationsDialog()
{
}

void CameraTransformationsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_CAMERA_TRANSFORMATIONS_STEP_SIZE, m_stepSize);
}


BEGIN_MESSAGE_MAP(CameraTransformationsDialog, CDialogEx)
	ON_BN_CLICKED(IDC_CAMERA_TRANSLATE_RADIO, &CameraTransformationsDialog::OnBnClickedCameraTranslateRadio)
	ON_BN_CLICKED(IDC_CAMERA_ROTATE_RADIO, &CameraTransformationsDialog::OnBnClickedCameraRotateRadio)
	ON_BN_CLICKED(IDC_CAMERA_VIEW_FRAME_RADIO, &CameraTransformationsDialog::OnBnClickedCameraViewFrameRadio)
	ON_BN_CLICKED(IDC_CAMERA_WORLD_FRAME_RADIO, &CameraTransformationsDialog::OnBnClickedCameraWorldFrameRadio)
END_MESSAGE_MAP()


// CameraTransformationsDialog message handlers


void CameraTransformationsDialog::OnBnClickedCameraTranslateRadio()
{
	m_selectedTransformation = Transformation::TRANSLATION;
	SetDlgItemText(IDC_CAMERA_TRANSFORMATIONS_STEP_SIZE, "1.0");
}


void CameraTransformationsDialog::OnBnClickedCameraRotateRadio()
{
	m_selectedTransformation = Transformation::ROTATE;
	SetDlgItemText(IDC_CAMERA_TRANSFORMATIONS_STEP_SIZE, "1.0");
}


void CameraTransformationsDialog::OnBnClickedCameraViewFrameRadio()
{
	m_selectedFrame = Transformation::VIEW_FRAME;
}


void CameraTransformationsDialog::OnBnClickedCameraWorldFrameRadio()
{
	m_selectedFrame = Transformation::WORLD_FRAME;
}
