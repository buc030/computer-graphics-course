#include "StdAfx.h"
#include "Camera.h"
#include <assert.h>
#include <sstream>
#include "MeshModel.h"

using namespace std;

MeshModel* Camera::m_coordinatesSystem = NULL;

void Camera::initCoordinatesSystemMeshModel()
{
	if (Camera::m_coordinatesSystem == NULL)
	{
		Camera::m_coordinatesSystem = new MeshModel("coodinateSystem.obj");
		vector<CMetirial> meterials;
		//red
		meterials.push_back(CMetirial(CRGBColor(0, 0, 0), CRGBColor(0, 0, 0), CRGBColor(0, 0, 0), CRGBColor(0.7, 0, 0), 1.0f));

		Camera::m_coordinatesSystem->setMeterials(meterials);
	}
}

Camera::Camera(Renderer* renderer) : m_isPerspective(false)
{
	vec2 screenWidthAndHeghit = renderer->GetScreenWidthAndHeight();
	m_orthParams.m_left = -10;
	m_orthParams.m_right = 10;
	m_orthParams.m_bottom = -10;
	m_orthParams.m_top = 10;
	m_orthParams.m_zNear = 0;
	m_orthParams.m_zFar = 10;
	//(65.0, 1, 1, 1000);
	m_perParams.m_fovy = 65.0;
	m_perParams.m_aspect = 1;
	m_perParams.m_zNear = 1;
	m_perParams.m_zFar = 100;
	Ortho();
	UpdateScreenWidthAndHeight(screenWidthAndHeghit.x, screenWidthAndHeghit.y);
	initCoordinatesSystemMeshModel();
}

void Camera::draw(Renderer* renderer)
{
	renderer->DrawCamera(this);
}
void Camera::scale(GLfloat x, GLfloat y, GLfloat z, bool aroundWorld)
{
	assert(false);
}
void Camera::resetTransformations()
{
	assert(false);
}

string Camera::print() const
{
	stringstream ss;
	if (m_isPerspective)
		ss << "Perspective Camera " << vec3FromVec4(getLocationInWorldCoordinates()) << ": fovy = " <<
		m_perParams.m_fovy << ", aspect = " << m_perParams.m_aspect << ", zNear = "
		<< m_perParams.m_zNear << ", zFar = " << m_perParams.m_zFar;
	else
		ss << "Orthogonal Camera " << vec3FromVec4(getLocationInWorldCoordinates()) << ": left = " << m_orthParams.m_left << ", right = " << m_orthParams.m_right <<
		", top = " << m_orthParams.m_top << ", bottom = " << m_orthParams.m_bottom << ", zNear = " << m_orthParams.m_zNear <<
		", zFar = " << m_orthParams.m_zFar;
	return ss.str();
}


void Camera::UpdateScreenWidthAndHeight(int w, int h)
{
	m_perParams.m_aspect = (float)w / (float)h;
	float currCamOrthWidth = m_orthParams.m_left - m_orthParams.m_right;

	float newCamOrthHeight = currCamOrthWidth * ((float)w / (float)h);
	m_orthParams.m_top = abs(newCamOrthHeight) / 2;
	m_orthParams.m_bottom = -abs(newCamOrthHeight) / 2;
	bool oldIsPer = m_isPerspective;
	Perspective();
	if (!oldIsPer)
	{
		Ortho();
	}
}

void Camera::LookAtModel(Model* model)
{
	LookAt(getLocationInWorldCoordinates(), model->getLocationInWorldCoordinates(), vec4(0, 1, 0, 1));
}
//values are in camera cordinates.
void Camera::Ortho()
{
	float l, r, b, t, n, f;
	m_isPerspective = false;
	l = m_orthParams.m_left;
	r = m_orthParams.m_right;
	b = m_orthParams.m_bottom;
	t = m_orthParams.m_top;
	n = m_orthParams.m_zNear;
	f = m_orthParams.m_zFar;
	projection = mat4(
		vec4(2 / (r - l), 0, 0, -(r + l) / (r - l)),
		vec4(0, 2 / (t - b), 0, -(t + b) / (t - b)),
		vec4(0, 0, -2 / (f - n), -(f + n) / (f - n)),
		vec4(0, 0, 0, 1));
}

mat4 Camera::Perspective()
{
	float fovy, aspect, zNear, zFar;
	fovy = m_perParams.m_fovy;
	aspect = m_perParams.m_aspect;
	zNear = m_perParams.m_zNear;
	zFar = m_perParams.m_zFar;
	m_isPerspective = true;
	GLfloat angle = (M_PI / 180.0) * fovy;
	float top = zNear*tan(angle / 2);
	float bottom = -top;
	float left = bottom * aspect;
	float right = -left;
	Frustum(left, right, bottom, top, zNear, zFar);
	return projection;
}


//values are in camera cordinates.
void Camera::Frustum(const float l, const float r,
	const float b, const float t,
	const float n, const float f)
{
	projection = mat4(
		vec4(2 * n / (r - l), 0, (r + l) / (r - l), 0),
		vec4(0, 2 * n / (t - b), (t + b) / (t - b), 0),
		vec4(0, 0, -(f + n) / (f - n), -2 * f*n / (f - n)),
		vec4(0, 0, -1, 0));
}

mat4 Camera::getCameraTransform() const
{
	return m_model_transform_inverse*m_world_transform_inverse;
}

mat4 Camera::getCameraTransformInverse() const
{
	return m_world_transform*m_model_transform;
}
//eye,at and up are all in world coordinates
void Camera::LookAt(const vec4& eye, const vec4& at, const vec4& up)
{
	//calculate the vectors u,v,w which are the axis of the camera coordinates system:
	//we want at - eye top point to the -z direction (that is where the camera initialy is).
	//w corosound to z
	vec4 w = normalize(eye - at);
	vec4 u = normalize(cross(up, w));
	vec4 v = normalize(cross(w, u));


	w.w = u.w = v.w = 0;
	mat4 R(u, v, w, vec4(0, 0, 0, 1));
	//we need to move world vertexes to camera coordinates
	mat4 T(
		vec4(1, 0, 0, -eye.x),
		vec4(0, 1, 0, -eye.y),
		vec4(0, 0, 1, -eye.z),
		vec4(0, 0, 0, 1));
	mat4 Tinv(
		vec4(1, 0, 0, eye.x),
		vec4(0, 1, 0, eye.y),
		vec4(0, 0, 1, eye.z),
		vec4(0, 0, 0, 1));
	//translate world coordinates to camera coordintes

	//cTransformInverse = m_world_transform*m_model_transform = Tinv*transpose(R)

	m_world_transform = Tinv;
	m_world_transform_inverse = T;
	m_model_transform = transpose(R);
	m_model_transform_inverse = R;

	//cTransform = R*T;
	//cTransformInverse = Tinv*transpose(R);

}

