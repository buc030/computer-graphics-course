#include "stdafx.h"
#include "Polygon.h"
#include <sstream>
#include <map>
#include <vector>

using namespace std;
FaceIdcs::FaceIdcs() : v(0, 4), vn(0, 4), vt(0, 4)
{
	for (int i = 0; i<4; i++)
		vn[i] = vt[i] = 0;
}

FaceIdcs::FaceIdcs(std::istream & aStream) : v(4,0), vn(4,0), vt(4,0)
{
	for (int i = 0; i<4; i++)
		vn[i] = vt[i] = 0;

	char c;
	for (int i = 0; i < 4; i++)
	{
		aStream >> std::ws >> v[i] >> std::ws;
		if (aStream.peek() != '/')
			continue;
		aStream >> c >> std::ws;
		if (aStream.peek() == '/')
		{
			aStream >> c >> std::ws >> vn[i];
			continue;
		}
		else
			aStream >> vt[i];
		if (aStream.peek() != '/')
			continue;
		aStream >> c >> vn[i];
	}
}

IPolygon::~IPolygon()
{
};

IPolygon& IPolygon::operator = (const IPolygon& other)
{

	m_vertexIdxes = other.m_vertexIdxes;
	m_normalIdxes = other.m_normalIdxes;
	m_textureIdxes = other.m_textureIdxes;
	return *this;
}

string printVec(vec4 v1)
{
	stringstream ss;
	ss << "vec3(" << v1.x << "," << v1.y << "," << v1.z << ")";
	//<< m_v1 << m_v2 << m_v3 << m_v4 << "}\n";
	return ss.str();
}
std::string IPolygon::print() const
{
	stringstream ss;
	
	static int j = 0;
	ss << "vector<vec3> normalsTemp" << j << ";\n";
	for (int i = 0; i < 3; i++)
	{
		ss << "normalsTemp" << j << ".push_back(" << printVec(getNormal(i)) << ");\n";
	}

	ss << "IPolygon* a" << j << " = new IPolygon(";
	for (int i = 0; i < 3; i++)
	{
		ss << printVec(getVertex(i)) << (i < 2 ? "," : "");
	}

	
	ss << ", normalsTemp" << j << ");\n";
	ss << "m_polygons.push_back(a" << j << ");\n";

	j++;
	return ss.str();
}

void IPolygon::setVertexes(const std::vector<int>& vertexIdxes)
{
	m_vertexIdxes = vertexIdxes;
}
void IPolygon::setVertexNormals(const std::vector<int>& normalIdxes)
{
	m_normalIdxes = normalIdxes;
}
void IPolygon::setTextureCoordinates(const std::string& texName, const std::vector<int>& textCoordIdexes)
{
	m_textureIdxes[texName] = textCoordIdexes;
}
bool IPolygon::hasTextureCoordinates(const std::string& texName) const
{
	return m_textureIdxes.find(texName) != m_textureIdxes.end();
}
vec4 IPolygon::getVertex(int idx) const
{
	return m_vertexes[m_vertexIdxes[idx] - 1];
}

vec3 IPolygon::getNormal() const
{
	return normalize(cross(getVertex(1) - getVertex(0), getVertex(2) - getVertex(0)));
}

const vec3& IPolygon::getNormal(int idx) const
{
	return m_vertexNormals[m_normalIdxes[idx] - 1];
}

const vec2& IPolygon::getTextureCoord(const std::string& texName, int idx) const
{
	//m_textureIdxes[texName][idx]
	auto iter = m_textureCoordinates.find(texName);
	assert(iter != m_textureCoordinates.end());

	auto iterIdx = m_textureIdxes.find(texName);
	assert(iterIdx != m_textureIdxes.end());

	return (iter->second)[iterIdx->second[idx] - 1];
}

const std::vector<int>& IPolygon::getVertexesIndexes() const
{
	return m_vertexIdxes;
}

//IPolygon(FaceIdcs face, const vector<vec3>& vertices, const vector<vec3>& normals, const vector<vec2>& textureCoords);
IPolygon::IPolygon(const std::vector<vec3>& vertices, const std::vector<vec3>& vertexNormals, const std::map<std::string, std::vector<vec2> >& textureCoordinates) :
	m_vertexes(vertices), m_vertexNormals(vertexNormals), m_textureCoordinates(textureCoordinates)
{

}




