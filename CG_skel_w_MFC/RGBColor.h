#pragma once
#include "vec.h"
class CRGBColor
{
	void roundIfNecessery()
	{
		if (m_red > 1) m_red = 1;
		if (m_green > 1) m_green = 1;
		if (m_blue > 1) m_blue = 1;
	}
public:
	CRGBColor(float red, float green, float blue) : m_red(red), m_green(green), m_blue(blue)
	{
		roundIfNecessery();
	}

	CRGBColor(COLORREF color) : m_red(GetRValue(color) / 256.0), m_green(GetGValue(color) / 256.0), m_blue(GetBValue(color) / 256.0)
	{
		roundIfNecessery();
	}
	CRGBColor() : m_red(), m_green(), m_blue()
	{

	}

	operator COLORREF() const
	{
		return RGB(m_red * 255, m_green * 255, m_blue * 255);
	}
	
	operator vec3() const
	{
		return vec3(m_red, m_green, m_blue);
	}

	CRGBColor operator*(CRGBColor other) const
	{
		return CRGBColor(m_red*other.m_red, m_green*other.m_green, m_blue*other.m_blue);
	}

	CRGBColor operator*(float s) const
	{
		return CRGBColor(m_red*s, m_green*s, m_blue*s);
	}

	CRGBColor operator*(double s) const
	{
		return CRGBColor(m_red*s, m_green*s, m_blue*s);
	}

	CRGBColor CRGBColor::operator / (float i) const
	{
		return *this*(1 / i);
	}
	CRGBColor operator+=(CRGBColor other)
	{
		m_red += other.m_red;
		m_green += other.m_green;
		m_blue += other.m_blue;
		roundIfNecessery();
		return *this;
	}

	CRGBColor operator+(CRGBColor other) const
	{
		return CRGBColor(m_red+other.m_red, m_green+other.m_green, m_blue+other.m_blue);
	}

	float m_red, m_green, m_blue;

	static CRGBColor WHITE()
	{
		return CRGBColor(1, 1, 1);
	}

	static CRGBColor RED()
	{
		return CRGBColor(1, 0, 0);
	}

	static CRGBColor GREEN()
	{
		return CRGBColor(0, 1, 0);
	}

	static CRGBColor BLUE()
	{
		return CRGBColor(0, 0, 1);
	}

	static CRGBColor YELLOW()
	{
		return CRGBColor(1, 1, 0);
	}

	static CRGBColor ORANGE()
	{
		return CRGBColor(1, 0.5, 0);
	}

	static CRGBColor GOLD()
	{
		return CRGBColor(1, 215/255.0, 0);
	}
	static CRGBColor PINK()
	{
		return CRGBColor(1, 105 / 255.0, 180);
	}

	static CRGBColor BLACK()
	{
		return CRGBColor(0, 0, 0);
	}

	static CRGBColor random()
	{
		unsigned int red = rand()%256;
		unsigned int green = rand() % 256;
		unsigned int blue = rand() % 256;
		return RGB(red, green, blue);
	}

	friend std::ostream& operator << (std::ostream& os, const CRGBColor& v) {
		return os << "( " << v.m_red << ", " << v.m_green << ", " << v.m_blue << " )";
	}
};

