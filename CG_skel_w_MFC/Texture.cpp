#include "stdafx.h"
#include "Texture.h"

#include "CImg.h"
using namespace cimg_library;

Texture::Texture(const std::string& filename) 
{
	CImg<unsigned char> image(filename.c_str());
	for (int x = 0; x < image.width(); x++)
	{
		for (int y = 0; y < image.height(); y++)
		{
			m_data.push_back(image(x, y, 0, 0));
			m_data.push_back(image(x, y, 0, 1));
			m_data.push_back(image(x, y, 0, 2));
		}
	}
	m_width = image.width();
	m_height = image.height();
}


Texture::~Texture()
{
}
