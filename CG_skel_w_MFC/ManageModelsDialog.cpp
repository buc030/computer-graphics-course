// ManageModelsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "ManageModelsDialog.h"
#include "afxdialogex.h"
#include "MetirialPropertiesDialog.h"
#include "NonUniformMeterialPropertiesDialog.h"
#include <sstream>
#include "MeshModel.h"
#include "ModelTexturesDialog.h"
// CManageModelsDialog dialog

IMPLEMENT_DYNAMIC(CManageModelsDialog, CDialogEx)

CManageModelsDialog::CManageModelsDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CManageModelsDialog::IDD, pParent)
{

}

CManageModelsDialog::~CManageModelsDialog()
{
}

void CManageModelsDialog::recalculateList()
{
	m_listBox.ResetContent();
	for (int i = 0; i < m_scene->models.size(); i++)
	{
		stringstream ss;
		ss << "#" << i << ":    " << m_scene->models[i]->print();
		m_listBox.AddString(ss.str().c_str());
	}
}
BOOL CManageModelsDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	recalculateList();
	return TRUE;
}

void CManageModelsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOADED_MODELS_LIST, m_listBox);
	DDX_Control(pDX, IDC_ENV_MAPPING_CHECK, m_envMappingCheckbox);
	DDX_Control(pDX, IDC_SILHOUETTE_CHECK, m_silhouetteCheckbox);
	DDX_Control(pDX, IDC_TOON_SHADING_CHECK, m_toonCheckbox);
	DDX_Control(pDX, IDC_COLOR_ANIMATION_HUE_CHANGE_CHECK, m_colorAnimationHueChangeCheckbox);
	DDX_Control(pDX, IDC_COLOR_ANIMATION_SATURATION_CHANGE_CHECK, m_animationChangeSaturationCheckbox);
	DDX_Control(pDX, IDC_VERTEX_ANIMATION_CHECK, m_vertexAnimationCheckbox);
}


BEGIN_MESSAGE_MAP(CManageModelsDialog, CDialogEx)
	ON_BN_CLICKED(IDC_SET_ACTIVE_MODEL_BUTTON, &CManageModelsDialog::OnBnClickedSetActiveModelButton)
	ON_BN_CLICKED(IDC_SET_METERIAL_BUTTON, &CManageModelsDialog::OnBnClickedSetMeterialButton)
	ON_LBN_DBLCLK(IDC_LOADED_MODELS_LIST, &CManageModelsDialog::OnLbnDblclkLoadedModelsList)
	ON_BN_CLICKED(IDC_SET_NON_UNIFORM_METERIAL_BUTTON, &CManageModelsDialog::OnBnClickedSetNonUniformMeterialButton)
	ON_BN_CLICKED(IDC_UN_LOAD_MODEL_BUTTON, &CManageModelsDialog::OnBnClickedUnLoadModelButton)

	ON_BN_CLICKED(IDC_ENV_MAPPING_CHECK, &CManageModelsDialog::OnBnClickedEnvMappingCheck)
	ON_BN_CLICKED(IDC_SILHOUETTE_CHECK, &CManageModelsDialog::OnBnClickedSilhouetteCheck)
	ON_LBN_SELCHANGE(IDC_LOADED_MODELS_LIST, &CManageModelsDialog::OnLbnSelchangeLoadedModelsList)
	ON_BN_CLICKED(IDC_TOON_SHADING_CHECK, &CManageModelsDialog::OnBnClickedToonShadingCheck)
	ON_BN_CLICKED(IDC_COLOR_ANIMATION_HUE_CHANGE_CHECK, &CManageModelsDialog::OnBnClickedColorAnimationHueChangeCheck)
	ON_BN_CLICKED(IDC_VERTEX_ANIMATION_CHECK, &CManageModelsDialog::OnBnClickedVertexAnimationCheck)
	ON_BN_CLICKED(IDC_MODEL_TEXTURES_BUTTON, &CManageModelsDialog::OnBnClickedModelTexturesButton)
	ON_BN_CLICKED(IDC_COLOR_ANIMATION_SATURATION_CHANGE_CHECK, &CManageModelsDialog::OnBnClickedColorAnimationSaturationChangeCheck)
END_MESSAGE_MAP()


// CManageModelsDialog message handlers


void CManageModelsDialog::OnBnClickedSetActiveModelButton()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		return;
	}

	m_scene->m_activeModel = selected;
	m_listBox.SetSel(selected);
}


void CManageModelsDialog::OnBnClickedSetMeterialButton()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		return;
	}

	CMetirialPropertiesDialog dlg;
	vector<CMetirial> m(m_scene->models[selected]->getMeterials());
	m.resize(1);
	dlg.m_ambient.SetColor(m[0].m_ambient);
	dlg.m_diffuse.SetColor(m[0].m_diffuse);
	dlg.m_specular.SetColor(m[0].m_specular);
	dlg.m_emissive.SetColor(m[0].m_emissive);
	dlg.m_shinines = m[0].m_shinines;
	if (dlg.DoModal() == IDOK)
	{
		m[0] = CMetirial(
			CRGBColor(dlg.m_diffuse.GetColor()),
			CRGBColor(dlg.m_specular.GetColor()),
			CRGBColor(dlg.m_ambient.GetColor()),
			CRGBColor(dlg.m_emissive.GetColor()),
			dlg.m_shinines);
		m_scene->models[selected]->setMeterials(m);
	}
	m_listBox.SetSel(selected);
}


void CManageModelsDialog::OnLbnDblclkLoadedModelsList()
{
	//OnBnClickedSetMeterialButton();
}


void CManageModelsDialog::OnBnClickedSetNonUniformMeterialButton()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		return;
	}

	CNonUniformMeterialPropertiesDialog dlg;
	dlg.m_meterials = vector<CMetirial>(m_scene->models[selected]->getMeterials());
	dlg.DoModal();
	m_listBox.SetSel(selected);
	m_scene->models[selected]->setMeterials(dlg.m_meterials);
}


void CManageModelsDialog::OnBnClickedUnLoadModelButton()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		return;
	}

	if (selected == m_scene->m_activeModel)
	{
		m_scene->m_activeModel = -1;
	}
	else if (selected < m_scene->m_activeModel)
	{
		m_scene->m_activeModel--;
	}

	m_scene->models.erase(m_scene->models.begin() + selected);
	recalculateList();
}




void CManageModelsDialog::OnBnClickedEnvMappingCheck()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		m_envMappingCheckbox.SetCheck(1 - m_envMappingCheckbox.GetCheck());
		return;
	}

	
	m_scene->models[selected]->m_enviormentMapping = m_envMappingCheckbox.GetCheck();
	recalculateList();
	m_listBox.SetSel(selected);
}


void CManageModelsDialog::OnBnClickedSilhouetteCheck()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		m_silhouetteCheckbox.SetCheck(1 - m_silhouetteCheckbox.GetCheck());
		return;
	}

	m_listBox.SetSel(selected);
	m_scene->models[selected]->m_silhoutteRendering = m_silhouetteCheckbox.GetCheck();
	recalculateList();
}




void CManageModelsDialog::OnBnClickedToonShadingCheck()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		m_toonCheckbox.SetCheck(1 - m_toonCheckbox.GetCheck());
		return;
	}

	m_listBox.SetSel(selected);
	m_scene->models[selected]->m_toonShading = m_toonCheckbox.GetCheck();
	recalculateList();
}


void CManageModelsDialog::OnBnClickedColorAnimationHueChangeCheck()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		m_colorAnimationHueChangeCheckbox.SetCheck(1 - m_colorAnimationHueChangeCheckbox.GetCheck());
		return;
	}

	m_listBox.SetSel(selected);
	m_scene->models[selected]->m_colorAnimationHueChange = m_colorAnimationHueChangeCheckbox.GetCheck();
	recalculateList();
}

void CManageModelsDialog::OnBnClickedColorAnimationSaturationChangeCheck()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		m_animationChangeSaturationCheckbox.SetCheck(1 - m_animationChangeSaturationCheckbox.GetCheck());
		return;
	}

	m_listBox.SetSel(selected);
	m_scene->models[selected]->m_colorAnimationSaturationChange = m_animationChangeSaturationCheckbox.GetCheck();
}


void CManageModelsDialog::OnBnClickedVertexAnimationCheck()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		m_vertexAnimationCheckbox.SetCheck(1 - m_vertexAnimationCheckbox.GetCheck());
		return;
	}
	m_scene->models[selected]->m_vertexAnimation = m_vertexAnimationCheckbox.GetCheck();
	m_listBox.SetSel(selected);
	recalculateList();
}





void CManageModelsDialog::OnLbnSelchangeLoadedModelsList()
{
	int selected = m_listBox.GetCurSel();
	m_silhouetteCheckbox.SetCheck(m_scene->models[selected]->m_silhoutteRendering);
	m_envMappingCheckbox.SetCheck(m_scene->models[selected]->m_enviormentMapping);

	m_toonCheckbox.SetCheck(m_scene->models[selected]->m_toonShading);
	m_colorAnimationHueChangeCheckbox.SetCheck(m_scene->models[selected]->m_colorAnimationHueChange);
	m_animationChangeSaturationCheckbox.SetCheck(m_scene->models[selected]->m_colorAnimationSaturationChange);//IDC_COLOR_ANIMATION_SATURATION_CHANGE_CHECK
	m_vertexAnimationCheckbox.SetCheck(m_scene->models[selected]->m_vertexAnimation);
}



void CManageModelsDialog::OnBnClickedModelTexturesButton()
{
	int selected = m_listBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose model first!", MB_OK | MB_ICONSTOP);
		m_vertexAnimationCheckbox.SetCheck(1 - m_vertexAnimationCheckbox.GetCheck());
		return;
	}

	CModelTexturesDialog dlg(m_scene->models[selected], m_scene->m_renderer);
	if (dlg.DoModal() == IDOK)
	{
	}

	m_listBox.SetSel(selected);
}


