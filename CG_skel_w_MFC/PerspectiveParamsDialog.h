#pragma once


// CPerspectiveParamsDialog dialog

class CPerspectiveParamsDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CPerspectiveParamsDialog)

public:
	CPerspectiveParamsDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPerspectiveParamsDialog();

	float m_aspect;
	float m_fovy;
	float m_zNear;
	float m_zFar;

// Dialog Data
	enum { IDD = IDD_PERSPECTIVE_PARAMETERS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedOk();
};
