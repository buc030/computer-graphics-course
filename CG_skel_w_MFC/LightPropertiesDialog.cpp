// LightPropertiesDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "LightPropertiesDialog.h"
#include "afxdialogex.h"


// CLightPropertiesDialog dialog

IMPLEMENT_DYNAMIC(CLightPropertiesDialog, CDialogEx)

CLightPropertiesDialog::CLightPropertiesDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLightPropertiesDialog::IDD, pParent), m_xyz(0,0,0,0)
{

}

CLightPropertiesDialog::~CLightPropertiesDialog()
{
}

void CLightPropertiesDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MFCCOLORBUTTON1, m_ambient);
	DDX_Control(pDX, IDC_MFCCOLORBUTTON2, m_diffuse);
	DDX_Control(pDX, IDC_MFCCOLORBUTTON3, m_specular);

	DDX_Text(pDX, IDC_EDIT2, m_xyz.x);
	DDX_Text(pDX, IDC_EDIT3, m_xyz.y);
	DDX_Text(pDX, IDC_EDIT14, m_xyz.z);

	//DDX_Control(pDX, IDC_POINT_SOURCE_CHECK, m_pointSourceCheckbox);
	//DDX_Control(pDX, IDC_PARALLEL_SOURCE_CHECK, m_parallelSourceCheckbox);

	DDX_Check(pDX, IDC_PARALLEL_SOURCE_CHECK, m_isParallel);
	DDX_Control(pDX, IDC_POINT_SOURCE_CHECK, m_pointSourceCheckbox);
	DDX_Control(pDX, IDC_PARALLEL_SOURCE_CHECK, m_parallelSourceCheckbox);
}


BEGIN_MESSAGE_MAP(CLightPropertiesDialog, CDialogEx)

	ON_BN_CLICKED(IDC_POINT_SOURCE_CHECK, &CLightPropertiesDialog::OnBnClickedPointSourceCheck)
	ON_BN_CLICKED(IDC_PARALLEL_SOURCE_CHECK, &CLightPropertiesDialog::OnBnClickedParallelSourceCheck)
END_MESSAGE_MAP()


// CLightPropertiesDialog message handlers



void CLightPropertiesDialog::OnBnClickedPointSourceCheck()
{
	m_parallelSourceCheckbox.SetCheck(0);
}


void CLightPropertiesDialog::OnBnClickedParallelSourceCheck()
{
	m_pointSourceCheckbox.SetCheck(0);
}
