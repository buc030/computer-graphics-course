#pragma once
#include "afxwin.h"
#include <vector>
#include "Metirial.h"
#include "afxcmn.h"

// CNonUniformMeterialPropertiesDialog dialog

class CNonUniformMeterialPropertiesDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CNonUniformMeterialPropertiesDialog)
	virtual BOOL OnInitDialog();
	void reCalcList();
public:
	CNonUniformMeterialPropertiesDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNonUniformMeterialPropertiesDialog();

// Dialog Data
	enum { IDD = IDD_NON_UNIFORM_METERIAL_PROPERTIES_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_meterialsListBox;
	std::vector<CMetirial> m_meterials;
	afx_msg void OnBnClickedAddUniformMeterialToNonUniformMeterialButton();
	afx_msg void OnBnClickedAddRandomMeterialButton();
	afx_msg void OnLbnSelchangeList3();
};
