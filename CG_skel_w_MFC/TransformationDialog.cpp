// TransformationDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "TransformationDialog.h"
#include "afxdialogex.h"


// CTransformationDialog dialog

IMPLEMENT_DYNAMIC(CTransformationDialog, CDialogEx)

CTransformationDialog::CTransformationDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTransformationDialog::IDD, pParent)
{

}

CTransformationDialog::~CTransformationDialog()
{
}

void CTransformationDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TRANSFORMATIONS_STEP_SIZE, m_stepSize);
	
}


BEGIN_MESSAGE_MAP(CTransformationDialog, CDialogEx)
	ON_BN_CLICKED(IDC_TRANSLATE_RADIO, &CTransformationDialog::OnBnClickedTranslateRadio)
	ON_BN_CLICKED(IDC_SCALE_RADIO, &CTransformationDialog::OnBnClickedScaleRadio)
	ON_BN_CLICKED(IDC_ROTATE_RADIO, &CTransformationDialog::OnBnClickedRotateRadio)
	ON_BN_CLICKED(IDC_MODEL_FRAME_RADIO, &CTransformationDialog::OnBnClickedModelFrameRadio)
	ON_BN_CLICKED(IDC_WORLD_FRAME_RADIO, &CTransformationDialog::OnBnClickedWorldFrameRadio)
	ON_BN_CLICKED(IDC_Y_RADIO, &CTransformationDialog::OnBnClickedYRadio)
END_MESSAGE_MAP()


// CTransformationDialog message handlers


void CTransformationDialog::OnBnClickedTranslateRadio()
{
	m_selectedTransformation = Transformation::TRANSLATION;
	m_stepSize = 1.0;
	SetDlgItemText(IDC_TRANSFORMATIONS_STEP_SIZE, "1.0");
}


void CTransformationDialog::OnBnClickedScaleRadio()
{
	m_selectedTransformation = Transformation::SCALE;
	m_stepSize = 1.1;
	SetDlgItemText(IDC_TRANSFORMATIONS_STEP_SIZE, "1.1");
}


void CTransformationDialog::OnBnClickedRotateRadio()
{
	m_selectedTransformation = Transformation::ROTATE;
	m_stepSize = 1.0;
	SetDlgItemText(IDC_TRANSFORMATIONS_STEP_SIZE, "1.0");
}


void CTransformationDialog::OnBnClickedModelFrameRadio()
{
	m_selectedFrame = Transformation::MODEL_FRAME;
}


void CTransformationDialog::OnBnClickedWorldFrameRadio()
{
	m_selectedFrame = Transformation::WORLD_FRAME;
}


void CTransformationDialog::OnBnClickedXRadio()
{
	
}


void CTransformationDialog::OnBnClickedYRadio()
{
	
}


void CTransformationDialog::OnBnClickedZRadio()
{
	
}

