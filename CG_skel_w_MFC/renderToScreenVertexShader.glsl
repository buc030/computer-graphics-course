
uniform mat4 modelview; //ok
uniform mat4 projection; //ok
uniform int time;
uniform int vertexAnimation;

in vec3 vPosition; //model coordinates
in vec2 vTextCoord;
out vec2 texCoord;
out vec2 renderedTexCoord;

float rand1(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(12.9898,78.233, 309.2169))) * 43758.5453);
}
float rand2(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(5645.5647,124.8789, 2654.6945))) * 183.9832);
}
float rand3(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(477.89, 25.68 ,6876.854))) * 54235.6467);
}

void main()
{
	vec3 animatedPosition = vPosition;
	if(vertexAnimation == 1)
	{
		//animatedPosition += (sin(time/10.0))*(rand1(vPosition.xyz)  <= 0.5 ? 1 : -1)*normalize(vec3(rand1(vPosition.xyz),rand2(vPosition.xyz),rand3(vPosition.xyz)));
		//animation step is between -1 and 1
		float animStep = sin(rand1(vPosition.xyz)*time/10.0)*rand1(vPosition.xyz);
		animatedPosition += animStep*vec3(0,0.1,0);
	}

	gl_Position = projection*modelview*vec4(animatedPosition, 1);
	vec4 ndc = gl_Position/gl_Position.w;
	renderedTexCoord = (ndc.xy + vec2(1,1))/2.0;
}
