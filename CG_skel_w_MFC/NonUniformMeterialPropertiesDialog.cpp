// NonUniformMeterialPropertiesDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "NonUniformMeterialPropertiesDialog.h"
#include "MetirialPropertiesDialog.h"
#include "afxdialogex.h"
#include "Metirial.h"
#include <sstream>
// CNonUniformMeterialPropertiesDialog dialog

IMPLEMENT_DYNAMIC(CNonUniformMeterialPropertiesDialog, CDialogEx)

CNonUniformMeterialPropertiesDialog::CNonUniformMeterialPropertiesDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CNonUniformMeterialPropertiesDialog::IDD, pParent)
{

}

CNonUniformMeterialPropertiesDialog::~CNonUniformMeterialPropertiesDialog()
{
}

void CNonUniformMeterialPropertiesDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST3, m_meterialsListBox);
}


BEGIN_MESSAGE_MAP(CNonUniformMeterialPropertiesDialog, CDialogEx)
	ON_BN_CLICKED(IDC_ADD_UNIFORM_METERIAL_TO_NON_UNIFORM_METERIAL_BUTTON, &CNonUniformMeterialPropertiesDialog::OnBnClickedAddUniformMeterialToNonUniformMeterialButton)
	ON_BN_CLICKED(IDC_ADD_RANDOM_METERIAL_BUTTON, &CNonUniformMeterialPropertiesDialog::OnBnClickedAddRandomMeterialButton)
	ON_LBN_SELCHANGE(IDC_LIST3, &CNonUniformMeterialPropertiesDialog::OnLbnSelchangeList3)
END_MESSAGE_MAP()


BOOL CNonUniformMeterialPropertiesDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	reCalcList();
	return true;
}

void CNonUniformMeterialPropertiesDialog::reCalcList()
{
	m_meterialsListBox.ResetContent();


	for (int i = 0; i < m_meterials.size(); i++)
	{
		std::stringstream ss;
		ss << "#" << i << ":    " << m_meterials[i].print();
		m_meterialsListBox.AddString(ss.str().c_str());
	}
}

// CNonUniformMeterialPropertiesDialog message handlers
void CNonUniformMeterialPropertiesDialog::OnBnClickedAddUniformMeterialToNonUniformMeterialButton()
{
	CMetirialPropertiesDialog dlg;
	if (dlg.DoModal() == IDOK)
	{
		m_meterials.push_back(CMetirial(
			CRGBColor(dlg.m_diffuse.GetColor()),
			CRGBColor(dlg.m_specular.GetColor()),
			CRGBColor(dlg.m_ambient.GetColor()),
			CRGBColor(dlg.m_emissive.GetColor()),
			dlg.m_shinines));
		reCalcList();
	}
}


void CNonUniformMeterialPropertiesDialog::OnBnClickedAddRandomMeterialButton()
{
	m_meterials.push_back(CMetirial::random());
	reCalcList();
}


void CNonUniformMeterialPropertiesDialog::OnLbnSelchangeList3()
{
	int selected = m_meterialsListBox.GetCurSel();
	if (selected == -1)
	{
		AfxMessageBox("Choose meterial first!", MB_OK | MB_ICONSTOP);
		return;
	}
	CMetirialPropertiesDialog dlg;
	CMetirial& m = m_meterials[selected];
	dlg.m_ambient.SetColor(m.m_ambient);
	dlg.m_diffuse.SetColor(m.m_diffuse);
	dlg.m_specular.SetColor(m.m_specular);
	dlg.m_emissive.SetColor(m.m_emissive);
	dlg.m_shinines = m.m_shinines;
	if (dlg.DoModal() == IDOK)
	{
		m_meterials[selected]  = CMetirial(
			CRGBColor(dlg.m_diffuse.GetColor()),
			CRGBColor(dlg.m_specular.GetColor()),
			CRGBColor(dlg.m_ambient.GetColor()),
			CRGBColor(dlg.m_emissive.GetColor()),
			dlg.m_shinines);
		reCalcList();
	}
}

