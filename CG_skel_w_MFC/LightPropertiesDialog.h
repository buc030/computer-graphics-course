#pragma once
#include "afxcolorbutton.h"
#include "afxwin.h"
#include "vec.h"

// CLightPropertiesDialog dialog

class CLightPropertiesDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CLightPropertiesDialog)

public:
	CLightPropertiesDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLightPropertiesDialog();

// Dialog Data
	enum { IDD = IDD_LIGHT_PROPERTIES_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CMFCColorButton m_ambient;
	CMFCColorButton m_diffuse;
	CMFCColorButton m_specular;

	vec4 m_xyz;
	afx_msg void OnBnClickedPointSourceCheck();
	afx_msg void OnBnClickedParallelSourceCheck();
	int m_isParallel;
	CButton m_pointSourceCheckbox;
	CButton m_parallelSourceCheckbox;
};
