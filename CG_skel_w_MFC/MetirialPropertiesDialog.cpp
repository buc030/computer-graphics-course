// MetirialPropertiesDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "MetirialPropertiesDialog.h"
#include "afxdialogex.h"


// CMetirialPropertiesDialog dialog

IMPLEMENT_DYNAMIC(CMetirialPropertiesDialog, CDialogEx)

CMetirialPropertiesDialog::CMetirialPropertiesDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMetirialPropertiesDialog::IDD, pParent)
	, m_isUniform(0)
{

}

CMetirialPropertiesDialog::~CMetirialPropertiesDialog()
{
}

void CMetirialPropertiesDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_AMBIENT_MFCCOLORBUTTON, m_ambient);
	DDX_Control(pDX, IDC_DIFFUSE_MFCCOLORBUTTON, m_diffuse);
	DDX_Control(pDX, IDC_SPECULAR_MFCCOLORBUTTON, m_specular);
	DDX_Control(pDX, IDC_EMISSIVE_MFCCOLORBUTTON, m_emissive);
	DDX_Control(pDX, IDC_METERIAL_SHININES_SLIDER, m_shininesSlider);
	DDX_Slider(pDX, IDC_METERIAL_SHININES_SLIDER, m_shinines);
}


BEGIN_MESSAGE_MAP(CMetirialPropertiesDialog, CDialogEx)
END_MESSAGE_MAP()

BOOL CMetirialPropertiesDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	m_shininesSlider.SetRange(1, 500, true);
	m_shininesSlider.SetPos(m_shinines);
	return true;
}

// CMetirialPropertiesDialog message handlers
