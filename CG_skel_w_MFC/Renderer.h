#pragma once
#include <vector>
#include "CG_skel_w_MFC.h"
#include "vec.h"
#include "mat.h"
#include "GL/glew.h"
#include "RGBColor.h"
#include "Metirial.h"
#include <map>
#include <vector>

class IPolygon;
class Scene;
class Fragment;
class MeshModel;
class CLine;
class ILight;
class Camera;
class Texture;

using namespace std;
class Renderer
{
	struct EnvTexture
	{
		EnvTexture()  { }
		//EnvTexture(GLuint textureBuffer, vec3 takenPos) : m_textureBuffer(textureBuffer), m_takenPos(takenPos) {}
		GLuint m_textureBuffer;
		vec3 m_takenPos;
	};
	map<MeshModel*, EnvTexture> m_modelEnvTextureObject;

	int m_width, m_height;
	
	mat4 m_currentProjectioonMatrix;
	mat4 m_currentCameraTransform;
	mat4 m_currentCameraTransformInv;
	mat4 m_modelview;
	mat4 m_normalsModelview;
	Scene* m_scene;
	mat4 m_totalTransform;

	mat4 m_oTransform;
	mat4 m_oTransformInv;

	//this is the model we will draw 
	bool m_isClippingNecesery;
	int m_currMeterialIndex;
	vector<CMetirial> m_currMeterial;
	vector<vec4> m_currLightsPositions;


	void createModelVertexBuffer(MeshModel* model);
	void createTextureBuffer(Texture* texture);

	typedef enum
	{
		RENDER_FLAT_MODEL_PROGRAM = 0,
		RENDER_GOURAD_MODEL_PROGRAM,
		RENDER_PHONG_MODEL_PROGRAM,
		RENDER_NORMAL_PER_VERTEX_PROGRAM,
		RENDER_NORMAL_PER_FACE_PROGRAM,
		RENDER_GRID_PROGRAM,
		RENDER_ADD_TEXTURE_PROGRAM,
		RENDER_ENVIORMENT_MAPPING,
		RENDER_TO_SCREEN_PROGRAM,
		RENDER_SILHOUETTE_RENDERING,
		RENDER_TOON_SHADING,
		RENDER_COLOR_ANIMATION,
		NUM_OF_PROGRAMS
	} ProgramType;

	void setActiveProgram(ProgramType p);
	ProgramType m_activeProgram;
	GLint m_programs[NUM_OF_PROGRAMS];

	typedef enum
	{
		VERTEXES_VBO = 0,
		NORMALS_VBO,
		NEIGBOUR1_VBO,
		NEIGBOUR2_VBO,
		TEXTURE_COORD_VBO,
		NORMAL_TEXTURE_COORD_VBO,
		TANGENT_COORD_VBO,
		BITANGENT_COORD_VBO,
		METERIAL_IDXES_VBO,
		NUM_OF_VBOS
	} VBOType;

	struct VBOInfo
	{
		VBOInfo() : m_availability(NUM_OF_VBOS, false), m_vbos(NUM_OF_VBOS) {}
		vector<GLuint> m_vbos;
		vector<bool> m_availability;
		bool isAvailable(VBOType t) const
		{
			return m_availability[t];
		}

		void setVbo(VBOType t, GLuint vbo)
		{
			m_availability[t] = true;
			m_vbos[t] = vbo;
		}

		GLuint getVbo(VBOType t)
		{
			assert(isAvailable(t));
			return m_vbos[t];
		}
	};

	map<MeshModel*, int> m_meshModelTime;
	map<MeshModel*, GLuint> m_modelToVAO;
	map<MeshModel*, VBOInfo> m_modelToVBOs;
	map<Texture*, GLuint> m_textureToTextureGlObject;
	//////////////////////////////
	// openGL stuff. Don't touch.

	GLuint gScreenTex;
	GLuint gScreenVtc;
	void InitOpenGLRendering();
	//////////////////////////////
public:
	void loadModelTextureCoordinates(MeshModel* model);
	void loadModelNormalTextureCoordinates(MeshModel* model);

	typedef enum
	{
		PHONG_REFLECTION,
		MODIFIED_PHONG_REFLECTION
	} ReflectionModelType;

	typedef enum
	{
		FLAT,
		GOURAUND,
		PHONG,
		NONE
	} ShadingType;

	ReflectionModelType m_reflectionModel;
	ShadingType m_shadingType;
	bool m_drawPerVertexNormals;
	bool m_drawPerFaceNormals;

	Renderer();
	Renderer(int width, int height);
	~Renderer(void);
	vec2 GetScreenWidthAndHeight() const
	{
		return vec2(m_width, m_height);
	}
	void Init();

	void ClearBuffers();
	void SwapBuffers();
	void DrawMeshModel(MeshModel* model);
	void unloadModel(MeshModel* model);

	void DrawGrid(int width = 20, int height = 20);
	void DrawLight(ILight* light);
	void DrawCamera(Camera* light);

	void SetScene(Scene* scene);

	void SetObjectMatrices(const mat4& oTransform, const mat4& oTransformInv);
	void resizeWindow(int width, int height);

	void SetWireframeOnly(bool wireFrameOnly);
	
private:
	typedef enum
	{
		X_PLANE = 0,
		NEG_X_PLANE,
		Y_PLANE,
		NEG_Y_PLANE,
		Z_PLANE,
		NEG_Z_PLANE,
		NUM_OF_PLANES
	} Plane;

	/*
	struct FrameBuffer
	{
		FrameBuffer(GLuint fb, GLuint color_rb, GLuint depth_rb) : m_fb(fb), m_color_rb(color_rb), m_depth_rb(depth_rb) {}
		GLuint m_fb;
		GLuint m_color_rb;
		GLuint m_depth_rb;
	};
	void bindFrameBuffer(FrameBuffer fb);
	*/
	
	vector<GLuint> m_renderingFrameBufferStack;
	MeshModel* m_mappingRederingPassModel;
	void DrawMeshModelMultiPass(MeshModel* model, GLuint colorTex, GLuint depthTex);
	void CalculateEnviormentCubeMap(MeshModel* model);
	void renderEnviorment(std::vector<unsigned char>& data, GLuint color_rb, bool debug);
	void CreateFrameBuffers(GLuint& fb, GLuint& color_rb, GLuint& depth_rb, int width, int height);
	void bindModelVertexBuffer(MeshModel* model);
	void unbindModelVertexBuffer(MeshModel* model);
	void debugPopupCurrentDraw(GLuint fb);

	friend class IPolygon;


	ProgramType getRequestedShadingProgram();
	template<class T>
	void setUniform(string name, T value);
};
