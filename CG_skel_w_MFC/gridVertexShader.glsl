
uniform mat4 cameraTransform;
uniform mat4 projection;

uniform vec2 dim;

const vec3 triOffset[] = vec3[](
    vec3(0,0,0),
    vec3(0,0,1),
    vec3(1,0,1),
    vec3(0,0,0),
    vec3(1,0,1),
    vec3(1,0,0));

out vec3 vertexColor;



void main()
{
    int triVert = gl_VertexID % 6;
    int gridIndex = gl_VertexID / 6;
	int xIdx = gridIndex / int(dim.x);
	int zIdx = gridIndex % int(dim.x);
    vec3 coord = vec3(xIdx, 0, zIdx);
    coord += triOffset[triVert];

	coord -= vec3(dim.x/2, 0, dim.y/2);
    gl_Position = projection*cameraTransform*vec4(coord , 1);

	vertexColor = vec3(1,1,1);

	//if(coord.x == 0 && coord.y == 0 && coord.z == 0)
	if((xIdx % 2 == 1 && zIdx % 2 == 1) || (xIdx % 2 == 0 && zIdx % 2 == 0))
	{
		vertexColor = vec3(0.3,0.3,0.3);
	}
}