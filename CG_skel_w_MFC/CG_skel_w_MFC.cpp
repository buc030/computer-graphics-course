// CG_skel_w_MFC.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

#include "GL/glew.h"
#include "GL/freeglut.h"
#include "GL/freeglut_ext.h"
#include "vec.h"
#include "mat.h"
#include "InitShader.h"
#include "Scene.h"
#include "MeshModel.h"
#include "Renderer.h"
#include "Camera.h"
#include <string>


#define BUFFER_OFFSET( offset )   ((GLvoid*) (offset))



typedef enum 
{
	FILE_OPEN = 0,
	MAIN_ABOUT,
	MAIN_MODEL_TRANSFORMATIONS,
	MAIN_HUNGERIAN_CUBE_TRANSFORMATIONS,
	MAIN_CAMERA_TRANSFORMATIONS,
	MAIN_LIGHT_TRANSFORMATIONS,
	MAIN_RESET_TRANSFORMATIONS,
	MAIN_ADD_CAMERA,
	MAIN_MANAGE_CAMERAS,
	MAIN_MANAGE_MODELS,
	MAIN_PERSPECTIVE_ORTHO,
	MAIN_ADD_CUBE_PRIM,
	MAIN_FOCUSE_TO_ACTIVE_MODEL,
	MAIN_SHOW_CAMERAS,
	MAIN_MANAGE_LIGHT,
	MAIN_SET_SCENE_AMBIENT_LIGHT,
	RENDERING_WIRE_FRAME_ONLY,
	RENDERING_FULL,
	BOUNDING_BOX_ON, 
	BOUNDING_BOX_OFF,
	DRAW_NORMALS_PER_VERTEX,
	DRAW_NORMALS_PER_FACE,
	DRAW_NORMALS_OFF,
	SHADING_FLAT,
	SHADING_GOARAUND,
	SHADING_PHONG,
	REFLECTION_MODEL_PHONG,
	REFLECTION_MODEL_MODIFIED_PHONG,
	TRANSFORM_HUNGERIAN_LEFT_FACE,
	TRANSFORM_HUNGERIAN_RIGHT_FACE,
	TRANSFORM_HUNGERIAN_UP_FACE,
	TRANSFORM_HUNGERIAN_DOWN_FACE,
	TRANSFORM_HUNGERIAN_FRONT_FACE,
	TRANSFORM_HUNGERIAN_BACK_FACE,
	HUNGERIAN_SOLVE
} MenueItem;


#include "InputDialog.h"
#include "TransformationDialog.h"
#include "CameraDialog.h"
#include "ManageModelsDialog.h"
#include "CameraTransformationsDialog.h"
#include "LightsDialog.h"
#include "Transformation.h"
#include "HungerianCubeMeshModel.h"

CToolBar myBar;
Scene *scene;
Renderer *renderer;
CTransformationDialog transformationDlg;
CameraTransformationsDialog cameraTransformationDlg;
CXyzDialog placeCameraDlg;
CCameraDialog manageCamDlg;
CManageModelsDialog manageModelsDialog;
CLightsDialog lightsDlg;
CMFCColorDialog dlgColors;

Transformation modelTransformation = Transformation(Transformation::TRANSLATION, Transformation::MODEL_FRAME);
Transformation cameraTransformation = Transformation(Transformation::TRANSLATION, Transformation::MODEL_FRAME);
Transformation cameraRotateTrans = Transformation(Transformation::ROTATE, Transformation::WORLD_FRAME);
Transformation lightTransformation = Transformation(Transformation::TRANSLATION, Transformation::MODEL_FRAME);
Transformation* activeTransformation = &cameraTransformation;

int last_x,last_y;
bool lb_down,rb_down,mb_down;

//----------------------------------------------------------------------------
// Callbacks

void display( void )
{
	//Call the scene and ask it to draw itself
	renderer->ClearBuffers();
	scene->draw();
	renderer->SwapBuffers();
	
}

void reshape( int width, int height )
{
	renderer->resizeWindow(width, height);
	for (int i = 0; i < scene->cameras.size(); i++)
	{
		scene->cameras[i]->UpdateScreenWidthAndHeight(width, height);
	}

	glutPostRedisplay();
}

void keyboard( unsigned char key, int x, int y )
{
	switch ( key ) {
	case 'Z': 
		activeTransformation->applyTransformation(Scene::IN_DIR);
		break;
	case 'z':
		activeTransformation->applyTransformation(Scene::OUT_DIR);
		break;
	case 'w':
		scene->zoomActiveCamera(Scene::IN_DIR);
		break;
	case 'W':
		scene->zoomActiveCamera(Scene::OUT_DIR);
		break;
	case 033:
		exit( EXIT_SUCCESS );
		break;
	}
	glutPostRedisplay();
}

void keyboard_arrows(int key, int x, int y)
{
	if (activeTransformation->m_selectedTransformation == Transformation::NOTHING)
	{
		cout << "No active transformation\n";
		return;
	}
	switch (key)
	{
	case GLUT_KEY_UP:
		activeTransformation->applyTransformation(Scene::UP_DIR);
		break;
	case GLUT_KEY_DOWN:
		activeTransformation->applyTransformation(Scene::DOWN_DIR);
		break;
	case GLUT_KEY_LEFT:
		activeTransformation->applyTransformation(Scene::LEFT_DIR);
		break;
	case GLUT_KEY_RIGHT:
		activeTransformation->applyTransformation(Scene::RIGHT_DIR);
		break;
	}

	glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
	//button = {GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON}
	//state = {GLUT_DOWN,GLUT_UP}
	//set down flags
	switch(button) 
	{
		case GLUT_LEFT_BUTTON:
			lb_down = (state!=GLUT_UP);
			break;
		case GLUT_RIGHT_BUTTON:
			rb_down = (state!=GLUT_UP);
			break;
		case GLUT_MIDDLE_BUTTON:
			mb_down = (state!=GLUT_UP);	
			break;
	}
	// add your code
}

void passive_motion(int x, int y)
{
	// update last x,y
	last_x = x;
	last_y = y;
}

void motion(int x, int y)
{
	// calc difference in mouse movement
	int dx=x-last_x;
	int dy=y-last_y;
	cameraRotateTrans.m_model = scene->cameras[scene->m_activeCamera];

	if (lb_down)
	{
		if (dy > 0)
		{
			while (dy--)
				cameraRotateTrans.applyTransformation(Scene::DOWN_DIR);
		}
		if (dy < 0)
		{
			while (dy++)
				cameraRotateTrans.applyTransformation(Scene::UP_DIR);
		}

		if (dx > 0)
		{
			while (dx--)
				cameraRotateTrans.applyTransformation(Scene::LEFT_DIR);
		}
		if (dx < 0)
		{
			while (dx++)
				cameraRotateTrans.applyTransformation(Scene::RIGHT_DIR);
		}
		glutPostRedisplay();
	}

	
	// update last x,y
	last_x=x;
	last_y=y;
}

void chooseReflectionModelMenu(int id)
{
	switch (id)
	{
	case REFLECTION_MODEL_PHONG:
		renderer->m_reflectionModel = Renderer::PHONG_REFLECTION;
		break;
	case REFLECTION_MODEL_MODIFIED_PHONG:
		renderer->m_reflectionModel = Renderer::MODIFIED_PHONG_REFLECTION;
		break;
	}
	glutPostRedisplay();
}



void chooseShadingMenu(int id)
{
	switch (id)
	{
	case SHADING_FLAT:
		renderer->m_shadingType = Renderer::FLAT;
		break;
	case SHADING_GOARAUND:
		renderer->m_shadingType = Renderer::GOURAUND;
		break;
	case SHADING_PHONG:
		renderer->m_shadingType = Renderer::PHONG;
		break;
	}
	glutPostRedisplay();
}
void drawNormalsMenu(int id)
{
	switch (id)
	{
	case DRAW_NORMALS_PER_VERTEX:
		renderer->m_drawPerVertexNormals = true;
		break;
	case DRAW_NORMALS_PER_FACE:
		renderer->m_drawPerFaceNormals = true;
		break;
	case DRAW_NORMALS_OFF:
		renderer->m_drawPerFaceNormals = false;
		renderer->m_drawPerVertexNormals = false;
		break;
	}
	glutPostRedisplay();
}

void timerFunc(int value)
{
	if (!value)
	{
		return;
	}
	glutPostRedisplay();
	glutTimerFunc(100, timerFunc, 1);
	scene->draw();
}


void transformHungerianMenu(int id)
{
	HungerianCubeMeshModel* cube = dynamic_cast<HungerianCubeMeshModel*>(scene->models[scene->m_activeModel]);
	assert(cube);
	switch (id)
	{
		case TRANSFORM_HUNGERIAN_LEFT_FACE:
			cube->rotateFaceAndAnimate(scene, HungerianCubeMeshModel::LEFT_FACE_ROTATE, true);
			cube->rotateFaceAndAnimate(scene, HungerianCubeMeshModel::LEFT_FACE_ROTATE, true);
			break;
		case TRANSFORM_HUNGERIAN_RIGHT_FACE:
			cube->rotateFaceAndAnimate(scene, HungerianCubeMeshModel::RIGHT_FACE_ROTATE, true);
			break;
		case TRANSFORM_HUNGERIAN_UP_FACE:
			cube->rotateFaceAndAnimate(scene, HungerianCubeMeshModel::UP_FACE_ROTATE, true);
			break;
		case TRANSFORM_HUNGERIAN_DOWN_FACE:
			cube->rotateFaceAndAnimate(scene, HungerianCubeMeshModel::DOWN_FACE_ROTATE, true);
			break;
		case TRANSFORM_HUNGERIAN_FRONT_FACE:
			cube->rotateFaceAndAnimate(scene, HungerianCubeMeshModel::FRONT_FACE_ROTATE, true);
			break;
		case TRANSFORM_HUNGERIAN_BACK_FACE:
			cube->rotateFaceAndAnimate(scene, HungerianCubeMeshModel::BACK_FACE_ROTATE, true);
			break;
		default:
			assert(false);
			break;
	}
}


void renderingMenu(int id)
{
	switch (id)
	{
	case RENDERING_WIRE_FRAME_ONLY:
		renderer->SetWireframeOnly(true);
		break;
	case RENDERING_FULL:
		renderer->SetWireframeOnly(false);
		break;
	}
	glutPostRedisplay();
}

void boundingBoxMenu(int id)
{
	switch (id)
	{
	case BOUNDING_BOX_ON:
		scene->setActiveModelBoundingBox(true);
		break;
	case BOUNDING_BOX_OFF:
		scene->setActiveModelBoundingBox(false);
		break;
	}
	glutPostRedisplay();
}
void fileMenu(int id)
{
	switch (id)
	{
		case FILE_OPEN:
			CFileDialog dlg(TRUE,_T(".obj"),NULL,NULL,_T("*.obj|*.*"));
			if(dlg.DoModal()==IDOK)
			{
				std::string s((LPCTSTR)dlg.GetPathName());
				scene->loadOBJModel((LPCTSTR)dlg.GetPathName());
				modelTransformation.m_model = scene->models[scene->m_activeModel];
				activeTransformation = &modelTransformation;
				glutPostRedisplay();
			}
			break;
	}
}

void mainMenu(int id)
{
	switch (id)
	{
	case MAIN_MODEL_TRANSFORMATIONS:
		
		if (transformationDlg.DoModal() == IDOK)
		{
			modelTransformation.m_selectedFrame = transformationDlg.m_selectedFrame;
			modelTransformation.m_selectedTransformation = transformationDlg.m_selectedTransformation;
			modelTransformation.m_step = transformationDlg.m_stepSize;
			activeTransformation = &modelTransformation;
		}
		
		break;
	case MAIN_HUNGERIAN_CUBE_TRANSFORMATIONS:
		modelTransformation.m_selectedTransformation = Transformation::HUNGERIAN_LEFT_FACE;
		activeTransformation = &modelTransformation;
		break;
	case MAIN_CAMERA_TRANSFORMATIONS:
		if (cameraTransformationDlg.DoModal() == IDOK)
		{
			cameraTransformation.m_selectedFrame = cameraTransformationDlg.m_selectedFrame;
			cameraTransformation.m_selectedTransformation = cameraTransformationDlg.m_selectedTransformation;
			cameraTransformation.m_step = cameraTransformationDlg.m_stepSize;
			activeTransformation = &cameraTransformation;
		}
		break;
	case MAIN_LIGHT_TRANSFORMATIONS:
		if (cameraTransformationDlg.DoModal() == IDOK)
		{
			lightTransformation.m_selectedFrame = cameraTransformationDlg.m_selectedFrame;
			lightTransformation.m_selectedTransformation = cameraTransformationDlg.m_selectedTransformation;
			lightTransformation.m_step = cameraTransformationDlg.m_stepSize;
			activeTransformation = &lightTransformation;
		}
		break;

	case MAIN_RESET_TRANSFORMATIONS:
		scene->models[scene->m_activeModel]->resetTransformations();
		break;
	case MAIN_ADD_CAMERA:
		scene->addCamera(vec3(0,0,0));
		break;
	case HUNGERIAN_SOLVE:
		dynamic_cast<HungerianCubeMeshModel*>(scene->models[scene->m_activeModel])->solve(scene);
		break;
		
	case MAIN_MANAGE_CAMERAS:
		
		manageCamDlg.setCameras(scene->getAllCameras());
		manageCamDlg.setActiveCamera(scene->m_activeCamera);
		if (manageCamDlg.DoModal() == IDOK)
		{
			scene->setCameras(manageCamDlg.m_cameras);
			scene->m_activeCamera = manageCamDlg.m_activeCamera;
			cameraTransformation.m_model = scene->cameras[scene->m_activeCamera];
		}
		break;
	case MAIN_MANAGE_MODELS:
		manageModelsDialog.m_scene = scene;
		manageModelsDialog.DoModal();
		if (scene->m_activeModel != -1)
		{
			modelTransformation.m_model = scene->models[scene->m_activeModel];
			if(scene->models[scene->m_activeModel]->m_colorAnimationHueChange || 
				scene->models[scene->m_activeModel]->m_colorAnimationSaturationChange ||
				scene->models[scene->m_activeModel]->m_vertexAnimation)
			{
				glutTimerFunc(100, timerFunc, 1);
			}
			else
			{
				glutTimerFunc(100, timerFunc, 0);
			}
		}
		else
		{
			modelTransformation.m_model = NULL;
		}
		
		break;
	case MAIN_PERSPECTIVE_ORTHO:
		scene->toggleActiveCameraProjection();
		break;
	case MAIN_ADD_CUBE_PRIM:
		scene->loadPrimitiveModel(Scene::CUBE);
		modelTransformation.m_model = scene->models[scene->m_activeModel];
		activeTransformation = &modelTransformation;
		break;
	case MAIN_FOCUSE_TO_ACTIVE_MODEL:
		if (!scene->models.empty())
			scene->cameras[scene->m_activeCamera]->
				LookAtModel(scene->models[scene->m_activeModel]);
		break;
	case MAIN_SHOW_CAMERAS:
		scene->m_drawCameras = (!scene->m_drawCameras);
		break;
	case MAIN_MANAGE_LIGHT:
		lightsDlg.m_scene = scene;
		lightsDlg.DoModal();
		if (scene->m_activeLight < scene->m_lights.size())
		{
			lightTransformation.m_model = scene->m_lights[scene->m_activeLight];
		}
		
		break;
	case MAIN_SET_SCENE_AMBIENT_LIGHT:
		if (dlgColors.DoModal() == IDOK)
		{
			scene->m_ambient = dlgColors.GetColor();
		}
		break;
	case MAIN_ABOUT:
		AfxMessageBox(_T("Computer Graphics"));
		break;

	}

	glutPostRedisplay();
}

void addMenueEntryToMenu(int toMenu, string name, int value)
{
	glutSetMenu(toMenu);
	glutAddMenuEntry(name.c_str(), value);
}

void addSubMenuToMenu(int toMenu, string name, int subMenu)
{
	glutSetMenu(toMenu);
	glutAddSubMenu(name.c_str(), subMenu);
}


void initMenu()
{

	//create menu's
	int mainMenuId = glutCreateMenu(mainMenu);
	int fileMenuId = glutCreateMenu(fileMenu);
	int boundingBoxMenuId = glutCreateMenu(boundingBoxMenu);
	int drawNormalsMenuId = glutCreateMenu(drawNormalsMenu);
	int renderingMenuId = glutCreateMenu(renderingMenu);
	int chooseShadingMenuId = glutCreateMenu(chooseShadingMenu);
	int chooseReflectionModelMenuId = glutCreateMenu(chooseReflectionModelMenu);
	int transformHungerianMenuId = glutCreateMenu(transformHungerianMenu);

	//create sub menu's
	addSubMenuToMenu(mainMenuId, "File", fileMenuId);
	addSubMenuToMenu(mainMenuId, "Renderer", renderingMenuId);
	addSubMenuToMenu(mainMenuId, "Choose reflection model", chooseReflectionModelMenuId);
	addSubMenuToMenu(mainMenuId, "Choose shading", chooseShadingMenuId);
	addSubMenuToMenu(mainMenuId, "Bounding Box", boundingBoxMenuId);
	addSubMenuToMenu(mainMenuId, "Draw normals", drawNormalsMenuId);
	addSubMenuToMenu(mainMenuId, "Transform Hungerian cube", transformHungerianMenuId);
	addMenueEntryToMenu(mainMenuId, "Solve Hungerian cube", HUNGERIAN_SOLVE);

	addMenueEntryToMenu(drawNormalsMenuId, "Off", DRAW_NORMALS_OFF);
	addMenueEntryToMenu(drawNormalsMenuId, "Per vertex", DRAW_NORMALS_PER_VERTEX);
	addMenueEntryToMenu(drawNormalsMenuId, "Per face", DRAW_NORMALS_PER_FACE);

	addMenueEntryToMenu(transformHungerianMenuId, "left face", TRANSFORM_HUNGERIAN_LEFT_FACE);
	addMenueEntryToMenu(transformHungerianMenuId, "right face", TRANSFORM_HUNGERIAN_RIGHT_FACE);
	addMenueEntryToMenu(transformHungerianMenuId, "up face", TRANSFORM_HUNGERIAN_UP_FACE);
	addMenueEntryToMenu(transformHungerianMenuId, "down face", TRANSFORM_HUNGERIAN_DOWN_FACE);
	addMenueEntryToMenu(transformHungerianMenuId, "front face", TRANSFORM_HUNGERIAN_FRONT_FACE);
	addMenueEntryToMenu(transformHungerianMenuId, "back face", TRANSFORM_HUNGERIAN_BACK_FACE);

	addMenueEntryToMenu(mainMenuId, "Transform Model", MAIN_MODEL_TRANSFORMATIONS);
	addMenueEntryToMenu(mainMenuId, "Transform Camera", MAIN_CAMERA_TRANSFORMATIONS);
	addMenueEntryToMenu(mainMenuId, "Transform Light", MAIN_LIGHT_TRANSFORMATIONS);
	

	addMenueEntryToMenu(mainMenuId, "Reset Transformations", MAIN_RESET_TRANSFORMATIONS);
	addMenueEntryToMenu(mainMenuId, "Place Camera", MAIN_ADD_CAMERA);
	
	
	addMenueEntryToMenu(mainMenuId, "Manage Cameras", MAIN_MANAGE_CAMERAS);
	addMenueEntryToMenu(mainMenuId, "Manage Models", MAIN_MANAGE_MODELS);
	addMenueEntryToMenu(mainMenuId, "Manage Lights", MAIN_MANAGE_LIGHT);
	//addMenueEntryToMenu(mainMenuId, "Set Scene ambient light", MAIN_SET_SCENE_AMBIENT_LIGHT);
	
	addMenueEntryToMenu(mainMenuId, "Toggle Perspective/Orthogonal Projection", MAIN_PERSPECTIVE_ORTHO);
	addMenueEntryToMenu(mainMenuId, "Add a cube primitive", MAIN_ADD_CUBE_PRIM);

	addMenueEntryToMenu(mainMenuId, "Focus on active model", MAIN_FOCUSE_TO_ACTIVE_MODEL);
	addMenueEntryToMenu(mainMenuId, "Toggle show cameras", MAIN_SHOW_CAMERAS);
	
	addMenueEntryToMenu(boundingBoxMenuId, "On", BOUNDING_BOX_ON);
	addMenueEntryToMenu(boundingBoxMenuId, "Off", BOUNDING_BOX_OFF);
	
	
	addMenueEntryToMenu(renderingMenuId, "Wire frame only", RENDERING_WIRE_FRAME_ONLY);
	addMenueEntryToMenu(renderingMenuId, "Solid", RENDERING_FULL);
	
	//add open to file menu
	addMenueEntryToMenu(fileMenuId, "Open..", FILE_OPEN);
	
	addMenueEntryToMenu(mainMenuId, "About", MAIN_ABOUT);
	
	addMenueEntryToMenu(chooseShadingMenuId, "Flat", SHADING_FLAT);
	addMenueEntryToMenu(chooseShadingMenuId, "Goaraund", SHADING_GOARAUND);
	addMenueEntryToMenu(chooseShadingMenuId, "Phong", SHADING_PHONG);
	
	
	addMenueEntryToMenu(chooseReflectionModelMenuId, "Phong", REFLECTION_MODEL_PHONG);
	addMenueEntryToMenu(chooseReflectionModelMenuId, "Modified Phong", REFLECTION_MODEL_MODIFIED_PHONG);


	glutSetMenu(mainMenuId);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

}
//----------------------------------------------------------------------------


void profile(int n)
{

	long int before = GetTickCount();
	for (int i = 0; i < n; i++)
	{
		//cout << "i = " << i << endl;
		display();
	}
	long int after = GetTickCount();
	cout << "time : " << after - before << endl;
}

#include "MeshModel.h"
int my_main( int argc, char **argv )
{
	//----------------------------------------------------------------------------
	// Initialize window
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA| GLUT_DOUBLE);
	glutInitWindowSize( 512, 512 );
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutCreateWindow( "CG" );

	glewExperimental = GL_TRUE;
	glewInit();
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		/*		...*/
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	
	
	renderer = new Renderer(512,512);
	scene = new Scene(renderer);
	//----------------------------------------------------------------------------
	// Initialize Callbacks

	glutDisplayFunc( display );
	glutKeyboardFunc( keyboard );
	glutMouseFunc( mouse );
	glutMotionFunc ( motion );
	glutPassiveMotionFunc(passive_motion);
	glutReshapeFunc( reshape );
	glutSpecialFunc(keyboard_arrows);
	initMenu();

	cameraTransformation.m_model = scene->cameras[scene->m_activeCamera];
	scene->cameras[scene->m_activeCamera]->LookAt(vec3(0, 1, 8), vec3(0, 0, -1), vec3(0, 1, 0));
	//scene->cameras[scene->m_activeCamera]->LookAt(vec3(1.9, 6, 2.3), vec3(0, 0, -1), vec3(0, 1, 0));
	//scene->cameras[scene->m_activeCamera]->LookAt(vec3(0, 55, 1100), vec3(0, 0, -1), vec3(0, 1, 0));
	scene->cameras[scene->m_activeCamera]->Perspective();

	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\cube.txt");
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\cow.obj");
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\sphere\\sphere.obj");
	//
	//modelTransformation.m_model = scene->models[scene->m_activeModel];
	//scene->models[0]->translate(-1, 0, 10);
	//scene->addLight(vec4(10, 0, 0, 1), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE());
	//E:\workTechnion\graphics course\modelsUV
	scene->addLight(vec4(0, 0, 10, 1), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE());
	/*
	scene->addLight(vec4(0, 1000, 0, 1), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE());
	scene->addLight(vec4(0, -1000, 0, 1), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE());
	scene->addLight(vec4(1000, 0, 0, 1), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE());
	scene->addLight(vec4(-1000, 0, 0, 1), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE());
	scene->addLight(vec4(0,0,1000, 1), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE());
	scene->addLight(vec4(0,0,-1000, 1), CRGBColor::WHITE(), CRGBColor::WHITE(), CRGBColor::WHITE());
	*/
	lightTransformation.m_model = scene->m_lights[scene->m_activeLight];

	//return 0;
	// profiling code
	
	
	//
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\cube.txt");
	
	//scene->loadOBJModel("C:\\Users\\shaivaknin\\git_workspace\\obj_examples\\sphere\\sphere.obj");
	//scene->loadOBJModel("C:\\Users\\shaivaknin\\git_workspace\\obj_examples\\cube.obj");
	//scene->loadOBJModel("C:\\Users\\shaivaknin\\git_workspace\\obj_examples\\cube.obj");
	

	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\objects\\colony_ship.obj");
	//E:\workTechnion\graphics course\objects

	scene->loadPrimitiveModel(Scene::HUNGERIAN_CUBE);
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\teapot.obj");
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\cow.obj");
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\sphere\\sphere.obj");
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\modelsUV\\cube.obj");
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\sphere\\sphere.obj");
	//scene->loadPrimitiveModel(Scene::CUBE);
	//scene->models[scene->m_activeModel]->m_enviormentMapping = true;
	//scene->loadPrimitiveModel(Scene::CUBE);
	//scene->models[scene->m_activeModel]->m_enviormentMapping = true;
	//scene->loadOBJModel("C:\\Users\\shaivaknin\\git_workspace\\obj_examples\\cube.obj");
	//CImg<unsigned char> image("C:\\Users\\shaivaknin\\git_workspace\\obj_examples\\chess.bmp");
	//cout << "width = " << image.width() << endl;;
	//scene->loadOBJModel("C:\\Users\\shaivaknin\\git_workspace\\obj examples\\sphere.obj");
	//modelTransformation.m_model = scene->models[scene->m_activeModel];
	//scene->models[scene->m_activeModel]->m_enviormentMapping = true;
	//modelTransformation.m_model->translate(0, 2, 0);
	//modelTransformation.m_model->rotateX(180);
	//modelTransformation.applyTransformation(Scene::UP_DIR);
	
	//scene->loadOBJModel("C:\\Users\\shaivaknin\\git_workspace\\obj_examples\\teapot.obj");
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\cow.obj");
	//scene->loadOBJModel("C:\\Users\\shaivaknin\\git_workspace\\modelsUV\\cowUV.obj");
	//float s = 1.0/30;
	//scene->models[scene->m_activeModel]->scale(s,s,s);
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\sphere\\sphere.obj");
	//200px-IntP_Brick_NormalMap
	//scene->loadPrimitiveModel(Scene::CUBE);
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\sphere\\sphere.obj");
	//scene->models[scene->m_activeModel]->loadNormalsTexture("C:\\Users\\shaivaknin\\git_workspace\\obj_examples\\200px-IntP_Brick_NormalMap.bmp");
	//scene->loadOBJModel("C:\\Users\\shaivaknin\\git_workspace\\obj_examples\\cow.obj");
	renderer->m_shadingType = Renderer::PHONG;
	//renderer->m_shadingType = Renderer::GOURAUND;
	//scene->models[scene->m_activeModel]->m_silhoutteRendering = true;
	//scene->models[scene->m_activeModel]->m_toonShading = true;

	//scene->models[scene->m_activeModel]->m_enviormentMapping = true;
	//scene->models[scene->m_activeModel]->m_enviormentMapping = true;
	//scene->models[scene->m_activeModel]->loadTexture("E:\\workTechnion\\graphics course\\objects\\chess.bmp");
	//scene->models[scene->m_activeModel]->loadTexture("E:\\workTechnion\\graphics course\\objects\\chess.bmp");
	//scene->loadOBJModel("E:\\workTechnion\\graphics course\\obj_examples\\cow.obj");
	//scene->loadPrimitiveModel(Scene::CUBE);
	modelTransformation.m_model = scene->models[scene->m_activeModel];
	//modelTransformation.m_model->translate(0, 1, 0);
	activeTransformation = &modelTransformation;
	//modelTransformation.m_model->translate(0, 0, 2);
	//scene->models[scene->m_activeModel]->m_colorAnimationHueChange = true;
	//scene->m_activeModel--;
	//scene->cameras[scene->m_activeCamera]->
	//	LookAtModel(scene->models[scene->m_activeModel]);
	//scene->m_activeModel++;
	//profile(10, 1);
	//return 0;
	//for (int i = 1; i < 100; i++) profile(2, i);


	glutMainLoop();
	delete scene;
	delete renderer;
	return 0;
}

CWinApp theApp;

using namespace std;

int main( int argc, char **argv )
{
	int nRetCode = 0;
	
	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		nRetCode = 1;
	}
	else
	{
		my_main(argc, argv );
	}
	
	return nRetCode;
}
