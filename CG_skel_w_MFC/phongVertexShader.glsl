
uniform int normalMapping;
uniform mat4 modelMatrix; //ok
uniform mat4 cameraMatrix; //ok

uniform mat4 modelview; //ok
uniform mat4 normalsModelview; //ok
uniform mat4 projection; //ok
uniform int time;
uniform int vertexAnimation;

in vec3 vPosition; //model coordinates
in vec3 normal; //model coordinates
in vec3 vT;
in vec3 vB;
in vec2 vNormalTextCoord;
in float meterialIdx;

out vec2 texCoord;
out vec3 N;
out vec3 L[MAX_LIGHTS];
out vec3 E;
out vec3 fT;
out vec3 fB;

out struct Meterial {
   vec3 ambient;
   vec3 diffuse; 
   vec3 specular; 
   vec3 emissive;
   float shinines;
} meterial;

float rand1(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(12.9898,78.233, 309.2169))) * 43758.5453);
}
float rand2(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(5645.5647,124.8789, 2654.6945))) * 183.9832);
}
float rand3(vec3 co){
    return fract(sin(dot(co.xyz ,vec3(477.89, 25.68 ,6876.854))) * 54235.6467);
}

Meterial getMeterial(vec3 v)
{
	Meterial m;
	m.ambient = vec3(0,0,0);
	m.diffuse = vec3(0,0,0);
	m.specular = vec3(0,0,0);
	m.emissive = v;
	return m;
}
void main()
{
	vec3 animatedPosition = vPosition;
	if(vertexAnimation == 1)
	{
		//animation step is between -1 and 1
		float animStep = sin(rand1(vPosition.xyz)*time/10.0)*rand1(vPosition.xyz);
		animatedPosition += animStep*vec3(0,0.1,0);
	}

	vec4 v = modelview*vec4(animatedPosition, 1);
	N = (normalsModelview*vec4(normal, 0)).xyz;
	E = -v.xyz;

	for(int i = 0; i < MAX_LIGHTS && i < numLights; i++)
	{
		if(lights[i].position.w == 0.0)
		{
			L[i] = lights[i].position.xyz;
		}
		else
		{
			L[i] = lights[i].position.xyz - v.xyz;
		}
	}

	meterial = allMeterials[int(meterialIdx)];
	
	
	gl_Position = projection*v;


	if(normalMapping == 1)
	{
		texCoord = vNormalTextCoord;
		fT = vT;
		fB = vB;
	}
	
}

