#pragma once


// CMetirialPropertiesDialog dialog
#include "RGBColor.h"
#include "afxcolorbutton.h"
#include "afxcmn.h"

class CMetirialPropertiesDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CMetirialPropertiesDialog)
	virtual BOOL OnInitDialog();
public:
	CMetirialPropertiesDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMetirialPropertiesDialog();

// Dialog Data
	enum { IDD = IDD_METIRIAL_PROPERTIES };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CMFCColorButton m_ambient;
	CMFCColorButton m_diffuse;
	CMFCColorButton m_specular;
	CMFCColorButton m_emissive;
	int m_isUniform;
	CSliderCtrl m_shininesSlider;
	int m_shinines;
};
